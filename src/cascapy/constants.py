"""
SYNOPSIS:
    This module contains numerical values of most common physical constants in CGS units.
    It also contains a method to build a grid of values.
    
DEPENDENCIES:
    numpy, astropy
    
META:
    author: R. Belmont
    date:   23/05/24
"""


import numpy as np
import astropy.units as u

# Universal constants
ec       = 4.8032068e-10            # electron charge
me       = 9.1093837015e-28         # electron mass
mi       = 1.67262192e-24           # proton mass
c        = 2.99792458e+10           # speed of light
mc2      = me*c*c                   # electron rest mass energy
kb       = 1.380649E-16             # Boltzmann constant
hp       = 6.62607015e-27           # Planck constant
alpha    = 1./137.035999679         # fine structure constant
r0       = ec*ec/mc2                # electron classical radius
lC       = hp/(me*c)                # Compton wavelength
sigmaT   = 8.*np.pi*(r0**2)/3.      # Thomson cross-section
Gc       = 6.6726e-8                # gravitational constants
sigmas   = 5.6705e-5                # Stefan constant
aW       = 2.70117803291906         # Wien's kind of constants for Planck law: aa=<E>/kT= pi^4/30/zeta(3)

# Distances
pc       = 3.08568025e18            # parsec
kpc      = pc*1.e3                  # kilo-parsec
Mpc      = pc*1.e6                  # Mega-parsec
Gpc      = pc*1.e9                  # Giga-parsec

# Energies
eV       = 1.602176634e-12
keV      = 1.e3*eV
MeV      = 1.e6*eV
GeV      = 1.e9*eV
TeV      = 1.e12*eV

# angles
degree   = np.pi/180.

# Time
hour     = 3600.
day      = 24*hour
year     = 365.*day

# CMB properties
kTcmb    = 2.725*kb                  # CMB black-body temperature at z=0
ecmb     = aW*kTcmb                  # average energy of CMB photons
ncmb     = 410.5089783               # number density of CMB photons at z=0
ucmb     = 4.171848126e-13           # energy density of CMB photons at z=0


# --------------------------------------------------------------------------
def ftime(t):
    """
    SYNOPSIS:
        Convert time from seconds to the best time unit (among seconds, minutes, hour, days,
        and years)
    ARGUMENTS:
        t:      time in seconds
    RETURNS:
        t:      time (in best units)
        unit:   unit (str)
    """
    t = t*u.s
    units = ['s', 'min', 'h', 'd', 'yr', 'kyr', 'Myr', 'Gyr']
    uu = 's'
    for ui in units:
        if t.to_value(ui) >= 1:
            uu = ui
    return t.to_value(uu), uu
# --------------------------------------------------------------------------


# --------------------------------------------------------------------------
def grid(xmin0, xmax0, nbin, log=False):
    """
    SYNOPSIS:
        This function defines a parameter grid with bins in a given range.
    ARGUMENTS:
        xmin0:  left edge of the parameter range
        xmax0:  right edge of the parameter range
        nbin:   number of bins
        log:    Flag for log grid (default is false)
    RETURNS:
        xa:     array of bin centers
        xb:     array of bin edges
        dx:     array of bin widths
    """
    xmin = np.float64(xmin0)
    xmax = np.float64(xmax0)
    
    if log:
        xb = np.logspace(np.log10(xmin), np.log10(xmax), nbin+1, dtype='float64')
        xa = (xb[:-1]*xb[1:])**0.5
    else:
        xb = np.linspace(xmin, xmax, nbin+1, dtype='float64')
        xa = 0.5*(xb[:-1]+xb[1:])
        
    dx = xb[1:]-xb[:-1]
    
    return xa, xb, dx
# --------------------------------------------------------------------------
