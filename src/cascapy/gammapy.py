"""
SYNOPSIS:
    This module aims at building a Gammapy model from a table of spectra collecting results of
    Monte Carlo simulations. Such a model can then be used in any gammapy analysis as any other
    model.
    We could have defined a time-dependent spectral model or an energy dependent temporal model.
    However, these kind of models are not yet entirely supported in gammapy. Moreover, it would
    imply heavier tables (with many more time points) and integrating over each time bin at the
    fitting, which would  slow down the analysis. Instead, the class defined here is a collection
    of steady spectral models, one for each pre-defined energy bin. The time integration is hence
    performed at the table level, before any fitting procedure. However, it must be kept in mind
    that the chosen time bins should be short enough for the flux to be rather constant within
    each time bin.

DEPENDENCIES:
    numpy, astropy, gammapy, scipy, matplotlib

META:
    author: R. Belmont
    date:   3/12/24
"""

# Generic packages
try:
    from gammapy.modeling.models import TemplateNDSpectralModel, SpectralModel
    from gammapy.modeling import Fit
    from gammapy.modeling.parameter import Parameter, Parameters
    from gammapy.modeling.models import Models, SkyModel, DatasetModels
    from gammapy.maps import MapAxis, RegionNDMap
    from gammapy.datasets import Dataset, Datasets, SpectrumDatasetOnOff, SpectrumDataset
    from gammapy.data import observatory_locations, FixedPointingInfo, Observation
    from gammapy.maps import RegionGeom
    from gammapy.makers import SpectrumDatasetMaker
    from gammapy.irf import load_irf_dict_from_file
    from gammapy.utils.random import get_random_state
except ImportError as err:
    print("Gammapy must be installed before importing the cascapy.gammapy package")
    raise ModuleNotFoundError

import os as os
from itertools import product
import numpy as np
from scipy.interpolate import interp1d
from scipy.ndimage import gaussian_filter as gfilter
from scipy.optimize import root_scalar
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
import astropy.units as u
from astropy.coordinates import SkyCoord, Angle
from astropy.time import Time
from regions import CircleSkyRegion

# Current packages
from .constants import GeV, TeV
from .tablemodel import TableModel

__all__ = ["ScaledTemplateNDSpectralModel", "CascadeTemporalSpectralModel", "Scan", "CascadeDatasets",
           "TableFit", "get_dir_datasets"]


# ==================================================================================================
class ScaledTemplateNDSpectralModel(TemplateNDSpectralModel):
    """
    SYNOPSIS:
        This class extends the gammapy TemplateNDSpectralModel() by adding a scale parameter.
        
    INSTANTIATION ARGUMENTS (see TemplateNDSpectralModel):
        mmap:           RegionNDMap object containing the data and variable parameters
        interp_kwargs:  keyword to pass the interpolation method
        meta:           meta information
        filename:       name for optional yaml saving
    """
    
    tag = ['ScaledTemplateNDSpectralModel', 'tablemodel']
    
    def __init__(self, mmap, **kwargs):
        super().__init__(mmap, **kwargs)
        self.scale = Parameter("scale", 1.0, unit="", scale_method='scale10',
                               interp="log", frozen=False)  # , min=1.e-10, max=1.e10)
        self.default_parameters = Parameters([self.scale]+[par for par in self.parameters])

    def _evaluate(self, energy, **kwargs):
        """
        This method is a copy-paste of the parent-class evaluate() method
        """
        coord = {"energy_true": energy}
        coord.update(kwargs)
        pixels = [0, 0] + [self.map.geom.axes[key].coord_to_pix(value) for key, value in coord.items()]
        val = self.map.interp_by_pix(pixels, **self._interp_kwargs)
        return u.Quantity(val, self.map.unit, copy=False)

    def evaluate(self, energy, **kwargs):
        """
        Wrapper of the parent-class evaluate() method that adds a dimensionless scale parameter
        """
        scale = 1.*kwargs["scale"].value
        del kwargs['scale']
        return scale * self._evaluate(energy, **kwargs)

# ==================================================================================================


# ==================================================================================================
class CascadeTemporalSpectralModel(Models):
    """
    SYNOPSIS:
        This class builds is derived from the 'Models' gammapy class and is a collection of
        several 'Skymodels', one per time bin. All parameters of these models are linked.
        Only the observation time is different. This set of models is meant to be used to
        join-fit a corresponding set of observation data.
        Some variable parameters of the datafile can be frozen at this stage (instead of later
        at the fitting stage). This makes the model lighter and faster since the frozen parameters
        are hard removed from the table, and the interpolation step is performed on a table with
        fewer dimensions.
        Additionally, a smoothing can be performed at this stage in order to damp some spurious
        oscillations in the spectra of Monte Carlo simulation when the statistics is low.
        Warning: In order to prevent spurious oscillations at the order of the table energy range
        (due to the interpolation procedure) make sure that the energies where the model fall within
        the table range (at least with a bin-width safety distance).

    ARGUMENTS:
        filename:   name of the table file to read
        frozen:     dictionary of parameters of table parameters (with values) that must be frozen
        smooth_dex: smoothing scale (in dex). Often, 0.1 provides nice results.
        mname:      name for the current model

    EXAMPLES:
        # Build a gammapy table model and visualize its energy- and time content
        from cascapy.constants import TeV
        from cascapy.gammapy import *
        models = CascadeTemporalSpectralModel('./data/table.h5', smooth_dex=0.1, frozen={'Emax': 10 * TeV})
        models.spectra()
        models.lightcurves()
    """
    def __init__(self, filename, frozen=None, smooth_dex=0, mname='test'):

        # Model name
        self.name = mname

        # Read table file
        tbl = TableModel(file=filename, mode='r')

        # Build energy axis (in erg)
        ebins = np.append(tbl.ebins[:, 0], tbl.ebins[-1, -1])
        energy_axis = MapAxis.from_energy_edges(ebins, unit='erg', name='energy_true', interp='log')
        axes = [energy_axis]

        # store time bins
        self.tbins = tbl.tbins

        # Frozen parameters:
        if frozen is None:
            frozen = {}

        # Build axes for the variable parameters (WARNING: in reverse order!!)
        for par in reversed(tbl.vparams):
            if par not in frozen:
                axes += [MapAxis.from_nodes(tbl.vparams[par].values, interp='lin', name=par)]

        # Scale data before smoothing or interpolating
        data = tbl.data.copy()
        data = np.multiply(data, energy_axis.center.value ** 2)
        data[data <= 0] = 1.e-25

        # Perform a smoothing of the cascade contribution using gaussian filtering with a width of
        # 'smooth_dex' decades
        if smooth_dex > 0:
            data = np.log(data)
            dex0 = energy_axis.nbin / np.log10(energy_axis.edges.value[-1] / energy_axis.edges.value[0])
            data[1, ...] = gfilter(data[1, ...], sigma=smooth_dex * dex0, axes=(-1,))
            data = np.exp(data)

        # Add up primary and secondary photons to get the total spectrum.
        # Addition must be performed in linear scale
        data = np.sum(data, axis=0)

        # interpolate for frozen parameters
        data = np.log(data)
        ifroz = 0
        for [rank, par] in enumerate(tbl.vparams):
            if par in frozen:
                f = interp1d(tbl.vparams[par].values, data, kind='linear', axis=rank - ifroz, assume_sorted=True,
                             fill_value='extrapolate', bounds_error=False)
                data = f(frozen[par])
                ifroz += 1
        data = np.exp(data)

        # Descale data after smoothing and interpolating
        data = np.multiply(data, 1. / energy_axis.center.value ** 2)

        # Loop and create one model per time bin
        models = []
        nt = np.shape(data)[-2]
        for it in range(nt):

            # model name
            mid = mname + f"{it:02d}"

            # create map from axes and maps
            mapp = RegionNDMap.create(region=None, axes=axes, data=np.take(data, it, axis=-2), unit="erg-1 s-1 cm-2")

            # Create spectral model
            interp_kwargs = {"method": 'linear', "values_scale": 'log', "fill_value": -50}
            meta = {'table': filename, 'filename': mid, 'tmin': tbl.tbins[it, 0], 'tmax': tbl.tbins[it, 1]}
            meta.update(frozen)
            spectral_model = ScaledTemplateNDSpectralModel(mapp, interp_kwargs=interp_kwargs, meta=meta)

            # link the parameters of the spectral model
            if it > 0:
                for name in spectral_model.default_parameters.names:
                    setattr(spectral_model, name, getattr(models[0].spectral_model, name))

            # Create and append Skymodel
            models.append(SkyModel(spectral_model=spectral_model, name=mid))

        # Build final models list
        models = Models(models)
        models.update_link_label()

        # alias for the main parameters (unfortunately, it is not possible to call it parameters because
        # it is already a property method in the parent class)
        self.params = models.parameters

        # set the parameter scan grid to the table grid
        for par in self.params:
            if par.name in tbl.vparams:
                par.scan_values = tbl.vparams[par.name].values

        super().__init__(models)

    @property
    def parameters(self):
        """Parameters as a `~gammapy.modeling.Parameters` object."""
        return self.params

    def spectra(self, xlim=(1.e0, 1.e4), ylim=(1.e-13, 1.e-5), nvals=100):
        """
        SYNOPSIS:
            This method builds and opens a widget to visualize some interpolated spectra of the current model at
            all times.
        ARGUMENTS:
            xlim:   tuple of plot x-boundaries
            ylim:   tuple of plot y-boundaries
            nvals:  number of values for the parameters sliders
        """

        # get energy, and parameter Map axes
        maxes = self[0].spectral_model.map.geom.axes
        maxes = self[0].spectral_model.map.geom.axes
        e = maxes['energy_true'].center
        nvpar = len(self[0].spectral_model.parameters.names)
        # remove scale parameter
        nvpar = nvpar - 1
        # number of time bins
        nt = len(self)

        # Plot margins
        lpad = 0.02
        rpad = 0.03
        tpad = 0.03
        bpad = 0.02
        hax = 0.1
        vax = 0.06
        sldw = 0.1

        fig = plt.figure()

        # Define the main plot area and plot a default spectrum
        left = lpad + nvpar*sldw + hax
        bottom = tpad+sldw+hax
        right = 1-rpad
        top = 1-tpad
        ax = fig.add_axes((left, bottom, right-left, top-bottom))

        # Define sliders for time and variable parameters
        sliders = []
        axes = []
        for [i, maxis] in enumerate(maxes[1:]):

            vmin, vmax = maxis.bounds.value
            dv = (vmax-vmin)/nvals

            # Parameter, vertical sliders
            left = lpad+i*sldw
            bottom = bpad + hax
            axes.append(fig.add_axes((left, bottom, sldw, 1-vax-tpad-bottom)))
            sliders.append(Slider(ax=axes[-1], orientation="vertical",
                                  valstep=dv,
                                  label=maxis.name,
                                  valmin=vmin,
                                  valmax=vmax,
                                  valinit=vmin))
            self[0].parameters[maxis.name].value = vmin

        # Horizontal slider for time bins
        left = lpad + nvpar*sldw + hax
        bottom = bpad
        axes.append(fig.add_axes((left, bottom, 1-rpad-left-0.01, sldw)))
        sliders.append(Slider(ax=axes[-1], orientation="horizontal", valstep=1,
                              label='Time',
                              valmin=0,
                              valmax=nt-1,
                              valinit=0))

        # Default plot
        y0 = self[0].spectral_model(e)
        line1, = ax.plot(e/GeV, e**2*y0, ls='-')
        ax.set_title = 'Time'

        # Function to be called anytime a slider's value changes in order to update the plot
        def update(val):

            it = 0
            for [ii, axis] in enumerate(maxes[1:]):
                self[0].parameters[axis.name].value = sliders[ii].val
                it = sliders[-1].val

            line1.set_ydata(e**2*self[it].spectral_model(e))

            fig.canvas.draw_idle()

        for i in range(nvpar+1):
            sliders[i].on_changed(update)

        ax.set_xscale('log')
        ax.set_yscale('log')
        ax.set_xlim(xlim)
        ax.set_ylim(ylim)
        ax.set_xlabel('Energy (GeV)')
        ax.set_ylabel('EFE (erg/s/cm2)')
        plt.show()

    def lightcurves(self, xlim=None, ylim=(1.e-13, 1.e-5), nvals=100):
        """
        SYNOPSIS:
            This method builds and opens a widget to visualize the lightcurve of the current model at
            all energies.
        ARGUMENTS:
            xlim:   tuple of plot x-boundaries
            ylim:   tuple of plot y-boundaries
            nvals:  number of values for the sliders
        """

        # get energy, and parameter Map axes
        maxes = self[0].spectral_model.map.geom.axes
        e = maxes['energy_true'].center
        ea = maxes['energy_true'].edges
        nvpar = len(self[0].spectral_model.parameters.names)
        # remove scale parameter
        nvpar = nvpar - 1
        # number of time bins
        tt = (self.tbins[:, 0] * self.tbins[:, 1])**0.5
        if xlim is None:
            xlim = self.tbins[0, 0], self.tbins[-1, 1]

        # Plot margins
        lpad = 0.02
        rpad = 0.03
        tpad = 0.03
        bpad = 0.02
        hax = 0.1
        vax = 0.06
        sldw = 0.1

        fig = plt.figure()

        # Define the main plot area and plot a default spectrum
        left = lpad + nvpar*sldw + hax
        bottom = tpad+sldw+hax
        right = 1-rpad
        top = 1-tpad
        ax = fig.add_axes((left, bottom, right-left, top-bottom))

        # Define sliders for time and variable parameters
        ie = int(len(e)/2)
        sliders = []
        axes = []
        for [i, maxis] in enumerate(maxes[1:]):

            vmin, vmax = maxis.bounds.value
            dv = (vmax-vmin)/nvals

            # Parameter, vertical sliders
            left = lpad+i*sldw
            bottom = bpad + hax
            axes.append(fig.add_axes((left, bottom, sldw, 1-vax-tpad-bottom)))
            sliders.append(Slider(ax=axes[-1], orientation="vertical",
                                  valstep=dv,
                                  label=maxis.name,
                                  valmin=vmin,
                                  valmax=vmax,
                                  valinit=vmin))
            self[0].parameters[maxis.name].value = vmin

        # Horizontal slider for energy
        left = lpad + nvpar*sldw + hax
        bottom = bpad
        axes.append(fig.add_axes((left, bottom, 1-rpad-left-0.01, sldw)))
        sliders.append(Slider(ax=axes[-1], orientation="horizontal", valstep=1,
                              label='Energy',
                              valmin=0,
                              valmax=len(e)-1,
                              valinit=ie))

        # Default plot
        y0 = [self[it].spectral_model.energy_flux(ea[ie], ea[ie+1]).value for it in range(len(self))]
        line1, _, bar1 = ax.errorbar(tt, y0, xerr=[tt-self.tbins[:, 0], self.tbins[:, 1]-tt], ls='', marker='o')
        ax.set_title = 'Time'

        # Function to be called anytime a slider's value changes in order to update the plot
        def update(val):

            for [ii, axis] in enumerate(maxes[1:]):
                self[0].parameters[axis.name].value = sliders[ii].val
            ii = sliders[-1].val
            y = np.squeeze([self[it].spectral_model.energy_flux(ea[ii], ea[ii+1]).value for it in range(len(self))])
            line1.set_ydata(y)
            bar1[0].set_segments(zip(zip(self.tbins[:, 0], y), zip(self.tbins[:, 1], y)))

            fig.canvas.draw_idle()

        for i in range(nvpar+1):
            sliders[i].on_changed(update)

        ax.set_xscale('log')
        ax.set_yscale('log')
        ax.set_xlim(xlim)
        ax.set_ylim(ylim)
        ax.set_xlabel('Time (s)')
        ax.set_ylabel(r'$F_E$ (erg/s/cm2)')
        plt.show()

# ==================================================================================================


# ==================================================================================================
class Scan:
    """
    SYNOPSIS:
        This class contains information about a scan result
    ARGUMENTS:
        grids:              array of values for the scan parameter
        success:            array of scan success
        total_stat:         array of scan statistics
        params:             array of Parameters()
        filename_table:     filename of the table used to make the scan
        filename_datasets:   filename of a saved dataset used to make this scan
    """

    def __init__(self, grids=None, success=None, total_stat=None, params=None,
                 filename_table=None, filename_datasets=None):

        if filename_datasets is None:
            self.filename_datasets = None
        else:
            self.filename_datasets = os.path.abspath(filename_datasets)

        if filename_table is None:
            self.filename_table = None
        else:
            self.filename_table = os.path.abspath(filename_table)

        if grids is None:
            grids = {}
        self._nd = len(grids)
        self._grids = grids

        shape = tuple([len(values) for values in grids.values()])

        if success is None:
            success = np.full(shape, False)
        self._success = success

        if total_stat is None:
            total_stat = np.zeros(shape)
        self._total_stat = total_stat

        if params is None:
            params = np.empty(shape, dtype=type(Parameters()))
        self._params = params

    @property
    def nd(self):
        return self._nd

    @property
    def grids(self):
        return self._grids

    @property
    def success(self):
        return self._success

    @property
    def total_stat(self):
        return self._total_stat

    @property
    def params(self):
        return self._params

    def append(self, scan):
        """
        Append the results of a 1d scan to the current scan if 1d too.
        """

        if self.nd == 1 and scan.nd == 1:

            name = list(scan.grids.keys())[0]
            if name in self._grids:
                value_added = False
                for val, succ, stat, par in zip(scan.grids[name], scan.success, scan.total_stat, scan.params):
                    if val not in self.grids[name]:
                        self._grids[name] = np.append(self._grids[name], val)
                        self._success = np.append(self._success, succ)
                        self._total_stat = np.append(self._total_stat, stat)
                        # numpy-append adds each individual parameter instead of appending an entire Parameters() object
                        # instead use list-append
                        self._params = list(self._params) + [par]
                        value_added = True
                if value_added:
                    self._params = np.array(self._params)
                    self.sort()
            else:
                raise ValueError(f"{name} not found in grid names")

        else:
            raise ValueError(f"Can only append 1-d scan to 1-d scan (here {scan.nd}-d to {self.nd}-d)")

    def sort(self):
        """
        Sort 1d-scan values for increasing order of the scanned parameter
        """
        if len(self.grids) == 1:
            name = list(self.grids.keys())[0]
            ind = np.argsort(self.grids[name])
            if len(ind) > 0:
                self._grids[name] = self.grids[name][ind]
                self._total_stat = self.total_stat[ind]
                self._params = self.params[ind]
        else:
            raise ValueError(f"Can only sort 1-d arrays (here {self.nd}-d)")

    def copy(self):
        cls = self.__class__
        result = cls.__new__(cls)
        result.__dict__.update(self.__dict__)
        return result

    def __str__(self):
        strg = ''
        strg += f"nd = {self._nd} \n"
        strg += f"grids = {self._grids} \n"
        if self.nd == 0:
            strg += f"success = {self._success} \n"
            strg += f"total_stat = {self._total_stat} \n "
            for par in self._params[()]:
                strg += f"{par.name}: {par.value}, "
            strg += '\n'
        elif self.nd == 1:
            strg += f"success = {self._success} \n"
            strg += f"total_stat = {self._total_stat} \n "
            for pars in self._params:
                for par in pars:
                    strg += f"{par.name}: {par.value}, "
                strg += '\n'
        else:
            strg += f"success = ... \n"
            strg += f"total_stat = ... \n "
            strg += f"params = ... \n "
        return strg

    def to_file(self, filename):
        """
        Save scan using numpy.save and allow_pickle=True
        """
        np.save(filename, self, allow_pickle=True)

    @staticmethod
    def from_file(filename):
        """
        Load a scan saved using numpy.save and allow_pickle=True
        """
        return np.load(filename, allow_pickle=True)[()]

    def plot(self, savefig=None):
        """
        Simple plots for 1d- and 2d-scans
        """

        fig, ax = plt.subplots()

        if self.nd == 1:
            names = list(self.grids.keys())
            x = list(self.grids.values())[0]
            y = np.sqrt(self.total_stat - self.total_stat.min())

            ax.plot(x, y, marker='o')
            ax.set_xlabel(names[0])
            ax.set_ylabel(r'$\sigma = \sqrt{TS}$')

            if savefig is not None:
                plt.savefig(f'DS{savefig}/DS{savefig}_prof.png', bbox_inches='tight')

            plt.show()

        elif self.nd == 2:

            names = list(self.grids.keys())
            x, y = self.grids.values()
            stat = self.total_stat.T
            z = np.sqrt(stat - stat.min())

            x, y = np.meshgrid(x, y)
            levels = np.linspace(0, 10, 11)
            cf = ax.contourf(x, y, z, levels=levels, vmax=10, cmap='viridis_r')
            cb = plt.colorbar(cf)
            cb.ax.set_ylabel(r'$\sigma = \sqrt{TS}$')

            ax.set_xlabel(names[0])
            ax.set_ylabel(names[1])

            if savefig is not None:
                plt.savefig(f'DS{savefig}/DS{savefig}_map.png', bbox_inches='tight')

            plt.show()

        else:

            raise ValueError(f"Can only plot 1-d or 2-d scans (here {self.nd}-d)")

        return ax
# ==================================================================================================


# ==================================================================================================
class CascadeDatasets(Datasets):

    """
    SYNOPSIS:
        This class extends the generic Datasets class.
        Each dataset of this class corresponds to a different time bin and is associated to a
        separated model which corresponds to the prediction of a more general model in this
        specific time bin.
    """

    # -------------------------------------------------------------------------------------------
    def __init__(self, datasets=None):
        super().__init__(datasets)
        self._model = None
        self.filename = None

    # -------------------------------------------------------------------------------------------

    # -------------------------------------------------------------------------------------------
    @property
    def parameters(self):
        """
        Lazy descriptor for `self._model[0].parameters.unique_parameters`
        """
        return self._model[0].parameters.unique_parameters

    # -------------------------------------------------------------------------------------------

    # -------------------------------------------------------------------------------------------
    @property
    def model(self):
        """
        Lazy descriptor for self.models
        """
        return self._model

    @model.setter
    def model(self, models):
        """
        Lazy setter for self.models
        """
        self.models = models

    # -------------------------------------------------------------------------------------------

    # -------------------------------------------------------------------------------------------
    @property
    def models(self):

        models = {}

        for dataset in self:
            if dataset.models is not None:
                for model in dataset.models:
                    models[model] = model
        models = DatasetModels(list(models.keys()))

        if self._covariance and self._covariance.parameters == models.parameters:
            return DatasetModels(models, covariance_data=self._covariance.data)
        else:
            return models

    @models.setter
    def models(self, models):

        if len(self) != len(models):
            raise ValueError(f"Model size does not match dataset: {len(models)}, {len(self)}")

        self._model = models

        if models:
            self._covariance = DatasetModels(models).covariance

            for dataset, model in zip(self, models):
                dataset.models = DatasetModels(model)

    # -------------------------------------------------------------------------------------------

    # -------------------------------------------------------------------------------------------
    def create(self, models, eb=None, skycoord=None, loc='north', zenith=40, tref=None, name='test'):
        """
        SYNOPSIS:
            Create a data set from model and observations properties
        ARGUMENTS:
            models:     set of linked models (one per time bin)
            eb:         edges of the energy bins
            skycoord:   source coordinates (Skycoord object)
            loc:        CTA site ('north' or 'south')
            zenith:     average zenith angle
            tref:       reference time for the beginning of the first time bin (Time() object)
            name:       dataset name
        RESULT:
            no parameter returned.
            The dataset instance is however updated
        """

        # table model
        nt = len(models)
        tbins = np.zeros((nt, 2))
        for it in range(nt):
            tbins[it] = models[it].spectral_model.meta['tmin'], models[it].spectral_model.meta['tmax']

        # Energy bins
        if eb is None:
            eb = np.logspace(np.log10(0.04), np.log10(8.0), 20)
        eb2 = np.logspace(np.log10(eb[0] / 3), np.log10(eb[-1] * 3), 3 * len(eb))
        energy_axis = MapAxis.from_edges(eb, unit="TeV", name="energy", interp="log")
        energy_axis_true = MapAxis.from_edges(eb2, unit="TeV", name="energy_true", interp="log")

        # Observation site
        if loc.lower() not in ['north', 'south']:
            raise ValueError("Observatory location not found (north or south)")
        loca = observatory_locations['cta_' + loc.lower()]
        loc = loc[0].upper() + loc[1:].lower()

        # Source zenith angle
        if zenith not in [20, 40, 60]:
            raise ValueError("Zenith angle not found (20, 40 or 60)")

        # observation direction
        if skycoord is None:
            skycoord = SkyCoord(266.40498829, -28.93617776, unit="deg", frame="icrs")

        # Create an empty dataset for this observation
        center = skycoord.directional_offset_by(position_angle=0 * u.deg, separation=0.5 * u.deg)
        on_region = CircleSkyRegion(center=center, radius=Angle("0.11 deg"))
        geom = RegionGeom.create(region=on_region, axes=[energy_axis])
        dataset_empty = SpectrumDataset.create(geom=geom, energy_axis_true=energy_axis_true, name='empty')

        # Misc
        pointing = FixedPointingInfo(fixed_icrs=skycoord)
        maker = SpectrumDatasetMaker(selection=["exposure", "edisp", "background"])
        tirf = np.array([1800., 18000., 180000.])
        ttr = np.sqrt(tirf[1:] * tirf[:-1])

        # Observation time
        if tref is None:
            tref = Time("2023-03-01")

        # Build datasets
        datasets = []
        for it in range(nt):

            # Time bin duration
            dt = tbins[it, 1] - tbins[it, 0]
            if dt < ttr[0]:
                t = tirf[0]
            elif dt > ttr[-1]:
                t = tirf[-1]
            else:
                ind = np.where(ttr < dt)[0][-1]
                t = tirf[ind + 1]

            # CTA IRF
            irfname = (os.environ["CTA_IRFS"] +
                       f"Prod5-{loc}-{zenith:d}deg-AverageAz-4LSTs09MSTs.{int(t):d}s-v0.1.fits.gz")
            irfs = load_irf_dict_from_file(irfname)

            # Create observation template
            obs = Observation.create(pointing=pointing, tstart=tref + tbins[it, 0] * u.s, livetime=dt * u.s, irfs=irfs,
                                     location=loca, obs_id=it, reference_time=tref)

            # create simulated map from model
            dataset = dataset_empty.copy(name=name + f"_{it:02d}")
            dataset = maker.run(dataset, obs)
            #            dataset.models = models[it]

            # perform on/off analysis to include to background model
            dataset_on_off = SpectrumDatasetOnOff.from_spectrum_dataset(dataset=dataset, acceptance=1, acceptance_off=5,
                                                                        name=name + f"_{it:02d}")

            # Add dataset to list
            datasets.append(dataset_on_off)

        # create private attribute
        self._datasets = Datasets(datasets=datasets)

        # Apply models
        self.model = models

    # -------------------------------------------------------------------------------------------

    # -------------------------------------------------------------------------------------------
    def fake(self, nobs=1, random_state='random-seed'):
        """
        SYNOPSIS:
            Fake data of all timebins a given number of times
        NOTICE:
            A datasetonoff() can only be faked ONCE using this method. Indeed, the background level before
            fake() is set by the empty parent dataset() can is used to fake the datasetonoff. However,
            after a fake(), the background level of the datasetonoff() is estimated from the on/off counts
            of the faked dataset. However, this method can consistently fake() many observation using
            the 'nobs' keyword.
        ARGUMENTS:
            nobs:           number of faked data to produce
            random_state:   integer       -> constant random seed
                            'random_seed' -> run dependent random seed
        RESULT:
            a list of nobs faked datasets
        """

        # Init random generator
        random_state = get_random_state(random_state)
        seeds = random_state.get_state()[1][1:1 + len(self)]

        # Check that the background model has not been changed by a previous fake
        # -> Background predictions for DatasetOnOff are based on 'on/off' counts once a fake
        #    has already been performed
        # -> However before any fake, it retains the predictions of the parent Dataset, which
        #    is derived from IRFS only.
        # If it is the case, then save this initial background
        counts = np.sum([dataset.counts.data.sum() for dataset in self])
        if counts != 0:
            raise ValueError("Background model has been changed by a previous fake. Only fake empty datasets.")
        bkgs = [dataset.background.copy() for dataset in self]

        # perform all fakes
        datasets = []
        for idx in range(nobs):

            dataset_fake = self.copy()

            for it, (bkg, ds) in enumerate(zip(bkgs, dataset_fake)):
                # Realisation- and dataset- dependent seed
                seed = int(seeds[it] / (idx + 1) / (nobs + 1))

                # fake data
                ds.fake(npred_background=bkg, random_state=seed)
                ds.meta_table["OBS_ID"] = [idx]

            # append to list of faked data
            datasets.append(dataset_fake)

        return datasets
    # -------------------------------------------------------------------------------------------

    # -------------------------------------------------------------------------------------------
    def read(self, filename, **kwargs):
        """
        Read a dataset that was saved to a file using the 'write()' méthode of the parent class.
        """
        dataset = super().read(filename, **kwargs)
        dataset.filename = os.path.realpath(filename)
        return dataset
    # -------------------------------------------------------------------------------------------
# ==================================================================================================


# ==================================================================================================
class TableFit(Fit):
    """
    SYNOPSIS:
        This class holds specific methods to fit data with large table models.
        Namely, this methods aim at:
            - more robust results by scanning the parameter space to prevent the fitting procedure to get stuck in
              local minima
            - more efficiency by using 'reduced' table models where frozen parameters are excluded. Despite some
              tile overhead to redefine the model at each fit step, it allows to fit in a smaller parameter space,
              hence reducing significantly the interpolation time.
    ARGUMENTS:
        table:      filename of the tabulated models
        dataset:    the dataset to analyse
        frozen:     dictionary of parameters to always freeze and exclude from the table
    """

    def __init__(self, table, datasets, frozen=None, **kwargs):

        super().__init__(**kwargs)
        self.table = os.path.realpath(table)

        # Set Dataset
        self.datasets = datasets

        # Get all parameters of the table model and set default initial values to some common parameters
        self.parameters = CascadeTemporalSpectralModel(filename=table, mname='tmp')[0].parameters.copy()
        init0 = {"scale": 1.0, "gamma": 2.0, "alpha": 1.0, "lambdaB": 6.0, "logB": -17.0, "emax": 10.0 * TeV}
        for nm in init0.keys() & self.parameters.names:
            if (init0[nm] >= self.parameters[nm].min) and (init0[nm] <= self.parameters[nm].max):
                self.parameters[nm].value = init0[nm]

        if frozen is not None:
            for nm, val in frozen.items():
                self.parameters[nm].value = val
                self.parameters[nm].frozen = True

    def update_from_parameters(self, parameters):
        for par in parameters:
            if par.name in self.parameters.names:
                self.parameters[par.name].min = par.min
                self.parameters[par.name].max = par.max
                self.parameters[par.name].scan_values = par.scan_values
                self.parameters[par.name].prior = par.prior
                self.parameters[par.name].value = par.value
                self.parameters[par.name].frozen = par.frozen

    # -------------------------------------------------------------------------------------------
    def fit(self, chatter=1, update=True):
        """
        SYNOPSIS:
            Before fitting, this method re-defined the table model by excluding all frozen parameters
            instead of freezing then at the fitting leven.
            This allows interpolation of the tabulated spectrum in a smaller parameter space, hence
            it is much faster (despite some overhead in creating this new model at each fit)
        ARGUMENTS:
            chatter (int):  level of information displayed
            update (bool):  if true, the parameter attribute is updated
        RESULT:
            gammapy fit result object
        """

        # use a local copy of the dataset
        ds = self.datasets.copy()

        # get all frozen parameters to hard-exclude them from the table model
        frozen = {par.name: par.value for par in self.parameters if par.frozen}

        # Build a reduced model from where all frozen parameters have been discarded
        ds.model = CascadeTemporalSpectralModel(filename=self.table, frozen=frozen, mname='tmp')

        # update model parameters with dataset parameters as fit initialisation
        for par in ds.model.parameters:
            if par.name in self.parameters.names:
                par.min = self.parameters[par.name].min
                par.max = self.parameters[par.name].max
                par.scan_values = self.parameters[par.name].scan_values
                par.prior = self.parameters[par.name].prior
                par.value = self.parameters[par.name].value
                # clip values
                if (par.min == par.min) and (par.max == par.max):
                    par.value = np.clip(par.value, par.min*(1+10*np.sign(par.min)*np.finfo(float).eps),
                                        par.max*(1-10*np.sign(par.max)*np.finfo(float).eps))
#                    par.value = np.clip(par.value, par.min, par.max)

        if chatter > 1:
            print("Fitting for parameters ", *(par.name for par in self.parameters if not par.frozen), "...")

        # print('Avant')
        # for par in ds[0].models.parameters:
        #     print(par)

        # perform fit
        result = self.run(ds)

        if update:
            for par in result.parameters:
                self.parameters[par.name].value = par.value
#                if (par.value < par.min) or (par.value > par.max):
#                    print(f"Warning: {par.name} outside of allowed range: {par.value}")
            for nm, value in frozen.items():
                self.parameters[nm].value = value

        # print('Apres')
        # print(result.total_stat)
        # for par in self.parameters:
        #     print(par)

        if chatter > 1:
            print(f"Fit Success: {result.success}")
            print(f"stat-tot: {result.total_stat}")
        if chatter > 2:
            print(ds.models[0].spectral_model)

        return result

    # -------------------------------------------------------------------------------------------

    # -------------------------------------------------------------------------------------------
    def run_scan(self, grids, chatter=1, reduce=True, filename=None):
        """
        SYNOPSIS:
            This method freezes the parameters to scan to all possible combinations of grid values, and
            fit for the remaining parameters.
            It can also proceed to a reduction step if required (see the 'reduce' method)
        ARGUMENTS:
            grids:      dictionary {name:array} of parameters to scan and scanning values
            reduce:     If True, also proceed to a reduction step by recursively letting the last free the last
                        parameter of the scan and re-fitting.
            filename:   if set, this is the root base for all files written to save results. If not set
                        results are not saved.
            chatter:    chatter level

        RESULT:
            a Scan() instance that contains the stat values, success, and the parameters of all fits.
            When reduction is required, it results a list of Scan() instances, one per reduction step.
        """

        # Exclude grids values out of the parameter range
        for nm in grids:
            grids[nm] = grids[nm][(grids[nm] >= self.parameters[nm].min) & (grids[nm] <= self.parameters[nm].max)]

        # grid properties
        snames = list(grids.keys())
        nspar = len(snames)
        shape = tuple([len(values) for values in grids.values()])

        if chatter > 0:
            print(f"Performing {len(shape)}-d scan")

        # Init an empty Scan() object that will hold the scan result
        newscan = Scan(grids=grids, filename_table=self.table, filename_datasets=self.datasets.filename)

        # Scan all parameters
        for ind in product(*([range(ll) for ll in shape])):

            if chatter > 1:
                print(ind)

            # Use the best parameters values of the previous scan step fit
            if np.count_nonzero(np.array(ind) > 0):
                indprev = list(ind)
                j = nspar - 1 - np.nonzero(indprev[::-1])[0][0]
                indprev[j] = indprev[j] - 1
                for par in newscan.params[(*indprev,)]:
                    self.parameters[par.name].value = par.value

            # if very first step: use values of self.parameters
            else:
                pass

            # set values of the scanned parameter ones
            for i, nm in enumerate(snames):
                self.parameters[nm].value = grids[nm][ind[i]]
                self.parameters[nm].frozen = True

            # perform fit
            res = self.fit(chatter=chatter, update=True)

            # store results
            newscan.total_stat[(*ind,)] = res.total_stat
            newscan.success[(*ind,)] = res.success
            newscan.params[(*ind,)] = Parameters([par.copy() for par in self.parameters])

        # store results to files if filename provided
        if filename is not None:
            newscan.to_file(filename+f"_{newscan.nd}d")

        # perform reduction if required
        if reduce:
            scans = [newscan]
            for i in range(nspar):
                newscan = self.reduce(newscan, chatter=chatter, filename=filename)
                scans += [newscan]
            return scans
        else:
            return newscan

    # -------------------------------------------------------------------------------------------

    # -------------------------------------------------------------------------------------------
    def reduce(self, scan, chatter=1, filename=None):
        """
        SYNOPSIS:
            Once a n-d scan has been performed by freezing n parameters to values on the n-d grid, a
            reduction step consists in letting the last parameter free, and re-fitting at each grid value
            of the remaining scanned parameter. This results in ta (n-1)-d scan where the last parameter
            is excluded.
        ARGUMENTS:
            scan:       And existing n-d scan instance
            filename:   if set, this is the root base for all files written to save results. If not set
                        results are not saved.
            chatter:    chatter level

        RESULT:
            a (n-1)-Scan() instance that contains the stat values, success, and the parameters of all fits
        """

        # get the names of all scanned parameters in the previous scan
        snames = list(scan.grids.keys())

        # free the last parameter
        self.parameters[snames[-1]].frozen = False

        # remove the last parameter from the list to scan
        grids = {nm: scan.grids[nm] for nm in snames[:-1]}
        shape = tuple([len(values) for values in grids.values()])

        if chatter > 0:
            print(f"Performing {len(shape)}-d scan")

        # Init an empty scan object that will hold the scan result
        newscan = Scan(grids=grids, filename_table=self.table, filename_datasets=self.datasets.filename)

        # Scan all parameters
        for ind in product(*([range(ll) for ll in shape])):

            if chatter > 1:
                print(ind)

            # Set all parameters to their corresponding value in the previous scan
            # Set the freed parameter to the best value in the previous scan
            imin = scan.total_stat[(*ind,)].argmin()
            for par in scan.params[(*ind, imin)]:
                self.parameters[par.name].value = par.value

            # perform fit
            res = self.fit(chatter=chatter, update=True)

            # store results
            newscan.total_stat[(*ind,)] = res.total_stat
            newscan.success[(*ind,)] = res.success
            newscan.params[(*ind,)] = Parameters([par.copy() for par in self.parameters])

        # store results to files if filename provided
        if filename is not None:
            newscan.to_file(filename+f"_{newscan.nd}d")

        return newscan

    # -------------------------------------------------------------------------------------------

    # -------------------------------------------------------------------------------------------
    @staticmethod
    def get_range(y, level):
        """
        Get boundaries of regions where the input array is below a given level.
        Returns a list containing the tuples (left,right) indices of all regions.
        This aims at helping in defining confidence levels
        """
        rg = []
        n = len(y)
        i1 = np.where(y < level)[0]
        while len(i1) > 0:
            i1 = i1[0]
            i2 = i1 + 1 + np.where(y[i1 + 1:] > level)[0]
            if len(i2) > 0:
                i2 = i2[0] - 1
            else:
                i2 = n - 1
            rg += [[i1, i2]]
            i1 = i2 + 1 + np.where(y[i2 + 1:] < level)[0]
        return np.array(rg)

    # -------------------------------------------------------------------------------------------

    # -------------------------------------------------------------------------------------------
    def fit_from_scan_1d(self, scan_1d, level=10, chatter=1):
        """
        SYNOPSIS:
            Fit procedure for situations with degenerate parameters and/or complicated parameter spaces
            - Start from an existing 1d scan along one parameter (others being free)
            - Define several regions of local minima where the statistics is below a given statistics
            - Perform a global fit in each region (letting free the scanned parameter)
        ARGUMENTS:
            scan_1d:        an existing 1-d Scan() instance
            level:          upper value (in statistics TS) to define separated regions
            chatter (int):  level of information displayed
        RESULT:
            a scan_1d type of dictionary containing the properties of all minima founds
        """

        name = list(scan_1d.grids.keys())[0]
        y = scan_1d.total_stat

        # get edges of regions where the statistics is below 'level'
        rg = self.get_range(y - y.min(), level)

        xx, yy, pp, succ = [], [], [], []
        # In each range find minimum
        for i, (i1, i2) in enumerate(rg):

            if chatter > 0:
                print(f"Fitting region {i}")

            # get best fit in scan
            imin = i1 + y[i1:i2 + 1].argmin()
            self.update_from_parameters(scan_1d.params[imin])

            # refit
            self.parameters[name].frozen = False
            res = self.fit(update=False, chatter=chatter)

            # append results
            xx += [res.parameters[name].value]
            yy += [res.total_stat]
            succ += [res.success]
            pars = Parameters(self.parameters.copy())
            pp += [pars]

            if chatter > 0:
                print('success:   ', res.success)
                print('total stat:', res.total_stat)
                for par in pars:
                    print(par)

        return Scan(grids={name: np.array(xx)}, success=np.array(succ), total_stat=np.array(yy), params=pp)
    # -------------------------------------------------------------------------------------------

    # -------------------------------------------------------------------------------------------
    def _fopt(self, val, name, offset=0., init=None, chatter=1, update=True):

        if init is not None:
            self.update_from_parameters(init)
        self.parameters[name].value = val
        self.parameters[name].frozen = True

        res = self.fit(chatter=chatter, update=update)

        return res.total_stat - offset

    # -------------------------------------------------------------------------------------------
    # -------------------------------------------------------------------------------------------
    def confidence_from_scan_1d(self, scan_1d, nsigma=None, chatter=1):
        """
        SYNOPSIS:
            This method starts from an existing 1-d scan.
            For each uncertainty level sigma in nsigma[], it defines all regions of the scanned
            parameter space where the statistics is below sigma^2, based on the scanned values.
            Then it refines the regions boundaries by root-finding the exact parameter values where
            the statistics equals sigma^2.
        ARGUMENTS:
            scan_1d:    A 1-d 'Scan()' object
            nsigma:     list of confidence levels to compute
            chatter (int): level of information displayed
        RESULT:
            levels:     array of parameter boundaries for each region and each level sigma
            scan:       A 'Scan()' object made of the found parameters values
        """

        if nsigma is None:
            nsigma = []

        name = list(scan_1d.grids.keys())[0]
        x = scan_1d.grids[name]
        y = scan_1d.total_stat
        ymin = y.min()

        xx, yy, pp, succ, levels = [], [], [], [], []
        for ns in nsigma:

            if chatter > 0:
                print(f"Getting {ns}-sigma level")

            lev = []

            for (i1, i2) in self.get_range(y - ymin, ns ** 2):

                # Left level
                x1 = None
                if i1 > 0:
                    #                    print('i1',i1,x[i1])
                    bkts = [x[i1 - 1], x[i1]]
                    bkts.sort()
                    try:
                        res = root_scalar(self._fopt, args=(name, ymin + ns ** 2, scan_1d.params[i1], chatter),
                                          bracket=bkts, maxiter=10, xtol=0.001)
                    except Exception as errr:
                        print(f"Warning: search for left confidence level {ns} failed.")
                        print(errr)
                    else:
                        x1 = res.root
                        xx += [x1]
                        succ += [True]
                        yy += [self._fopt(res.root, name, 0., scan_1d.params[i1], chatter)]

                        pars = Parameters(self.parameters.copy())
                        pars[name].value = res.root
                        pp += [pars]

                # Right level
                x2 = None
                if i2 < len(x) - 1:
                    #                    print('i2',i2,x[i2])
                    bkts = [x[i2], x[i2 + 1]]
                    bkts.sort()
                    try:
                        res = root_scalar(self._fopt, args=(name, ymin + ns ** 2, scan_1d.params[i2], chatter),
                                          bracket=bkts, maxiter=10, xtol=0.001)
                    except Exception as errr:
                        print(f"Warning: search for right confidence level {ns} failed")
                        print(errr)
                    else:
                        x2 = res.root
                        xx += [x2]
                        succ += [True]
                        yy += [self._fopt(res.root, name, 0., scan_1d.params[i2], chatter)]
                        pars = Parameters(self.parameters.copy())
                        pars[name].value = res.root
                        pp += [pars]

                    lev += [[x1, x2]]

            levels += [lev]

        scan = Scan(grids={name: np.array(xx)}, success=np.array(succ), total_stat=np.array(yy), params=pp)

        return levels, scan

    # -------------------------------------------------------------------------------------------

    # -------------------------------------------------------------------------------------------
    def profile_from_scan_1d(self, scan_1d, level=10, nsigma=None, chatter=1, filename=None):
        """
        SYNOPSIS:
            From an existing 1-d scan, this method performs global fits (with the scanned parameter
            free) in all regions where the stat (TS-TSmin) is below a given level. Adds the fits
            results to the input scan and returns the augmented scan.
            This methods can also compute the x-sigma uncertainty on the scanned parameter, in all
            regions.
        ARGUMENTS:
            scan_1d:    input 1d scan ('Scan()' object)
            level:      level defining the regions to investigate (default 10)
            nsigma:     list of uncertainty levels (default: no uncertainty computed)
            filename:    if not None: the resulting scan will be stored to 'DSxxxxx/DSxxxx.prof' with
                        xxxxx being the provided name
            chatter (int): lelvel of information displayed
        RESULT:
            scan_1d:        the augmented scan
        """

        if nsigma is None:
            nsigma = []

        # save local copy of the dataset
        scan_1d = scan_1d.copy()

        # Performs global fit in all minimal regions of scan_1d
        scan = self.fit_from_scan_1d(scan_1d, level=level, chatter=chatter)
        scan_1d.append(scan)

        # get levels:
        if len(nsigma) > 0:
            levels, scan = self.confidence_from_scan_1d(scan_1d, nsigma=nsigma, chatter=chatter)
            scan_1d.append(scan)
            if chatter > 0:
                for i, ss in enumerate(nsigma):
                    print(f"{ss}-sigma levels:", [ll for ll in levels[i]])

        # store to file
        if filename is not None:
            np.save(f'{filename}_prof.npy', scan_1d, allow_pickle=True)

        return scan_1d
    # -------------------------------------------------------------------------------------------

# ==================================================================================================


def get_dir_datasets(path='./'):
    """
    Return the list of all xxxxx datasets stored in the 'path' directory, with names as 'DSxxxxx'
    """
    ids = [i[2:] for i in os.listdir(path) if os.path.isdir(os.path.join(path, i)) and i.startswith('DS')]
    ids.sort()
    return ids


# ==================================================================================================
if __name__ == '__main__':
    pass
# ==================================================================================================
