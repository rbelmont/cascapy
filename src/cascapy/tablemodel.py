"""
SYNOPSIS:
    This package defines a class to build, read and visualize a table model.

DEPENDENCIES
    public: numpy, matplotlib, time, itertools, getpass, multiprocessing, copy, os
    misc:   h5py

META:
    author: R. Belmont
    date:   03/10/24
"""

# General packages
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
from matplotlib.patches import Rectangle
from h5py import File as H5file
from time import time, localtime, strftime
from getpass import getuser
from itertools import product
from multiprocessing import Pool, set_start_method, current_process, Lock
from copy import deepcopy

# current package
from .constants import hour, day, GeV, TeV, ftime
from .cascade import EventCatalog, BinnedData, parse_filename
from .intrinsic import *

__all__ = ['Param', 'TableModel', 'time_bins', 'view_tbins', 'Logf']


# ==================================================================================================
class Param:
    """
    SYNOPSIS:
        This is made for parameters of a table model
        
    INSTANTIATION ARGUMENTS:
        name (str):     name of the parameter
        rank (int):     rank of the parameter in the model table (low rank vary slowly)
        values (arr):   list of parameter values used to build the table
    
    ATTRIBUTES:
        name (str):     name of the parameter
        rank (int):     rank of the parameter in the model table (low rank vary slowly)
        values (arr):   list of parameter values used to build the table
        nval (int):     number of values in the parameter values list
        index (int):    current index of the parameter (initialised to 0)
        value (float):  current value of the parameter (initialised to first value in list)
    """
# ==================================================================================================
    def __init__(self, name, rank, values):
        self.name = name
        self.values = values
        self.rank = rank
        self.nval = len(self.values)
        self.index = 0
        self.value = self.values[0]
# ==================================================================================================


# ==================================================================================================
class TableModel:
    """
    SYNOPSIS:
        This class builds a table model either from Monte-Carlo simulations (and passed arguments)
        or from an existing table HDF5 file.
        
    ARGUMENTS:
        file:   path+filename or hdf5 file object
                For write mode: only string filename is allowed
                For read mode: both types are allowed:
                - If a filename is provided, then the routine opens the file, read data, close file
                and returns a copy of all data read. Note that all data is then loaded into RAM which
                might be RAM consuming for large files.
                - If a file object is provided, then the file must already be opened manually with
                read access, and it must be closed manually afterward. In that case, the routine
                only returns references to the relevant file data. Data is only loaded piecewise
                when needed. Note that the file must not be closed before any use of file data has
                been completed.
        mode:   'w': the table model is built from instantiation arguments and from Monte Carlo
                    simulations files
                'r': the table model is read from an already existing table HDF5 file. And
                     arguments are not used.
        simupath:   path to the folder where all files simulation files are stored. In that
                    directory simulations files must be in subdirectories based on redshift
                    and named according to their z, -log(B), log(lambdaB) : e.g.
                    'simupath/z042/particles_z0425_B1625_L600.csc'
        ebins:      arrays of left and right edges of the energy bins (nex2)
        tbins:      arrays of left and right edges of the time bins (ntx2)
        ngen:       number of generation to keep (default is 4)
        smodel:     an instance of the Smodel class defining the time/spectral/angular properties
                    of the primary emission
#        igmf:       default dictionary {'logB','lambdB'} of log10(B) and log(lambdaB). Only
                    used when logB or lambdaB a not variable parameters
        vparams:    a dictionary with values for all variables parameters
        comments:   list of string to add custom comments to that model
        chatter:    level of information display (integer)
    RETURN:
        None.
    """

    # ----------------------------------------------------------------------------------------------
    def __init__(self,
                 file='table.h5',
                 mode='r',
                 simupath='./',
                 ebins=None,
                 tbins=None,
                 ngen=4,
                 smodel=None,
                 igmf=None,
                 vparams=None,
                 comments='',
                 chatter=0):
        """
        SYNOPSIS:
            This method instantiates the table model.
        """
        self.file = file
        self.simupath = simupath
        self.chatter = chatter
        self.data = None
        self.ngen = ngen

        # Read table from an existing file
        if mode == 'r':
        
            self.read_table()
        
        # Build table from Monte Carlo simulations and store it to file
        elif mode == 'w':
        
            self.ebins = ebins
            self.tbins = tbins
            if smodel is not None:
                self.smodel = smodel
            if vparams is None:
                self.vparams = {}
            else:
                self.vparams = vparams
            self.comments = comments
            if igmf is None:
                self.igmf = {}
            else:
                self.igmf = igmf

        # Raise error for bad input
        else:
        
            raise ValueError(f"Unknown action mode: '{mode}'. Must be 'r' or 'w'")
            
    # ----------------------------------------------------------------------------------------------

    # ----------------------------------------------------------------------------------------------
    # ENERGY
    # ----------------------------------------------------------------------------------------------
    @property
    def ebins(self):
        return self._ebins

    @ebins.setter
    def ebins(self, bins):
        if bins is None:
            bins = np.logspace(np.log10(1*GeV), np.log10(10*TeV), 41)
        bins = np.atleast_1d(bins)
        if np.size(bins) < 2:
            raise ValueError("Bins require at least 2 elements (2 edges)")
        if len(np.shape(bins)) < 2:
            self._ebins = np.array([bins[:-1], bins[1:]]).T
        else:
            self._ebins = bins
        self.nebins = len(self._ebins)

    # ----------------------------------------------------------------------------------------------
    # TIME
    # ----------------------------------------------------------------------------------------------
    @property
    def tbins(self):
        return self._tbins

    @tbins.setter
    def tbins(self, bins):
        if bins is None:
            bins = np.logspace(np.log10(60), np.log10(5*day), 11)
        bins = np.atleast_1d(bins)
        if np.size(bins) < 2:
            raise ValueError("Bins require at least 2 elements (2 edges)")
        if len(np.shape(bins)) < 2:
            self._tbins = np.array([bins[:-1], bins[1:]]).T
        else:
            self._tbins = bins
        self.ntbins = len(self._tbins)
    # ----------------------------------------------------------------------------------------------

    # ----------------------------------------------------------------------------------------------
    # IGMF MODEL
    # ----------------------------------------------------------------------------------------------
    @property
    def igmf(self):
        return {key: self._fparams[key] for key in ['logB', 'lambdaB'] if key in self._fparams}

    @igmf.setter
    def igmf(self, igmf):
        if hasattr(self, '_fparams'):
            self._fparams.update(igmf)
        else:
            self._fparams = igmf
        if hasattr(self, '_vparams'):
            for name in self._vparams:
                if name in self._fparams:
                    del self._fparams[name]
            self.nfpar = len(self._fparams)

    # ----------------------------------------------------------------------------------------------
    # SOURCE MODEL
    # ----------------------------------------------------------------------------------------------
    @property
    def smodel(self):
        return self._smodel

    @smodel.setter
    def smodel(self, smod):
        self._smodel = smod
        if hasattr(self, '_fparams'):
            self._fparams.update(smod.params.copy())
        else:
            self._fparams = smod.params.copy()
        if hasattr(self, '_vparams'):
            for name in self._vparams:
                if name in self._fparams:
                    del self._fparams[name]
            self.nfpar = len(self._fparams)

    # ----------------------------------------------------------------------------------------------
    # VARIABLE PARAMETERS
    # ----------------------------------------------------------------------------------------------
    @property
    def vparams(self):
        return self._vparams

    @vparams.setter
    def vparams(self, params):
    
        if hasattr(self, '_vparams') and len(self.vparams) > 0:
            raise AttributeError("vparam can only be set once.")
        self.nvpar = len(params)
        self._vparams = {key: Param(name=key, rank=i, values=params[key]) for i, key in enumerate(params)}
                
        if hasattr(self, '_fparams'):
            for name in self._vparams:
                if name in self._fparams:
                    del self._fparams[name]
            self.nfpar = len(self._fparams)

#        ii = np.array([rank for rank,key in enumerate(params.keys()) if key in self.rparams])
#        if len(ii) > 0:
#            if np.max(ii)>len(ii)-1:
#                raise ValueError("Variable parameters requiring new file reading must be of smallest rank")

    # ----------------------------------------------------------------------------------------------
    # FIXED PARAMETERS
    # ----------------------------------------------------------------------------------------------
    @property
    def fparams(self):
        return self._fparams

    # ----------------------------------------------------------------------------------------------
    # PARAMETERS THAT REQUIRE READING A NEW SIMULATION FILE WHEN CHANGED
    # ----------------------------------------------------------------------------------------------
    @property
    def rparams(self):
        v = {'logB': 0., 'lambdaB': 0., 'z': 0.}
        for name in v:
            if hasattr(self, '_vparams'):
                if name in self.vparams:
                    v[name] = self.vparams[name].value
            if hasattr(self, '_fparams'):
                if name in self.fparams:
                    v[name] = self.fparams[name]
        return v

    # ----------------------------------------------------------------------------------------------
    # ADDITIONAL COMMENTS
    # ----------------------------------------------------------------------------------------------
    @property
    def comments(self):
        return self._comments

    @comments.setter
    def comments(self, com):
        self._comments = com

    # ----------------------------------------------------------------------------------------------
    # DISPLAY TABLE SUMMARY
    # ----------------------------------------------------------------------------------------------
    def info(self):
        """
        SYNOPSIS:
            This method displays to screen a table summary
        ARGUMENTS:
        RESULT:
            None
        """
        if hasattr(self, '_tbins'):
            print("TIME")
            print(f"   tmin (s)  = {np.min(self.tbins[:,0]):.2e}")
            print(f"   tmax (s)  = {np.max(self.tbins[:,1]):.2e}")
            print(f"   ntbins    = {self.ntbins}")
        if hasattr(self, '_ebins'):
            print("ENERGY")
            print(f"   emin (GeV) = {np.min(self.ebins[:,0])/GeV:.2e}")
            print(f"   emax (GeV) = {np.max(self.ebins[:,1])/GeV:.2e}")
            print(f"   nebins     = {self.nebins}")
        if hasattr(self, '_fparams'):
            print("IGMF")
            print(f"   {self.igmf}")
        if hasattr(self, '_smodel'):
            print("SOURCE MODEL")
            if self.smodel.tag == 'SeparableSourceModel':
                print("Separable Source Model")
                print(f"   Time    model: {self.smodel.tmodel.params}")
                print(f"   Energy  model: {self.smodel.emodel.params}")
            elif self.smodel.tag == 'NonSeparableSourceModel':
                print("Non Separable Source Model")
            else:
                print("Unknown Source Model")
            print(f"   Angular model: {self.smodel.amodel.params}")
            print(f"   normalization: {self.smodel.norm}")
        if hasattr(self, '_fparams'):
            print("FIXED PARAMETERS")
            print(f"   {self.fparams}")
        if hasattr(self, '_vparams'):
            print("VARIABLE PARAMETERS")
            for key in self.vparams:
                print(f"   {key}: {self.vparams[key].values}")
        if hasattr(self, '_comments'):
            print("ADDITIONAL COMMENTS")
            for com in self.comments:
                print(f"   {com.strip()}")

    # ----------------------------------------------------------------------------------------------
    def init_table(self):
        """
        SYNOPSIS:
            This method creates a new HDF5 file, initializes it with the correct structure and stores
            some basic information in it (e.g. time bins, energy bins, parameters etc.).
            Only real data (spectra) are missing at the end of this stage.
        ARGUMENTS:
        RETURN:
        """

        if not hasattr(self, '_tbins'):
            raise ValueError("Missing tbins")
        if not hasattr(self, '_ebins'):
            raise ValueError("Missing ebins")
        if not hasattr(self, '_smodel'):
            raise ValueError("Missing smodel")
        if not hasattr(self, '_vparams'):
            raise ValueError("Missing vparams")
        if not hasattr(self, '_fparams'):
            raise ValueError("Missing fparams")
        if len(self._vparams) == 0:
            raise ValueError("Empty vparams")
        for key in ['logB', 'lambdaB']:
            if key not in [name for name in self.vparams] and key not in self.fparams:
                raise ValueError(f"Missing {key}")

        # Open File and add some default headers --------------------------------------------------------
        f = H5file(self.file, 'w', track_order=True)

        # General descriptors
        f.attrs['Description'] = 'Cascadel Table Model'
        f.attrs['Date'] = time()
        f.attrs['Auhtor'] = getuser()
        f.attrs['SourceModel'] = self.smodel.tag
        if self.smodel.tag == 'SeparableSourceModel':
            f.attrs['TimeModel'] = self.smodel.params['Ttype']
            f.attrs['SpectralModel'] = self.smodel.params['Etype']
        f.attrs['AngularModel'] = self.smodel.params['Atype']
        f.attrs['nvpar'] = self.nvpar
        f.attrs['nfpar'] = self.nfpar
        if self.comments is not None:
            self.comments = list(self.comments)
            for i, cc in enumerate(self.comments):
                f.attrs[f'Comment {i:02d}'] = cc

        # Energies
        egroup = f.create_group("ENERGIES", track_order=True)
        el = egroup.create_dataset("L", data=self.ebins[:, 0], dtype=np.float32)
        er = egroup.create_dataset("R", data=self.ebins[:, 1], dtype=np.float32)
        ec = egroup.create_dataset("C", data=(self.ebins[:, 0]*self.ebins[:, 1])**0.5, dtype=np.float32)
        el.make_scale('El')
        er.make_scale('Er')
        ec.make_scale('Ec')

        # Times
        tgroup = f.create_group("TIMES", track_order=True)
        tl = tgroup.create_dataset("L", data=self.tbins[:, 0], dtype=np.float32)
        tr = tgroup.create_dataset("R", data=self.tbins[:, 1], dtype=np.float32)
        tc = tgroup.create_dataset("C", data=(self.tbins[:, 0]*self.tbins[:, 1])**0.5, dtype=np.float32)
        tl.make_scale('Tl')
        tr.make_scale('Tr')
        tc.make_scale('Tc')

        # Fixed parameters -------------------------------------------------------------------------
        fpgroup = f.create_group("FIXED PARAMETERS", track_order=True)
        for name in self.fparams:
            fpgroup.attrs[name] = self.fparams[name]

        # Variable parameters ----------------------------------------------------------------------
        vpgroup = f.create_group("VARIABLE PARAMETERS", track_order=True)
        for name, par in self.vparams.items():
            vals = vpgroup.create_dataset(name, data=par.values, dtype=np.float32)
            vals.make_scale(name)

        # Additional comments  ---------------------------------------------------------------------
        com = [ss.encode("ascii", "ignore") for ss in self.comments]
        acgroup = f.create_dataset("ADDITIONAL COMMENTS", dtype='S100', data=com, track_order=True)

        for i, com in enumerate(self.comments):
            acgroup[i] = com

        # Parameters of all simulations files ------------------------------------------------------
        f.create_group("SIMULATIONS", track_order=True)

        # parameters of Monte Carlo simulations ----------------------------------------------------
        ifile = 0
        ind_list = [*product(*(range(par.nval) for par in self.vparams.values()))]
        for line, ind in enumerate(ind_list):
            for i, key in enumerate(self.vparams):
                self.vparams[key].value = self.vparams[key].values[ind[i]]
                self.vparams[key].index = ind[i]
            sfile = self.simupath+parse_filename(self.rparams)
            evc = EventCatalog(sfile, chatter=0, info_only=True)
            sp = f["SIMULATIONS"].create_group(f"file{ifile:02d}")
            for key, val in evc.params.items():
                sp.attrs[key] = np.float32(val)
            ifile = ifile+1
                
        # data set ---------------------------------------------------------------------------------
        shape = tuple([2]+[par.nval for par in self.vparams.values()] + [self.ntbins, self.nebins])
        data = f.create_dataset("DATA", shape, dtype=np.float32)
        f["DATA"][()] = 0.0
            
        # label and attach data axis ---------------------------------------------------------------
        for rank, name in enumerate(self.vparams):
            data.dims[rank].label = name
            data.dims[rank].attach_scale(vpgroup[name])
        data.dims[self.nvpar].label = 'Generation'
        data.dims[self.nvpar+1].label = 'Time'
        data.dims[self.nvpar+1].attach_scale(tgroup["C"])
        data.dims[self.nvpar+2].label = 'Energy'
        data.dims[self.nvpar+2].attach_scale(egroup["C"])

        # close file
        f.close()

    # ----------------------------------------------------------------------------------------------
    def fill_table(self, chunkfactor=4, nproc=1):
        """
        SYNOPSIS:
            This method fills the HDF5 file with data.
            Parallel computation is handled. Namely, all parameter sets of the table are split
            into subsets of parameters (aka chunks) that can be handled as independent tasks by
            different processors.
        ARGUMENTS:
            nproc: number of tasks (or processors)
            chunkfactor: the number of chunks is computed as nproc * chunkfactor
        RETURN:
            None
        """

        tstart = time()

        # All combinations of parameter sets (index values)
        ind_list = [*product(*(range(par.nval) for par in self.vparams.values()))]
        n = len(ind_list)
        nproc = np.max([nproc, 1])

        if n == 0:
            chunksize = 0
            chunknumber = 0
        else:
            if nproc == 1:
                chunksize = n
                chunknumber = 1
            else:
                chunknumber = np.min([nproc * chunkfactor, n])
                chunksize, extra = divmod(n, chunknumber)
                if extra:
                    chunksize += 1
                    chunknumber -= 1

        print(f"Number of parameter sets:   {n}", flush=True)
        print(f"Number of tasks:            {nproc}", flush=True)
        print(f"Approximate size of chunks: {chunksize}", flush=True)
        print(f"Number of chunks:           {chunknumber}", flush=True)
        print("---------------------------------------------------")
        print("THREAD    PID      SET        TIME        ACTION")
        print("---------------------------------------------------")

        def init(llock1, llock2):
            global lock1, lock2
            lock1 = llock1
            lock2 = llock2

        # Build pool and run processes
        set_start_method('fork', force=True)
        l1 = Lock()
        l2 = Lock()
        p = Pool(processes=nproc, initializer=init, initargs=(l1, l2,))
        res = p.starmap(func=self.one_process, iterable=enumerate(ind_list), chunksize=chunksize)

        # store results to file
        f = H5file(self.file, 'a')
        for iset, (primary, secondary) in enumerate(res):
            f["DATA"][(0, *ind_list[iset], ...)] = primary
            f["DATA"][(1, *ind_list[iset], ...)] = secondary
        f.close()

        # Substitute the primary spectrum of all sets with the average one
        self.primary_average()

        # Display completion message
        if self.chatter > 0:
            tend = time()
            dt = ftime(tend - tstart)
            print(f"All tasks completed within {dt[0]:.1f} {dt[1]}", flush=True)

    # ----------------------------------------------------------------------------------------------
    def primary_average(self):
        """
        SYNOPSIS:
            Average primary spectra over all the different simulation files used in this table. As
            the primary spectrum does not depend on the IGMF properties, this improves the
            numerical Monte Carlo statistics with no loss of physical accuracy.
            Secondary spectra are left unchanged for they depend on the IGMF properties.
        ARGUMENTS:
        RESULT:
        """
        
        ind = [ipar for ipar, name in enumerate(self.vparams) if name in ['logB', 'lambdaB']]
        f = H5file(self.file, 'a')
        f["DATA"][(0, ...)] = np.mean(f["DATA"][(0, ...)], axis=tuple(ind), keepdims=True)
        f.close()

    # ----------------------------------------------------------------------------------------------
    def one_process(self, iset, ind):
        """
        SYNOPSIS:
            This method compute the data associated to one single parameter set
        ARGUMENTS:
            iset:       index of this parameter set
            ind:        list of indices for one parameter set in the current chunk.
        RETURN:
            primary:    Numerical array of primary photons (for all energy and time bins).
            secondary:  Numerical array of secondary photons (for all energy and time bins).
        """

        # Log at the beginning of computation
        if self.chatter > 0:
            p = current_process()
            self.loginfo(int(p.name.split('-')[-1]), p.pid, iset, 'started', lock1)

        # convert list to array
        ind = np.asarray(ind)

        # work with local copies of the class attributes, to make sure it does not mess up with
        # parallel processes
        smodel = deepcopy(self.smodel)
        vparams = deepcopy(self._vparams)
        rparams = deepcopy(self.rparams)

        # Set all parameter indices to force file reading at the very first parameter set in the
        # current chunk
        for par in vparams.values():
            par.index = -1

        # only keep the first ngen generations
        gen = range(self.ngen)

        # update parameter values
        for i, key in enumerate(vparams):
            vparams[key].value = vparams[key].values[ind[i]]
            vparams[key].index = ind[i]
        for key in rparams:
            if key in vparams:
                rparams[key] = vparams[key].value

        # Read new file for every set of parameter (although some time could be spared by reading the file
        # only when a new logB is passed, the chosen strategy is less memory demanding)
        sfile = self.simupath+parse_filename(rparams)
        evc = EventCatalog(sfile, chatter=self.chatter-1, fixt=True, gen=gen,
                           emin=self.ebins[0, 0], emax=self.ebins[-1, 1], tmax=self.tbins[-1, 1], lock=lock2)

        # Apply Model and bin data (with copy=False, the EventCatalog "evc" is modified and must be
        # computed again at the next parameter set)
        smodel.update({par.name: par.values[ind[rank]] for rank, par in enumerate(vparams.values())})
        bd = BinnedData(evc, smodel=smodel, gbins=gen, tbins=self.tbins, ebins=self.ebins,
                        chatter=self.chatter-1, copy=False).data

        # Log at the end of computation
        if self.chatter > 0:
            p = current_process()
            self.loginfo(int(p.name.split('-')[-1]), p.pid, iset, 'completed', lock1)

        # return primary and secondary spectra (array dimensions are time and energy)
        return bd[0, :, :], np.sum(bd[1:, :, :], axis=0)

    @staticmethod
    def loginfo(thr, pid, iset, action, llock):
        """
        Display information in the current computation
        """
        llock.acquire()
        print(f"  {thr:3d}  {pid:7d}   {iset:6d}   {time():.1f}   {action:10s}", flush=True)
        llock.release()

    # ----------------------------------------------------------------------------------------------
    def read_table(self):
        """
        SYNOPSIS:
            This method reads data from a table model, HDF5 file and updates the class instance.
        ARGUMENTS:
        RETURN:
        """

        if isinstance(self.file, H5file):
            h5f = self.file
        elif isinstance(self.file, str):
            h5f = H5file(self.file, 'r', track_order=True)
        else:
            raise ValueError("File must be either a filename (str) or an opened hdf5 file object")

        self.ebins = np.array([h5f['ENERGIES/L'], h5f['ENERGIES/R']]).T
        
        self.tbins = np.array([h5f['TIMES/L'], h5f['TIMES/R']]).T

        self.data = h5f['DATA']

        hp = h5f['VARIABLE PARAMETERS']
        dic = {}
        for rank in range(len(self.data.dims)-3):
            name = self.data.dims[rank].label
            dic[name] = hp[name][()]
        self.vparams = dic
        
        fparams = h5f['FIXED PARAMETERS']
        fparams = {key: fparams.attrs[key] for key in fparams.attrs.keys()}
        if fparams['Atype'] == 'Iso':
            amodel = IsotropicAngularModel()
        elif fparams['Atype'] == 'Disk':
            amodel = DiskAngularModel()
        else:
            raise ValueError(f"Unknown angular model: {fparams['Atype']}")
        if 'SourceModel' in h5f.attrs:
            smodel_name = h5f.attrs['SourceModel']
        else:
            smodel_name = 'SeparableSourceModel'
        if smodel_name == "SeparableSourceModel":
            if fparams['Ttype'] == 'Pulse':
                tmodel = PulseTimeModel()
            elif fparams['Ttype'] == 'PL':
                tmodel = PowerLawTimeModel()
            elif fparams['Ttype'] == 'BPL':
                tmodel = BrokenPowerLawTimeModel()
            elif fparams['Ttype'] == 'TPL':
                tmodel = TemplateTimeModel(time=[0], flux=[0])
            else:
                raise ValueError(f"Unknown time model: {fparams['Ttype']}")
            if fparams['Etype'] == 'PL':
                emodel = PowerLawSpectralModel()
            elif fparams['Etype'] == 'CPL':
                emodel = CutoffPowerLawSpectralModel()
            elif fparams['Etype'] == 'LPB':
                emodel = LogParabolaSpectralModel()
            elif fparams['Etype'] == 'BPL':
                emodel = BrokenPowerLawSpectralModel()
            elif fparams['Etype'] == 'TPL':
                emodel = TemplateSpectralModel(energy=[0], flux=[0])
            else:
                raise ValueError(f"Unknown spectral model: {fparams['Etype']}")
            smodel = SourceModel(tmodel=tmodel, emodel=emodel, amodel=amodel)
        elif smodel_name == "NonSeparableSourceModel":
            smodel = NonSeparableSourceModel(energy=fparams['energy'], time=fparams['time'], flux=fparams['flux'], amodel=amodel)
        else:
            raise ValueError(f"Unknown source model: {fparams['SourceModel']}")
        smodel.update(fparams)
        self.smodel = smodel
        self._fparams.update(fparams)
        
        comm = h5f['ADDITIONAL COMMENTS']
        self.comments = comm[()].astype(str)

        if isinstance(self.file, str):
            self.data = self.data[()]
            h5f.close()

    # ----------------------------------------------------------------------------------------------
    def spectra(self, xlim=(1.e0, 1.e4), ylim=(1.e-13, 1.e-5)):
        """
        SYNOPSIS:
            This method plots the spectra of table model using an interactive widget with sliders
            for all parameters.
        ARGUMENTS:
            xlim,ylim:  x- and y- range for ploting
        RETURN:
            None
        """
    
        e = np.sqrt(np.prod(self.ebins, axis=1))
        t = np.sqrt(np.prod(self.tbins, axis=1))

        lpad = 0.02
        rpad = 0.03
        tpad = 0.03
        bpad = 0.02
        hax = 0.1
        vax = 0.06
        sldw = 0.1

        fig = plt.figure()

        # Define the main plot area and plot a default spectrum
        left = lpad + self.nvpar*sldw + hax
        bottom = tpad+sldw+hax
        right = 1-rpad
        top = 1-tpad
        ax = fig.add_axes((left, bottom, right-left, top-bottom))

        # Define sliders for time and variable paramers
        ind = np.zeros(self.nvpar+1, dtype='i')
        sliders = []
        axes = []
        txtb = []
        for i, (name, par) in enumerate(self.vparams.items()):

            # Parameter, vertical sliders
            left = lpad+i*sldw
            bottom = bpad + hax
            axes.append(fig.add_axes((left, bottom, sldw, 1-vax-tpad-bottom)))
            sliders.append(Slider(ax=axes[-1],
                                  orientation="vertical",
                                  valstep=1,
                                  label=name,
                                  valmin=0,
                                  valmax=par.nval-1,
                                  valinit=0))
            ind[i] = 0
            txtb.append(axes[-1].text(lpad+sldw/2, -0.1, f"{par.values[0]:^8.2f}", transform=axes[-1].transAxes))
            
        left = lpad + self.nvpar*sldw + hax
        bottom = bpad
        axes.append(fig.add_axes((left, bottom, 1-rpad-left-0.01, sldw)))
        sliders.append(Slider(ax=axes[-1],
                              orientation="horizontal", valstep=1,
                              label='Time',
                              valmin=0,
                              valmax=len(t)-1,
                              valinit=0))

        y0 = self.data[(0, *ind, ...)]
        line1, = ax.plot(e/GeV, e**2*y0, ls='-')
        y1 = self.data[(1, *ind, ...)]
        line2, = ax.plot(e/GeV, e**2*y1, ls='--')
        ax.set_title('Time')

        # Function to be called anytime a slider's value changes in order to update the plot
        def update(val):
            iind = [s.val for s in sliders]
            line1.set_ydata(e**2*self.data[(0, *iind, ...)])
            line2.set_ydata(e**2*self.data[(1, *iind, ...)])
            for ii, (nname, ppar) in enumerate(self.vparams.items()):
                txtb[ii].set(text=f"{ppar.values[sliders[ii].val]:^8.2f}")
            fig.canvas.draw_idle()
        for i in range(self.nvpar+1):
            sliders[i].on_changed(update)

        ax.set_xscale('log')
        ax.set_yscale('log')
        ax.set_xlim(xlim)
        ax.set_ylim(ylim)
        ax.set_xlabel('Energy (GeV)')
        ax.set_ylabel('EFE (erg/s/cm2)')
        plt.show()

    def lightcurves(self, xlim=None, ylim=(1.e-14, 1.e-7)):
        """
        SYNOPSIS:
            This method builds and opens a widget to visualize the lightcurve of the current model at
            all energies.
        ARGUMENTS:
            xlim:   tuple of plot x-boundaries
            ylim:   tuple of plot y-boundaries
        """

        # get energy, and parameter Map axes
        e = np.sqrt(np.prod(self.ebins, axis=1))

        # number of time bins
        tt = np.sqrt(np.prod(self.tbins, axis=1))
        if xlim is None:
            xlim = self.tbins[0, 0], self.tbins[-1, 1]

        # Plot margins
        lpad = 0.02
        rpad = 0.03
        tpad = 0.03
        bpad = 0.02
        hax = 0.1
        vax = 0.06
        sldw = 0.1

        fig = plt.figure()

        # Define the main plot area and plot a default spectrum
        left = lpad + self.nvpar*sldw + hax
        bottom = tpad+sldw+hax
        right = 1-rpad
        top = 1-tpad
        ax = fig.add_axes((left, bottom, right-left, top-bottom))

        # Define sliders for time and variable parameters
        ind = np.zeros(self.nvpar, dtype='i')
        ie = int(len(e)/2)
        sliders = []
        axes = []
        txtb = []
        for i, (name, par) in enumerate(self.vparams.items()):

            left = lpad+i*sldw
            bottom = bpad + hax
            axes.append(fig.add_axes((left, bottom, sldw, 1-vax-tpad-bottom)))
            sliders.append(Slider(ax=axes[-1],
                                  orientation="vertical",
                                  valstep=1,
                                  label=name,
                                  valmin=0,
                                  valmax=par.nval - 1,
                                  valinit=0))

            ind[i] = 0
            txtb.append(axes[-1].text(lpad + sldw / 2, -0.1, f"{par.values[0]:^8.2f}", transform=axes[-1].transAxes))

        # Horizontal slider for energy
        left = lpad + self.nvpar*sldw + hax
        bottom = bpad
        axes.append(fig.add_axes((left, bottom, 1-rpad-left-0.01, sldw)))
        sliders.append(Slider(ax=axes[-1], orientation="horizontal", valstep=1,
                              label='Energy',
                              valmin=0,
                              valmax=len(e)-1,
                              valinit=ie))

        # Default plot
        y0 = e[ie]**2 * np.array([self.data[(0, *ind, it, ie)] for it in range(len(tt))])
        line1, _, bar1 = ax.errorbar(tt, y0, xerr=[tt-self.tbins[:, 0], self.tbins[:, 1]-tt], ls='', marker='o')
        y0 = e[ie]**2 * np.array([self.data[(1, *ind, it, ie)] for it in range(len(tt))])
        line2, _, bar2 = ax.errorbar(tt, y0, xerr=[tt-self.tbins[:, 0], self.tbins[:, 1]-tt], ls='', marker='o')
        ax.set_title = 'Energy'

        # Function to be called anytime a slider's value changes in order to update the plot
        def update(val):

            iie = sliders[-1].val
            iind = [s.val for s in sliders[:-1]]
            for ii, (nname, ppar) in enumerate(self.vparams.items()):
                txtb[ii].set(text=f"{ppar.values[sliders[ii].val]:^8.2f}")

            y = e[iie] ** 2 * np.array([self.data[(0, *iind, it, iie)] for it in range(len(tt))])
            line1.set_ydata(y)
            bar1[0].set_segments(zip(zip(self.tbins[:, 0], y), zip(self.tbins[:, 1], y)))

            y = e[iie] ** 2 * np.array([self.data[(1, *iind, it, iie)] for it in range(len(tt))])
            line2.set_ydata(y)
            bar2[0].set_segments(zip(zip(self.tbins[:, 0], y), zip(self.tbins[:, 1], y)))

            fig.canvas.draw_idle()

        for i in range(self.nvpar+1):
            sliders[i].on_changed(update)

        ax.set_xscale('log')
        ax.set_yscale('log')
        ax.set_xlim(xlim)
        ax.set_ylim(ylim)
        ax.set_xlabel('Time (s)')
        ax.set_ylabel(r'$F_E$ (erg/s/cm2)')
        plt.show()
# ==================================================================================================


# ==================================================================================================
def time_bins(pointing_time=60, night_start=1*hour, n_nights=4, night_duration=5*hour, nbins=4):
    """
    SYNOPSIS:
        Compute time bins
        - increasing with time to get a constant number of counts for a powerlaw time decay with
          index -1
        - coherent with night observations only.
    ARGUMENTS:
        pointing_time:  starting time of observation after burst start (default 1min)
        night_start:    starting time when night is dark enough for observations (can be negative
                        if the burst occurs at night)
        n_nights:       number of observation nights
        night_duration: duration of the night (default 5*hour)
        nbins:          number of time bins
    RETURN:
        tbins:          array of all left/right time bins edges
    """
# ==================================================================================================

    # Define the time bin array
    tbins = np.zeros((nbins, 2))

    # start observation time (with respect to burst)
    t1 = np.max([pointing_time, night_start])

    # If the night duration is set to more than 24h, then do not use any day/night constraint
    # and define contiguous time bins
    if night_duration >= 24*hour:
    
        t2 = t1 + n_nights*day
        tb = np.logspace(np.log10(t1), np.log10(t2), nbins+1)
        tbins[:, 0] = tb[:-1]
        tbins[:, 1] = tb[1:]

    # Otherwise define time bins only during nighttime.
    else:
    
        r = ((night_duration+night_start)/t1)**(1.0/nbins)

        tnmax = 0
        ok = False
        while not ok:

            tbins[:, 0] = t1
            tbins[:, 1] = t1*r

            inight = 0
            for it in range(1, nbins):
                
                tnmin = night_start + inight*day
                tnmax = tnmin + night_duration
                
                # If still nighttime: start a new bin
                if tbins[it-1, 1] < tnmax:
                    tbins[it, 0] = tbins[it-1, 1]
                    
                # If not: truncate the current bin to the night end and start a new bin at the
                # following night
                else:
                    tbins[it-1, 1] = tnmax
                    inight += 1
                    tnmin = night_start + inight*day
                    tnmax = tnmin + night_duration
                    tbins[it, 0] = tnmin

                # Close the bin
                tbins[it, 1] = tbins[it, 0] * r

            # Truncate the last bin at the night end
            tbins[-1, 1] = np.min([tbins[-1, 1], tnmax])
    
            ok = tnmax > (n_nights-1)*day
            r = r*1.01

    return tbins
    
    
# ==================================================================================================
def view_tbins(pointing_time=60, night_start=1*hour, n_nights=4, night_duration=5*hour, nbins=4):
    """
    SYNOPSIS:
        This routine defines times bins (passing arguments to the time_bins) routine, plot them and
        overplot the nigh time
    ARGUMENTS:
        pointing_time:  starting time of observation after burst start (default 1min)
        night_start:    starting time when night is dark enough for observations (can be negative
                        if the burst occurs at night)
        n_nights:       number of observation nights
        night_duration: duration of the night (default 5*hour)
        nbins:          number of time bins
    RETURN:
        None
    """
# ==================================================================================================

    t1, t2 = time_bins(pointing_time=pointing_time, n_nights=n_nights, night_start=night_start,
                       night_duration=5*hour, nbins=nbins).T

    fig, ax = plt.subplots()

    # plot nights
    for inight in range(n_nights+1):
        ax.add_patch(Rectangle((night_start+inight*day, -2), night_duration, 4, facecolor='k',
                               fill=True, lw=5, alpha=0.5))

    # plot obsevation bins
    for it, tt1 in enumerate(t1):
        ax.add_patch(Rectangle((tt1, -1), t2[it]-tt1, 2, edgecolor='k', facecolor='blue', fill=True, lw=1, alpha=1))

    plt.xscale('log')
    plt.ylim(-2, 2)
    t1 = pointing_time
    if night_start > 0:
        t1 = np.min([t1, night_start])
    plt.xlim(t1, day*n_nights)
    plt.xlabel('Time (s)')
    plt.title('Time bins and night time')
    plt.tick_params(left=False, labelleft=False)
    plt.show()
# ==================================================================================================


# ==================================================================================================
class _Chunk:
    """
    SYNOPSIS:
        Simple class aiming at checking the progress of a table computation.
        It contains the properties of a single chunk of cascades computed in the table.

    ARGUMENTS:
        cid (int):       chunk id
        start (float):  start time
        end (float):    end time

    ATTRIBUTES:
        cid (int):       chunk id
        start (float):  start time
        end (float):    end time
    PROPERTIES:
        duration (property): time to process this chunk
    """
    def __init__(self, cid=None, start=None, end=None):
        self.cid = cid
        self.start = start
        self.end = end

    @property
    def duration(self):
        if self.end is None:
            return None
        else:
            return self.end - self.start

# ==================================================================================================


# ==================================================================================================
class Logf:
    """
    SYNOPSIS:
        Class aiming at checking the progress of a table computation.
        Each table required to compute the cascade for a large number of different parameter sets (nset).
        The sets gathered in chunks (nchunk)
        The chunks are distributed over several tasks a.k.a. processes (ntask)
    ARGUMENTS:
        logfile: name of the log file to be analysed

    ATTRIBUTES:
        nset (int):         Total number of sets used in the current run
        nchunk (int):       Total number of chunks used in the current run
        ntask (int):        Total number of tasks used in the current run

    PROPERTIES
        tasklist: list of all pids in the current run

    EXAMPLES:
        from cascapy.tablemodel import Logf
        # Instantiate the report
        lf = Logf()
        # Display the full report
        lf.report(chatter=2)
    """

    def __init__(self, logfile='table.log'):

        f = open(logfile, 'r')

        lines = f.readlines()
        f.close()
        self.nset = int(lines[0].strip().split(":")[1])
        self.ntask = int(lines[1].strip().split(":")[1])
        self.chunksize = int(lines[2].strip().split(":")[1])
        self.nchunk = int(lines[3].strip().split(":")[1])
        self._tasks = {}
        self.complete = None

        for line in lines[7:]:

            sline = line.strip().split()

            try:
                pid, _, iset, etime, action = sline

            except ValueError:
                self.complete = line

            else:
                pid = int(pid)
                iset = int(iset)
                etime = float(etime)
                action = action.strip()

                if pid not in self._tasks:

                    if action == 'started':
                        self._tasks[pid] = {iset: _Chunk(cid=iset, start=etime)}
                    else:
                        raise ValueError(f"action not understood: {action}", pid)

                else:

                    if iset not in self._tasks[pid]:
                        if action == 'started':
                            self._tasks[pid][iset] = _Chunk(cid=iset, start=etime)
                        else:
                            raise ValueError(f"action not understood: {action}", pid)

                    else:

                        if action == 'completed':
                            self._tasks[pid][iset].end = etime
                        else:
                            raise ValueError(f"action not understood: {action}", pid)

    @property
    def tasklist(self):
        return [key for key in self._tasks]

    def taskreport(self, pid, chatter=0):

        dur_ave = 0.0
        if pid in self._tasks:

            nset = len([ch for ch in self._tasks[pid] if self._tasks[pid][ch].end is not None])

            if chatter > 0:
                print("---------------------------------------")
                print(f"Report for Task #{pid}")
                if chatter > 1:
                    print("   Set        start time       duration")

            iset = 0
            for iset in self._tasks[pid]:

                if self._tasks[pid][iset].duration is None:
                    dur = None
                else:
                    dur = float(self._tasks[pid][iset].duration)
                    dur_ave = dur_ave + dur

                if chatter > 1:
                    text = f"{iset:6d}  "
                    text += f"  {strftime('%D %T',localtime(self._tasks[pid][iset].start))}  "
                    if dur is None:
                        text += "   ------ "
                    else:
                        tt = ftime(dur)
                        text += f"  {tt[0]:^5.1f} {tt[1]:4s}"
                    print(text)

            if chatter > 0:
                print(f"   Number of completed sets: {nset}")
                print(f"   Status: {'processing' if self._tasks[pid][iset].end is None else 'available'}")

        else:

            raise ValueError(f"{pid} not found in {self.tasklist}")

        return nset, dur_ave

    def report(self, chatter=0):

        nset = 0
        tcpu = 0.0

        for pid in self.tasklist:
            n, t = self.taskreport(pid, chatter=chatter)
            nset += n
            tcpu = tcpu + t

        print("---------------------------------------")
        print(f"Total number of sets:   {self.nset}")
        print(f"Total number of tasks:  {self.ntask}")
        print(f"Total number of chunks: {self.nchunk}")
        print(f"Chunk size:             {self.chunksize}")
        print(f"Number of sets processed: {nset} => {int(100*nset/self.nset)}%")
        if nset > 0:
            print(f"Average cpu time per set: {tcpu/nset:.2f}")
            t_est = ftime(tcpu/nset*(self.nset-nset)/self.ntask)
            if t_est[0] > 0:
                print(f"Estimated remaining time: {t_est[0]:.1f} {t_est[1]}")
            else:
                print(self.complete.strip())
        print("---------------------------------------")
# ==================================================================================================


# ==================================================================================================
if __name__ == '__main__':
    pass
