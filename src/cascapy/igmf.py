"""
SYNOPSIS:
    This module defines a class describing the InterGalactic Magnetic Field (IGMF).
    It only aims at making some analytical estimates

DEPENDENCIES
    numpy

META:
    author: R. Belmont
    date:   16/05/24
"""

# Public packages
import numpy as np

# Current package
from .constants import mc2, ec, Mpc
from .interactions import Compton


# ==================================================================================================
# IGMF class
# ==================================================================================================
class IGMF:
    """
    SYNOPSIS:

    ARGUMENTS:
        b0:         magnetic field intensity at z=0 (in G)
        lb0:        magnetic coherence length at z=0 (in cm)
        average:    if True, b0 is divided by sqrt(3) to account for average over  random orientation

    ATTRIBUTES:
        b0:     magnetic field intensity at z=0 (in G)
        lb0:    magnetic coherence length at z=0 (in cm)
        r0:     non-relativistic Larmor radius at z=0 (i.e. also comoving) (in cm)
        comp:   instance of the Compton class

    EXAMPLES:
    """

    def __init__(self, b0=1.e-19, lb0=1 * Mpc, average=False):
        
        # Intensity at present time
        if average:
            self.b0 = b0 * np.sqrt(2./3.)
        else:
            self.b0 = b0
        
        # Coherence length at present time
        self.lb0 = lb0
        
        # Non-relativistic Larmor radius at z=0 (a.k.a. comoving non-relativistic Larmor radius)
        self.r0 = mc2 / ec / self.b0
        
        # Compton is required to compute the spiraling trajectory due to Compton cooling
        self.comp = Compton()

    def rl(self, energy_e, z):
        """
        SYNOPSIS:
            Comoving relativistic Larmor radius (in the limit v=c). This quantity depends on lepton
            energy and magnetic field, both evolving with cosmic time. However, The comoving larmor
            radius does not depend on cosmic time.
        ARGUMENTS:
            energy_e:  local lepton energy (in the local frame, in erg)
            z:   local redshift
        RETURN:
            RL: Comoving Larmor radius (in cm)
        """
        ge0 = energy_e / mc2 / (1 + z)     # Energy at z=0
        return ge0 * self.r0

    def delta_x(self, x, energy0_e, z0):
        """
        SYNOPSIS:
            Deflection angle as a function of distance from the production site, assuming the
            universe expansion is negligible on the time required to travel that distance.
        ARGUMENTS:
            x:   proper distance travelled since t0
            energy0_e: Local lepton energy at the production site (in the local frame, in erg)
            z0:  redshift at production site
        RETURN:
            delta:  deflection angle (in rad)
        """

        # Compton cooling distance
        lic0 = self.comp.lcoolic(energy0_e, z0)

        # Proper Larmor radius at comoving site
        rp = self.rl(energy0_e, z0) / (1 + z0)

        # Ballistic deflection (assuming no expansion on the travel timescale)
        delta = x/rp * (1.0 + 0.5*x/lic0)

        # Account for small coherence length (ah-hoc transition between the two regimes)
        lb = self.lb0 / (1 + z0)
        delta = delta / (1 + x/lb)**0.5

        return delta
        
    def delta_energy_e(self, energy_e, energy0_e, z0):
        """
        SYNOPSIS:
            Deflection angle as a function of lepton energy, assuming the universe expansion is
            negligible on the time required to cool down to that energy.
        ARGUMENTS:
            energy_e:  Lepton energy (in the local frame, in erg)
            energy0_e: Local lepton energy at the production site (in the local frame, in erg)
            z0:  redshift at production site
        RETURN:
            delta:  deflection angle (in rad)
        """

        # Compton cooling distance
        lic0 = self.comp.lcoolic(energy0_e, z0)

        # Proper Larmor radius at comoving site
        rp = self.rl(energy0_e, z0) / (1 + z0)

        # Ballistic deflection (assuming no expansion on the travel timescale)
        delta = lic0 / 2 / rp * ((energy0_e / energy_e) ** 2 - 1.0) * (energy0_e > energy_e)
        
        # Account for small coherence length (ah-hoc transition between the two regimes)
        x = lic0 * (energy0_e / energy_e - 1.0)
        x = x * (x > 0)
        lb = self.lb0 / (1 + z0)
        delta = delta / (1 + x/lb)**0.5

        return delta

    def delta_energy_ic(self, energy_ic, energy0_e, z0):
        """
        SYNOPSIS:
            Deflection angle as a function of scattered photon energy, assuming the universe
            expansion is negligible on the time required to cool down to that energy.
        ARGUMENTS:
            energy_ic: scattered photon energy (in the OBSERVER frame, in erg)
            energy0_e: Local lepton energy at the production site (in the local frame, in erg)
            z0:  redshift at production site
        RETURN:
            delta:  deflection angle (in rad)
        """
        energy_e = self.comp.ee_eic(energy_ic * (1 + z0), z0)
        return self.delta_energy_e(energy_e, energy0_e, z0)

    def lbc(self, energy_ic, energy0_e, z0):
        """
        SYNOPSIS:
            Transition, comoving coherence length between the ballistic and the diffusive regime as
            a function of the observed energy of secondary photons
        ARGUMENTS:
            energy_ic:    energy of the secondary photons as observed on Earth (in erg)
            energy0_e:    Lepton energy in the local frame (in erg)
            z0:     redshift at the production site
        RETURN:
            LB:     transition comoving coherence length (in cm)
        """

        # Compton cooling distance
        lic0 = self.comp.lcoolic(energy0_e, z0)

        # criterion: x = lB => ballistic for lb>x
        energy_e = self.comp.ee_eic(energy_ic * (1 + z0), z0)
        lb1 = lic0 * (energy0_e / energy_e - 1.0) * (1 + z0)
        
        # criterion: RL(Ee0) = lB => ballistic for lb>RL
        # This criterion is barely relevant...
        # lB2 = self.RL(Ee0,z0)

#        return np.min([lB1,lB2])
        return lb1

# def delay(self,z,delta,dta):
#    Dc = self.Dc(z)         # source comoving distance
#    te = self.t_z(z)        # primary emission time
#    ta = te + dta           # production time
#    xa = self.xc(te,ta)     # comoving annihilation distance to the source
#    w = xa*np.sin(delta)/Dc # w = sin(theta)
#    if (w>1.e-10):
#        Xc = Dc * np.sqrt( 1 - w**2 ) - xa*np.cos(delta) # comoving distance to the production site
#    else:
#        Xc = Dc * (1 - w**2/2 )  - xa*np.cos(delta)
#        print(ta,Xc/cl,xa/Dc,w,np.sin(delta),delta)
#    dT = self.dt0(ta,Xc)
#    return dta + dT + te


if __name__ == "__main__":
    pass
