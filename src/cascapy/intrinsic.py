"""
SYNOPSIS
    This module defines classes to describe the spectral, time, and angular properties of the source
    intrinsic emission. There are 3 separated parent classes for these properties. Each specific model
    type (e.g. a powerlaw) is then defined as specific children class derived from one of the formers.
    In the current version there is also a global `Source` class combining spectral, time and angular
    properties assuming that these properties are independent of each other (e.g. no spectral evolution
    with time) and constructed as the product of the three.
    All quantities (energy, time) are meant at z=0.
    In this module, some reference models are also build for a few known, known GRB sources.

DEPENDENCIES:
    numpy, scipy, astropy
    
META:
    author: R. Belmont
    date:   5/12/24
"""

# Generic packages
import numpy as np
from scipy.integrate import quad as quad
from scipy.interpolate import interp1d
from astropy.coordinates import SkyCoord
from astropy.time import Time

# Current packages
from .constants import keV, MeV, GeV, TeV, degree, hour
from .backgrounds import EBL
from .cosmo import LCDM

# module items available though 'from Source import *'
__all__ = ["TimeModel", "PowerLawTimeModel", "PulseTimeModel", "BrokenPowerLawTimeModel",
           "RelativeBrokenPowerLawTimeModel", "LogParabolaTimeModel", "TemplateTimeModel",
           "SpectralModel", "PowerLawSpectralModel", "CutoffPowerLawSpectralModel", "LogParabolaSpectralModel",
           "BrokenPowerLawSpectralModel", "TemplateSpectralModel",
           "AngularModel", "IsotropicAngularModel", "DiskAngularModel",
           "SourceModel", "NonSeparableSourceModel",
           "GRB130427A", "GRB190114C", "GRB190829A", "GRB180720B", "GRB221009A"]


# =================================================================================================
#                                      TIME MODELS
# =================================================================================================


# =================================================================================================
class TimeModel:
    """
    SYNOPSIS:
        This class is a generic class for models of time evolution of a variable source.
        Times are in seconds.
        
        Any precise time model requires to define a derived class for that model, in which at least
        one method must be OVERRIDDEN:
        self.f(t), the model at time t
        
        Also, when an analytical evaluation of the time integrated model between t1 and t2 is available,
        it is faster to also override self.fint(t1,t2). Otherwise, a default numerical computation of
        the integral is performed element by element which is very time-consuming.
        WARNING: when a deferred self.fint() method is defined, it must be written in a way so that
        it can handle numpy arrays of t1 and t2.
        
        In derived classes, all parameters must be stored in the _params dictionary attribute.
        In addition, this dictionary must have 'Ttype' keyword defining a short name for that
        derived model.
        
        So far, 5 derived class have been defined:
            - PulseTimeModel()
            - PowerLawTimeModel(alpha,tmin,tmax)
            - FlatTimeModel(tmin,tmax)
            - BrokenPowerLawTimeModel(alpha,ti)
            - TemplateTimeModel(time,flux)

    INSTANTIATION ARGUMENTS:
        None

    ATTRIBUTES:

    EXAMPLES:
        import numpy as np
        from cascapy.intrinsic import TimeModel

        # The following derived class defines a time model with f(t) = A + 1/t^1, where A is the
        # only model parameter
        class MyTimeModel(TimeModel):
            def __init__(self, A=0.0):
                TimeModel.__init__(self)
                self.params.update({'Ttype': 'mymodel', 'A': A})

            def f(self, t):
                return self.params['A'] + 1.0 / t ** 2

            def fint(self, t1i, t2i):
                t1 = np.array(t1i, copy=True, dtype=float, ndmin=1)
                t2 = np.array(t2i, copy=True, dtype=float, ndmin=1)
                F = np.zeros_like(t1)
                ind = (t2 > t1)
                F[ind] = self.params['A'] * (t2[ind] - t1[ind]) - (1.0 / t2[ind] - 1.0 / t1[ind])
                return np.squeeze(F)[()]


        Tmodel = MyTimeModel(A=3)
        Tmodel.info()
        print("f(1)      = ", Tmodel.f(1))
        print("fint(1,2) = ", Tmodel.fint(1, 2))
        print("fave(1,2) = ", Tmodel.fave(1, 2))    """

    def __init__(self):
        self._params = {'Ttype': 'DefaultContainer'}
    
    @property
    def params(self):
        return self._params
            
    def update(self, pars):
        """
        SYNOPSIS:
            Update the model parameters with a dictionary of new parameters values
        ARGUMENTS:
            pars: a dictionary of parameter names+values
        """
        for key, val in pars.items():
            if key in self._params:
                self._params[key] = val
        
    def f(self, t):
        """
        DEFERRED METHOD !!!
        SYNOPSIS:
            Computes and returns the model at time t
            The default flux is zero in a parent class. This class method must be OVERRIDDEN in any
            child class to reproduce the desired flux.
        ARGUMENTS:
            t: time in s
        RETURN:
            f: flux
        """
        return 0.0

    def fint(self, t1i, t2i):
        """
        SYNOPSIS:
            Computes and returns the flux integrated between times t1 and t2
        ARGUMENTS:
            t1, t2: times in s. Scalars or arrays of the same size...
        RETURN:
            fint: time integrated flux
        """
        t1 = np.array(t1i, copy=False, dtype=float, ndmin=1)
        t2 = np.array(t2i, copy=False, dtype=float, ndmin=1)
        fi = np.zeros_like(t1)
        ind = np.where(t2 > t1)[0]
        for ii in ind:
            fi[ii] = quad(self.f, t1[ii], t2[ii], full_output=True)[0]
        return np.squeeze(fi)[()]

    def fave(self, t1, t2):
        """
        SYNOPSIS:
            Computes and returns the flux average between times t1 and t2
        ARGUMENTS:
            t1, t2: times in s. Scalars or arrays of the same size...
        RETURN:
            fint: time integrated flux
        """
        return self.fint(t1, t2)/(t2-t1)
    
    def info(self):
        """
        Display some information on the time model
        """
        print(f"   Time model:")
        for key, val in self.params.items():
            print(f"   {key:15} = {val:>8}")


# =================================================================================================
class PulseTimeModel(TimeModel):
    """
    SYNOPSIS:
        This class defines a delta(t) function time model, the flux of which is zero any time but at
        t=0.

    INSTANTIATION ARGUMENTS:
        None

    EXAMPLE:
        from cascapy.intrinsic import PulseTimeModel
        Tmodel = PulseTimeModel()
        print("f(0) = ",Tmodel.f(0.))
        print("f(1) = ",Tmodel.f(1.))
        print("fint(0,0) = ",Tmodel.fint(0.,0.))
        print("fint(-1,1) = ",Tmodel.fint(-1,1))
        print("fint(1,2) = ",Tmodel.fint(1,2))
    """

    def __init__(self):
        TimeModel.__init__(self)
        self.params.update({'Ttype': 'Pulse'})

    def f(self, t):
        return 1.*(t == 0.)
        
    def fint(self, t1, t2):
        return 1.*((t1 <= 0) & (t2 >= 0))


# =================================================================================================
class PowerLawTimeModel(TimeModel):
    """
    SYNOPSIS:
        This class defines a powerlaw time model for tmin<t<tmax: N = t^(-alpha)

    INSTANTIATION ARGUMENTS:
        alpha:      index of the powerlaw
        tmin,tmax:  minimal and maximal times

    EXAMPLE:
        import numpy as np
        import matplotlib.pyplot as plt
        from cascapy.intrinsic import PowerLawTimeModel

        # A first model
        Tmodel = PowerLawTimeModel(alpha=1.5, tmin=15, tmax=1.e4)
        Tmodel.info()
        print("f(1)      = ", Tmodel.f(20))
        print("fint(1,2) = ", Tmodel.fint(20, 30))
        print("fave(1,2) = ", Tmodel.fave(20, 30))

        # Change a model parameter
        Tmodel.update({'alpha': 1.2})
        Tmodel.info()
        print("f(1)      = ", Tmodel.f(20))

        # Ploting some results

        Tmodel = PowerLawTimeModel(alpha=1.2, tmin=15, tmax=1.e4)
        t = np.logspace(0, 3, 100)
        plt.loglog(t, Tmodel.fint(t, t + 10))
        plt.xlabel("Window start time (s)")
        plt.ylabel("Flux integrated in a moving window of 10s")
        plt.title(f"Powerlaw model with index {Tmodel.params['alpha']:.1f} starting at t={Tmodel.params['tmin']:.1f}s")
        plt.show()
    """

    def __init__(self, alpha=1.2, tmin=10., tmax=1.e20):
        TimeModel.__init__(self)
        self.params.update({'Ttype': 'PL', 'alpha': alpha, 'tmin': tmin, 'tmax': tmax})

    def f(self, t):
        return ((t >= self.params['tmin']) & (t <= self.params['tmax'])) * t**(-self.params['alpha'])
        
    def fint(self, t1i, t2i):
        t1 = np.array(t1i, copy=True, dtype=float, ndmin=1)
        t2 = np.array(t2i, copy=True, dtype=float, ndmin=1)
        t1[t1 < self.params['tmin']] = self.params['tmin']
        t2[t2 > self.params['tmax']] = self.params['tmax']
        fi = np.zeros_like(t1)
        ind = (t2 > t1)
        s = 1.0-self.params['alpha']
        if s == 0:
            fi[ind] = np.log(t2[ind]/t1[ind])
        else:
            fi[ind] = (t2[ind]**s-t1[ind]**s)/s
        return np.squeeze(fi)[()]


# =================================================================================================
class FlatTimeModel(TimeModel):
    """
    SYNOPSIS:
        This class defines a constant time model for tmin<t<tmax: N = 1

    INSTANTIATION ARGUMENTS:
        tmin,tmax:  minimal and maximal times

    EXAMPLE:
        import numpy as np
        import matplotlib.pyplot as plt
        from cascapy.intrinsic import FlatTimeModel
        Tmodel = FlatTimeModel(tmin=-1.e3, tmax=10)
        Tmodel.info()
        t = np.linspace(-1100, 100, 100)
        plt.plot(t, Tmodel.f(t))
        plt.xlabel("Time (s)")
        plt.ylabel("Flux (arbitrary units)")
        plt.title(f"Flat model")
        plt.show()
    """

    def __init__(self, tmin=-1.e-20, tmax=0.0):
        TimeModel.__init__(self)
        self.params.update({'Ttype': 'Flat', 'tmin': tmin, 'tmax': tmax})

    def f(self, t):
        return ((t >= self.params['tmin']) & (t <= self.params['tmax'])) * 1.0
        
    def fint(self, t1i, t2i):
        t1 = np.array(t1i, copy=True, dtype=float, ndmin=1)
        t2 = np.array(t2i, copy=True, dtype=float, ndmin=1)
        t1[t1 < self.params['tmin']] = self.params['tmin']
        t2[t2 > self.params['tmax']] = self.params['tmax']
        fi = (t2 > t1) * (t2-t1)
        return np.squeeze(fi)[()]


# =================================================================================================
class BrokenPowerLawTimeModel(TimeModel):
    """
    SYNOPSIS:
        This class defines a multiple broken powerlaw time model constituted by N powerlaw pieces
        for where N_i = t^(-alpha_i) between t_i and t_{i+1}.
        The flux is assumed to be zero before t_0 and after t_{N-1}

    INSTANTIATION ARGUMENTS:
        alpha:  list of indices for the broken powerlaw (size=N)
        ti:     list of times separating the different pieces (must be of size N+1)

    ATTRIBUTES:
        Although the alpha and ti are provided as lists of values at instantiation, their
        values are stored in 'params' (dict) attribute as separated parameters with the following keys:
        "alpha00", "alpha01", "alpha02"... and "ti00", "ti01", "ti02", "ti03" etc. For
        instance, the value of the second index can be changed with self.update({"alpha01":1.43})

    EXAMPLE:
        import numpy as np
        import matplotlib.pyplot as plt
        from cascapy.intrinsic import BrokenPowerLawTimeModel
        tmodel = BrokenPowerLawTimeModel(alpha=[0.5, 1., 1.5, 2], ti=[1.,10.,100,1000,1.e4])
        tmodel.info()
        t = np.logspace(np.log10(0.1), np.log10(1.e5), 100)
        plt.loglog(t, tmodel.f(t))
        plt.xlabel("Time (s)")
        plt.ylabel("Flux (arbitrary units)")
        plt.title(f"BrokenPowerLawTimeModel")
        plt.show()
    """

    def __init__(self, alpha=(1.0,), ti=(1., 1.e8,)):
        TimeModel.__init__(self)
        self._params.update({'Ttype': 'BPL'})

        if len(alpha)+1 != len(ti):
            raise ValueError("The dimension of indices and time np.arrays do not match")

        # Set the parameter values as separated coefficients
        self.ns = len(alpha)
        for i, a in enumerate(alpha):
            self._params[f'alpha{i:02}'] = a
        for i, t in enumerate(ti):
            self._params[f'ti{i:02}'] = np.max([0.0, t])

        # define some normalisation constants
        self._ni = [1.0]
        for i in range(1, self.ns):
            ti = self.params[f'ti{i:02}']
            ti1 = self.params[f'ti{i+1:02}']
            ai = self.params[f'alpha{i:02}']
            self._ni += [self._ni[-1] * (ti/ti1)**ai]

    def f(self, tin):
        t = np.array(tin, copy=False, dtype=float, ndmin=1)
        res = np.zeros_like(t)
        for i in range(self.ns):
            ti = self.params[f'ti{i:02}']
            ti1 = self.params[f'ti{i+1:02}']
            ai = self.params[f'alpha{i:02}']
            ind = (t > ti) & (t <= ti1)
            res[ind] = self._ni[i] * (ti1/t[ind])**ai
        return np.squeeze(res)[()]

    def _fint(self, tin):
        t = np.array(tin, copy=False, dtype=float, ndmin=1)
        res = np.zeros_like(t)
        last = 0.0
        ti1 = 0.0
        for i in range(self.ns):
            ti = self.params[f'ti{i:02}']
            ti1 = self.params[f'ti{i+1:02}']
            ai = self.params[f'alpha{i:02}']
            ind = (t > ti) & (t <= ti1)
            if ai != 1:
                res[ind] = last + self._ni[i] * ti1 * ((t[ind]/ti1)**(1-ai)-(ti/ti1)**(1-ai))/(1-ai)
                last = last + self._ni[i] * ti1 * (1.0 - (ti/ti1)**(1-ai))/(1-ai)
            else:
                res[ind] = last + self._ni[i] * ti1 * np.log(t[ind]/ti)
                last = last + self._ni[i] * ti1 * np.log(ti1/ti)
        res[t > ti1] = last
        return np.squeeze(res)[()]

    def fint(self, t1i, t2i):
        return self._fint(t2i) - self._fint(t1i)


class RelativeBrokenPowerLawTimeModel(TimeModel):
    """
    SYNOPSIS:
        This class defines a multiple broken powerlaw time model constituted by N powerlaw pieces
        for where N_i = t^(-alpha_i) between t_i and t_{i+1}. Compared to the standard BPLTimeModel,
        this models os parametrized by the increase in time slope relative to the previous one.
        The flux is assumed to be zero before t_0 and after t_{N-1}

    INSTANTIATION ARGUMENTS:
        dalpha:  list of indices for the broken powerlaw (size=N), relative to the previous piece.
                 the first element is the first slope.
        ti:     list of times separating the different pieces (must be of size N+1)

    ATTRIBUTES:
        Although the alpha and ti are provided as lists of values at instantiation, their
        values are stored in 'params' (dict) attribute as separated parameters with the following keys:
        "dalpha00", "dalpha01", "dalpha02"... and "ti00", "ti01", "ti02", "ti03" etc. For
        instance, the value of the second index can be changed with self.update({"dalpha01":1.43})

    EXAMPLE:
        import numpy as np
        import matplotlib.pyplot as plt
        from cascapy.intrinsic import RelativeBrokenPowerLawTimeModel
        tmodel = RelativeBrokenPowerLawTimeModel(dalpha=[0.5, 0.7, .3, .6],
                                                 ti=[1., 10., 100, 1000, 1.e4])
        tmodel.info()
        t = np.logspace(np.log10(0.1), np.log10(1.e5), 100)
        plt.loglog(t, tmodel.f(t))
        plt.xlabel("Time (s)")
        plt.ylabel("Flux (arbitrary units)")
        plt.title(f"BrokenPowerLawTimeModel")
        plt.show()
    """

    def __init__(self, dalpha=(1.0,), ti=(1., 1.e8,)):
        TimeModel.__init__(self)
        self._params.update({'Ttype': 'rBPL'})

        if len(dalpha)+1 != len(ti):
            raise ValueError("The dimension of indices and time np.arrays do not match")

        # Set the parameter values as separated coefficients
        self.ns = len(dalpha)
        for i, a in enumerate(dalpha):
            self._params[f'dalpha{i:02}'] = a
        for i, t in enumerate(ti):
            self._params[f'ti{i:02}'] = np.max([0.0, t])

        # define some normalisation constants
        self._ni = [1.0]
        ai = self.params[f'dalpha{0:02}']
        for i in range(1, self.ns):
            ti = self.params[f'ti{i:02}']
            ti1 = self.params[f'ti{i+1:02}']
            dai = self.params[f'dalpha{i:02}']
            ai = ai + dai
            self._ni += [self._ni[-1] * (ti/ti1)**ai]

    def f(self, tin):
        t = np.array(tin, copy=False, dtype=float, ndmin=1)
        res = np.zeros_like(t)
        ai = 0
        for i in range(self.ns):
            ti = self.params[f'ti{i:02}']
            ti1 = self.params[f'ti{i+1:02}']
            dai = self.params[f'dalpha{i:02}']
            ai = ai + dai
            ind = (t > ti) & (t <= ti1)
            res[ind] = self._ni[i] * (ti1/t[ind])**ai
        return np.squeeze(res)[()]

    def _fint(self, tin):
        t = np.array(tin, copy=False, dtype=float, ndmin=1)
        res = np.zeros_like(t)
        last = 0.0
        ti1 = 0.0
        ai = 0
        for i in range(self.ns):
            ti = self.params[f'ti{i:02}']
            ti1 = self.params[f'ti{i+1:02}']
            dai = self.params[f'dalpha{i:02}']
            ind = (t > ti) & (t <= ti1)
            ai = ai + dai
            if ai != 1:
                res[ind] = last + self._ni[i] * ti1 * ((t[ind]/ti1)**(1-ai)-(ti/ti1)**(1-ai))/(1-ai)
                last = last + self._ni[i] * ti1 * (1.0 - (ti/ti1)**(1-ai))/(1-ai)
            else:
                res[ind] = last + self._ni[i] * ti1 * np.log(t[ind]/ti)
                last = last + self._ni[i] * ti1 * np.log(ti1/ti)
        res[t > ti1] = last
        return np.squeeze(res)[()]

    def fint(self, t1i, t2i):
        return self._fint(t2i) - self._fint(t1i)


class LogParabolaTimeModel(TimeModel):
    """
    SYNOPSIS:
        This class defines a log-Parabola time model: F(t) = (t/tpiv)**(-alpha-nu*np.log(t/tpiv))

    INSTANTIATION ARGUMENTS:
        alpha:      temporal index
        tmin/tmax:  minimal and maximal times (in s)
        tpiv:       reference time and time in the log index (in erg)
        nu:        log-parabola parameter

    EXAMPLE:
    """

    def __init__(self, alpha=1., tmin=1.0, tmax=1.e6, tpiv=10., nu=0.02):
        TimeModel.__init__(self)
        self._params.update({'Ttype': 'logPb', 'alpha': alpha, 'tmin': tmin, 'tmax': tmax, 'tpiv': tpiv, 'nu': nu})

    def f(self, tin):
        t = np.array(tin, copy=False, dtype=float, ndmin=1)
        res = np.zeros_like(t)
        ind = (t > self.params['tmin']) & (t < self.params['tmax'])
        tt = t[ind]/self.params['tpiv']
        res[ind] = tt**(-self.params['alpha'] - self.params['nu']*np.log(tt))
        return res


class TemplateTimeModel(TimeModel):
    """
    SYNOPSIS:
        This class defines template model based on tabulated times and fluxes

    INSTANTIATION ARGUMENTS:
        time:  array of tabulated times (in s)
        flux:  array of tabulate fluxes (in arbitrary unit)
        fmin:  minimal flux (in the same units). The flux array is clipped to that threshold before
               performing log-interpolation
        name:  a short name for this model

    EXAMPLE:
        import numpy as np
        import matplotlib.pyplot as plt
        from cascapy.intrinsic import TemplateTimeModel

        # Define tabulated model
        def f(t):
            g1, g2, tc, tmin, tmax = 1.3, 3.1, 1.e3, 20.,2.e4
            return ((t>tmin) & (t<tmax)) * t**(-g1) / (tc**(g2-g1)+t**(g2-g1))
        t = np.logspace(np.log10(10), np.log10(1.e5), 20)
        plt.plot(t,f(t),marker='o',label='Tabulated Data')

        # Define and plot template model
        tmodel = TemplateTimeModel(t,f(t))
        tt = np.logspace(np.log10(10.),np.log10(1.e5),200)
        plt.plot(tt,tmodel.f(tt),label='Interpolated model')
        plt.xscale('log')
        plt.yscale('log')
        plt.ylim(1.e-21,)
        plt.xlabel('Time (s)')
        plt.ylabel('Flux (arbitrary units)')
        plt.legend()
        plt.show()
    """

    def __init__(self, time, flux, name='TPL', fmin=1.e-20, fintmin=1.e-30, logt=True):
        TimeModel.__init__(self)
        self._logt = logt
        self._fmin = fmin
        self._fintmin = fintmin
        time = np.array(time)
        self._tmin = time[0]/2.0
        flux = np.array(flux)
        flux[flux <= 0] = fmin
        self.params.update({'Ttype': name, 'time': time, 'flux': flux})

        self._interp = None
        self._interp2 = None
        self.set_interp()

    def set_interp(self):
        # define interpolation for the flux
        if self._logt:
            if np.any(self.params['time'] <= 0):
                raise ValueError("Times must be positive for log interpolation")
            else:
                self._interp = interp1d(np.log(self.params['time']), np.log(self.params['flux']),
                                        bounds_error=False, fill_value=np.log(self._fmin))
        else:
            self._interp = interp1d(self.params['time'], np.log(self.params['flux']),
                                    bounds_error=False, fill_value=np.log(self._fmin))

        # define interpolation for the flux integrated from the beginning
        fluxint = self._fintmin*np.ones_like(self.params['time'])
        for ii in range(len(self.params['time'])-1):
            fluxint[ii+1] = fluxint[ii] + quad(self.f, self.params['time'][ii],
                                               self.params['time'][ii+1], full_output=True)[0]
        if self._logt:
            self._interp2 = interp1d(np.log(self.params['time']), np.log(fluxint),
                                     bounds_error=False, fill_value=np.log(self._fintmin))
        else:
            self._interp2 = interp1d(self.params['time'], np.log(fluxint),
                                     bounds_error=False, fill_value=np.log(self._fintmin))

    def update(self, pars):
        """
        SYNOPSIS:
            Update the model parameters with a dictionary of new parameters values
        ARGUMENTS:
            pars: a dictionary of parameter names+values
        """
        for key, val in pars.items():
            if key in self._params:
                self._params[key] = val
                if key in ['time', 'flux']:
                    self._tmin = self.params['time'][0] / 2.0
                    self.set_interp()

    def f(self, t):
        t = np.array(t)
        if self._logt:
            # make sure there is no negative time (_tmin is smaller than the minimal time in the
            # interpolation array but is positive)
            t[t < self._tmin] = self._tmin
            return np.exp(self._interp(np.log(t)))
        else:
            return np.exp(self._interp(t))

    def _fint(self, tin):
        t = np.array(tin, copy=True, dtype=float, ndmin=1)
        if self._logt:
            # make sure there is no negative time (_tmin is smaller than the minimal time in the
            # interpolation array but is positive)
            t[t < self._tmin] = self._tmin
            return np.exp(self._interp2(np.log(t)))
        else:
            return np.exp(self._interp2(t))

    def fint(self, t1, t2):
        return self._fint(t2)-self._fint(t1)


# =================================================================================================
#                                      SPECTRAL MODELS
# =================================================================================================


# =================================================================================================
class SpectralModel:
    """
    SYNOPSIS:
        This class is a generic class for spectral models of a source.
        Energies are in ergs.
        
        Any precise spectral model requires to define a derived class for that model, in which at
        least one method must be OVERIDEN:
        self._f_noabs(E), the model at energy E
        
        Also, when an analytical evaluation of the energy-integrated model between E1 and E2 is available,
        it is faster to also override self._fint_noabs(E1,E2) and self._nint_noabs(E1,E2). Otherwise,
        a default numerical computation of the integral is performed
        
        In derived classes, all parameters must be stored in the _params dictionary attribute.
        In addition, this dictionary must have 'Etype' keyword with a short name for that
        derived model.
        
        So far, 5 derived class have been defined:
            - PowerLawSpectralModel(gamma,emin,emax,epiv):
                                                    F(E) = (E/Epiv)**(-Gamma)
            - CutoffPowerLawSpectralModel(gamma,emin,emax,Epiv,q) :
                                                    F(E) = (E/Epiv)**(-Gamma)*np.exp(-(E/Emax)**q)
            - LogParabolaSpectralModel(gamma,emin,emax,epiv,eta)
                                                    sF(E) = (E/Epic)**(-Gamma-eta*np.log(E/Epiv))
            - BrokenPowerLawSpectralModel(gamma,energies)
            - TemplateSpectralModel(energy,flux)

    INSTANTIATION ARGUMENTS:
        z:      redshift (used tu compute EBL absorption)
        
    ATTRIBUTES:

    EXAMPLES:
        from cascapy.intrinsic import SpectralModel

        # The following derived class defines a time model with f(E) = A + 1/E^1, where A is the
        # only model parameter
        class MySpectralModel(SpectralModel):
            def __init__(self, A=0.0):
                SpectralModel.__init__(self)
                self.params.update({'Etype': 'mymodel', 'A': A})

            def _f_noabs(self, E):
                return self.params['A'] + 1.0 / E ** 2

            def _fint_noabs(self, E1, E2):
                return self.params['A'] * (E2 - E1) - (1.0 / E2 - 1.0 / E1)

        Emodel = MySpectralModel(A=3)
        Emodel.info()
        print("f(1)      = ", Emodel.f(1))
        print("fint(1,2) = ", Emodel.fint(1, 2))
    """

    def __init__(self, z=0.0):
        self.fabs = EBL(z=z).fabs
        self._params = {'Etype': 'DefaultContainer', 'z': z}

    @property
    def params(self):
        return self._params
            
    def update(self, params):
        """
        SYNOPSIS:
            Update the model parameters with a dictionary of new parameters values
        ARGUMENTS:
            params: a dictionary of parameter names+values
        """
        for key, val in params.items():
            if key in self._params:
                self._params[key] = val
        if 'z' in params:
            self.fabs = EBL(z=params['z']).fabs

    # Differential fluxes --------------------------------------------------------------------------
    def _f_noabs(self, e):
        """
        DEFERRED METHOD!!!
        SYNOPSIS:
            Energy flux at energy E when there is no EBL absorption
            The default flux is zero in a parent class. This method must be overridden in the
            derived classes of any spectral model.
        ARGUMENTS:
            e: energy in ergs
        """
        return 0.
        
    def f(self, energy, absorbed=False):
        """
        SYNOPSIS:
            Energy flux at energy E when there is no EBL absorption
        ARGUMENTS:
            energy: energy in ergs
            absorbed:
        """
        if absorbed:
            return self._f_noabs(energy) * self.fabs(energy)
        else:
            return self._f_noabs(energy)
            
    def n(self, energy, absorbed=False):
        """
        SYNOPSIS:
            Photon flux at energy E when there is no EBL absorption
        ARGUMENTS:
            energy:      energy in ergs
            absorbed:    True to apply absorption, False no to apply
        """
        return self.f(energy, absorbed=absorbed) / energy
        
    def ef(self, energy, absorbed=False):
        """
        SYNOPSIS:
            SED at energy E when their is no EBL absorption
        ARGUMENTS:
            energy: energy in ergs
            absorbed:
        """
        return self.f(energy, absorbed=absorbed) * energy

    # energy integrated fluxes: --------------------------------------------------------------------
    def _fint_noabs(self, e1, e2):
        """
        DEFERRED METHOD!
        SYNOPSIS:
            Energy flux integrated in the [E1,E2] band  when there is no EBL absorption.
            The default flux is computed numerically in a parent class. However, it is faster to
            override this method in a derived spectral model using an analytical evaluation.
        ARGUMENTS:
            e1,e2: limts of the energy band, in ergs
        """
        if e2 > e1:
            return quad(self.f, e1, e2, args=(False,), full_output=True)[0]
        else:
            return 0.0

    def fint(self, e1i, e2i, absorbed=False):
        """
        SYNOPSIS:
            Energy flux integrated in the [E1,E2] band  when there is no EBL absorption.
        ARGUMENTS:
            e1i
            e2i:    limits of the energy band, in ergs
            absorbed:    unabsorbed flux is False (default), absorbed flux if True
        """
        e1 = np.array(e1i, copy=False, dtype=float, ndmin=1)
        e2 = np.array(e2i, copy=False, dtype=float, ndmin=1)
        fi = np.zeros_like(e2)
        ind = np.where(e2 > e1)[0]
        # When absorbed, the integration is always performed numerically
        if absorbed:
            for ii in ind:
                fi[ii] = quad(self.f, e1[ii], e2[ii], args=(True,), full_output=True)[0]
        # When unabsorbed, the default integration routine (numerical integration) can be
        # overriden by an analytical computation.
        else:
            for ii in ind:
                fi[ii] = self._fint_noabs(e1[ii], e2[ii])
        return np.squeeze(fi)[()]

    def _nint_noabs(self, e1, e2):
        """
        DEFERRED METHOD!
        SYNOPSIS:
            Photon flux integrated in the [E1,E2] band  when there is no EBL absorption.
            The default flux is computed numerically in a parent class. However, it is faster to
            override this method in a derived spectral model using an analytical evaluation.
        ARGUMENTS:
            e1,e2: limts of the energy band, in ergs
        """
        if e2 > e1:
            return quad(self.n, e1, e2, args=(False,), full_output=True)[0]
        else:
            return 0.0

    def nint(self, e1i, e2i, absorbed=False):
        """
        SYNOPSIS:
            Energy flux integrated in the [E1,E2] band  when there is no EBL absorption.
        ARGUMENTS:
            e1i: limits of the energy band, in ergs
            e2i: limits of the energy band, in ergs
            absorbed:
        """
        e1 = np.array(e1i, copy=False, dtype=float, ndmin=1)
        e2 = np.array(e2i, copy=False, dtype=float, ndmin=1)
        ni = np.zeros_like(e2)
        ind = np.where(e2 > e1)[0]
        # When absorbed, the integration is always performed numerically
        if absorbed:
            for ii in ind:
                ni[ii] = quad(self.n, e1[ii], e2[ii], args=(True,), full_output=True)[0]
        # When unabsorbed, the default integration routine (numerical integration) can be
        # overridden by an analytical computation.
        else:
            for ii in ind:
                ni[ii] = self._nint_noabs(e1[ii], e2[ii])

        return np.squeeze(ni)[()]

    def info(self):
        """
        Display some information on the time model
        """
        print(f"   Spectral model:")
        for key, val in self.params.items():
            print(f"   {key:15} = {val:>8}")


# =================================================================================================
class PowerLawSpectralModel(SpectralModel):
    """
    SYNOPSIS:
        This class defines a powerlaw spectral model: F(E) = (E/Epiv)**(-Gamma)

    INSTANTIATION ARGUMENTS:
        z:          source redshift (for EBL absorption)
        Gamma:      spectral index
        Emin,Emax:  minimal and maximal energies of the powerlaw (in ergs)
        Epiv:       reference energy (in erg)

    EXAMPLE:
        import numpy as np
        import matplotlib.pyplot as plt
        from cascapy.constants import GeV, TeV
        from cascapy.intrinsic import PowerLawSpectralModel

        # Print flux integrated in the 100GeV,3*TeV band
        Emodel = PowerLawSpectralModel(z=0.42, Gamma=1.5, Emin=10 * GeV, Emax=10 * TeV)
        print("Unabsorbed flux:", Emodel.fint(100 * GeV, 3 * TeV, absorbed=False))
        print("Absorbed flux:  ", Emodel.fint(100 * GeV, 3 * TeV, absorbed=True))

        E = np.logspace(np.log10(1 * GeV), np.log10(100 * TeV), 200)
        Emodel = PowerLawSpectralModel(z=0.42, Gamma=1.5, Emin=10 * GeV, Emax=10 * TeV)
        l, = plt.loglog(E / GeV, E * Emodel.f(E), label='model 1')
        plt.loglog(E / GeV, E * Emodel.f(E, absorbed=True), '--', color=l.get_color())
        Emodel.update({'Gamma': 2.2, 'Emin': 5 * GeV, 'Emax': 5 * TeV})
        l, = plt.loglog(E / GeV, E * Emodel.f(E), label='model 2')
        plt.loglog(E / GeV, E * Emodel.f(E, absorbed=True), '--', color=l.get_color())
        plt.xlabel('Energy (GeV)')
        plt.ylabel('SED (arbitrary units)')
        plt.ylim(1.e-4, )
        plt.legend()
        plt.title(f"Absorbed and unabsorbed flux for z={Emodel.params['z']}")
        plt.show()
    """
    def __init__(self, z=0.0, gamma=1.8, emin=1 * MeV, emax=10 * TeV, epiv=1 * TeV):
        SpectralModel.__init__(self, z=z)
        self.params.update({'Etype': 'PL', 'gamma': gamma, 'emin': emin, 'emax': emax, 'epiv': epiv})

    def _f_noabs(self, energy):
        """
        Powerlaw spectral model for flux: F_E = (E/Epiv)**(1-Gamma)
        """
        return (((energy >= self.params['emin']) & (energy <= self.params['emax']))
                * (energy/self.params['epiv'])**(1-self.params['gamma']))

    def _fint_noabs(self, e1, e2):
        e1 = np.max([self.params['emin'], e1])
        e2 = np.min([self.params['emax'], e2])
        if e2 > e1:
            s = 2-self.params['gamma']
            if s == 0:
                return np.log(e2/e1)
            else:
                return ((e2/self.params['epiv'])**s-(e1/self.params['epiv'])**s)/s \
                                                                        * self.params['epiv']
        else:
            return 0.0

    def _nint_noabs(self, e1, e2):
        e1 = np.max([self.params['emin'], e1])
        e2 = np.min([self.params['emax'], e2])
        if e2 > e1:
            s = 1.0-self.params['gamma']
            if s == 0:
                return np.log(e2/e1)
            else:
                return ((e2/self.params['epiv'])**s-(e1/self.params['epiv'])**s)/s
        else:
            return 0.0


# =================================================================================================
class CutoffPowerLawSpectralModel(PowerLawSpectralModel):
    """
    SYNOPSIS:
        This class defines a cutoff powerlaw spectral model:
                                                F(E) = (E/Epiv)**(-Gamma) * np.exp(-(E/Emax)**q)

    INSTANTIATION ARGUMENTS:
        z:          source redshift (for EBL absorption)
        Gamma:      spectral index
        Emin,Emax:  minimal and maximal energies of the powerlaw (in ergs)
        Epiv:       reference energy (in erg)
        q:          rollover index (default 1)

    EXAMPLE:
        import numpy as np
        import matplotlib.pyplot as plt
        from cascapy.constants import GeV, TeV
        from cascapy.intrinsic import CutoffPowerLawSpectralModel

        # Plot the spectral model flux (absorbed and unabsorbed) for different rollover indices
        model = CutoffPowerLawSpectralModel(Gamma=1.9, Emax=15 * TeV, z=0.2)
        model.info()
        E = np.logspace(np.log10(1 * GeV), np.log10(1000 * TeV), 100)
        l, = plt.loglog(E / TeV, E * model.f(E), label='q=1')
        l, = plt.loglog(E / TeV, E * model.f(E, absorbed=True), '--', color=l.get_color())
        model.update({'q': 2})
        l, = plt.loglog(E / TeV, E * model.f(E), label='q=2')
        l, = plt.loglog(E / TeV, E * model.f(E, absorbed=True), '--', color=l.get_color())
        model.update({'q': 0.5})
        l, = plt.loglog(E / TeV, E * model.f(E), label='q=1/2')
        l, = plt.loglog(E / TeV, E * model.f(E, absorbed=True), '--', color=l.get_color())
        plt.legend()
        plt.xlim(1.e-3, 1.e3)
        plt.ylim(1.e-3, 1.e1)
        plt.xlabel('Energy E (in TeV)')
        plt.ylabel(r'$E F_E$ (arbitrary units)')
        plt.title(f"CPL model with index {model.params['Gamma']:.1f} and cutoff {model.params['Emax'] / TeV:.1f} TeV \
        at z={model.params['z']:.1f}")
        plt.show()
    """
    def __init__(self, z=0.0, gamma=1.8, emin=1 * MeV, emax=10 * TeV, epiv=1 * TeV, q=1):
        PowerLawSpectralModel.__init__(self, z=z, gamma=gamma, emin=emin, emax=emax, epiv=epiv)
        self.params['q'] = q
        self._params.update({'Etype': 'CPL'})

    # Cutoff Powerlow
    def _f_noabs(self, energy):
        """
        Cutoff powerlaw spectral model for flux: F_E = (E/Epiv)**(1-Gamma) * np.exp(-E/Emax)
        Set abs=True to apply EBL absorption
        """
        return (energy >= self.params['emin']) * (energy/self.params['epiv'])**(1-self.params['gamma']) * \
            np.exp(-(energy/self.params['emax'])**self.params['q'])


# =================================================================================================
class LogParabolaSpectralModel(PowerLawSpectralModel):
    """
    SYNOPSIS:
        This class defines a np.logparabola spectral model:
                                                F(E) = (E/Epiv)**(-Gamma-eta*np.log(E/Epiv))

    INSTANTIATION ARGUMENTS:
        z:          source redshift (for EBL absorption)
        Gamma:      spectral index
        Emin,Emax:  minimal and mawimal energies of the powerlaw (in ergs)
        Epiv:       reference energy and energy in the np.log index (in erg)
        eta:        np.log-parabola parameter

    EXAMPLE:
        import numpy as np
        import matplotlib.pyplot as plt
        from cascapy.constants import GeV, TeV
        from cascapy.intrinsic import LogParabolaSpectralModel

        # Plot the spectral model flux (absorbed and unabsorbed) for different np.logparabola indices
        model = LogParabolaSpectralModel(Gamma=1.9, Emax=15 * TeV, z=0.2, eta=0.01)
        model.info()
        E = np.logspace(np.log10(1 * GeV), np.log10(100 * TeV), 100)
        l, = plt.loglog(E / TeV, E * model.f(E), label='eta=0.01')
        l, = plt.loglog(E / TeV, E * model.f(E, absorbed=True), '--', color=l.get_color())
        model.update({'eta': 0.05})
        l, = plt.loglog(E / TeV, E * model.f(E), label='eta=0.05')
        l, = plt.loglog(E / TeV, E * model.f(E, absorbed=True), '--', color=l.get_color())
        model.update({'eta': 0.1})
        l, = plt.loglog(E / TeV, E * model.f(E), label='eta=0.1')
        l, = plt.loglog(E / TeV, E * model.f(E, absorbed=True), '--', color=l.get_color())
        plt.legend()
        plt.xlim(1.e-3, 1.e2)
        plt.ylim(1.e-3, 1.e1)
        plt.xlabel('Energy E (in TeV)')
        plt.ylabel(r'$E F_E$ (arbitrary units)')
        plt.show()
    """

    def __init__(self, z=0.0, gamma=1.8, emin=1 * MeV, emax=10 * TeV, epiv=1 * TeV, eta=0.02):
        PowerLawSpectralModel.__init__(self, z=z, gamma=gamma, emin=emin, emax=emax, epiv=epiv)
        self.params['eta'] = eta
        self._params.update({'Etype': 'LogPb'})

    def _f_noabs(self, energy):
        return ((energy >= self.params['emin']) & (energy <= self.params['emax'])) * \
               (energy/self.params['epiv'])**(1-self.params['gamma'] -
                                              self.params['eta']*np.log(energy/self.params['epiv']))


class BrokenPowerLawSpectralModel(SpectralModel):
    """
    SYNOPSIS:
        This class defines a multiple broken powerlaw spectral model constituted by N powerlaw pieces
        for where N_i = E^(-alpha_i) between E_i and E_{i+1}.
        The flux is assumed to be zero before E_0 and after E_{N-1}
    INSTANTIATION ARGUMENTS:
        gamma:      list of indices for the broken powerlaw (size=N)
        energies:    list of energies separating the different pieces (must be of size N+1)
    ATTRIBUTES:
        Although the gamma and energies are provided as lists of values at instantiation, their
        values are stored in 'params' (dict) attribute as separated parameters with the following keys:
        "gamma00", "gamma01", "gamma02"... and "energy00", "energy01", "energy02", "energy03" etc. For
        instance, the value of the second index can be changed with self.update({"gamma01":1.43})
    EXAMPLE:
        import numpy as np
        import matplotlib.pyplot as plt
        from cascapy.constants import GeV, TeV
        from cascapy.intrinsic import BrokenPowerLawSpectralModel

        emodel = BrokenPowerLawSpectralModel(gamma=[0.5, 1., 1.5, 2], energies=[0.1*GeV,1*GeV,10*GeV,100*GeV,1*TeV])
        emodel.info()
        e = np.logspace(np.log10(0.01*GeV), np.log10(10*TeV), 100)
        plt.loglog(e/GeV, e*emodel.f(e))
        plt.xlabel("Energy (GeV)")
        plt.ylabel(r"$EF_E$ (arbitrary units)")
        plt.title(f"BrokenPowerLawSpectralModel")
        plt.show()
    """

    def __init__(self, z=0., gamma=(2.0,), energies=(1.*MeV, 10*TeV,)):
        SpectralModel.__init__(self, z=z)
        self._params.update({'Etype': 'BPL'})

        if len(gamma)+1 != len(energies):
            raise ValueError("The dimension of indices and energies np.arrays do not match")

        energies[0] = np.max([1*MeV, energies[0]])

        # Set the parameter values as separated coefficients
        self.ns = len(gamma)
        for i, g in enumerate(gamma):
            self._params[f'gamma{i:02}'] = g
        for i, e in enumerate(energies):
            self._params[f'energies{i:02}'] = e

        # define some normalisation constants
        self._ni = [1.0]
        for i in range(1, self.ns):
            ei = self.params[f'energies{i:02}']
            ei1 = self.params[f'energies{i+1:02}']
            gi = self.params[f'gamma{i:02}']
            self._ni += [self._ni[-1] * (ei/ei1)**gi]

    def _f_noabs(self, ein):
        e = np.array(ein, copy=False, dtype=float, ndmin=1)
        res = np.zeros_like(e)
        for i in range(self.ns):
            ei = self.params[f'energies{i:02}']
            ei1 = self.params[f'energies{i+1:02}']
            gi = self.params[f'gamma{i:02}']
            ind = (e > ei) & (e <= ei1)
            res[ind] = self._ni[i] * (ei1/e[ind])**gi
        return np.squeeze(res)[()]

    def _fint(self, ein):
        e = np.array(ein, copy=False, dtype=float, ndmin=1)
        res = np.zeros_like(e)
        last = 0.0
        ei1 = 0.0
        for i in range(self.ns):
            ei = self.params[f'energies{i:02}']
            ei1 = self.params[f'energies{i+1:02}']
            gi = self.params[f'gamma{i:02}']
            ind = (e > ei) & (e <= ei1)
            if gi != 1:
                res[ind] = last + self._ni[i] * ei1 * ((e[ind]/ei1)**(1-gi)-(ei/ei1)**(1-gi))/(1-gi)
                last = last + self._ni[i] * ei1 * (1.0 - (ei/ei1)**(1-gi))/(1-gi)
            else:
                res[ind] = last + self._ni[i] * ei1 * np.log(e[ind]/ei)
                last = last + self._ni[i] * ei1 * np.log(ei1/ei)
        res[e > ei1] = last
        return np.squeeze(res)[()]

    def _fint_noabs(self, e1i, e2i):
        return self._fint(e2i) - self._fint(e1i)


class TemplateSpectralModel(SpectralModel):
    """
    SYNOPSIS:
        This class defines template model based on tabulated energies and fluxes

    INSTANTIATION ARGUMENTS:
        energy:  array of tabulated times (in erg)
        flux:  array of tabulated energy fluxes (in arbitrary unit equivalent to erg/s/cm2/erg)
        fmin:  minimal flux (in the same units). The flux array is clipped to that threshold before
               performing log-interpolation
        name:  a short name for this model

    EXAMPLE:
        import numpy as np
        import matplotlib.pyplot as plt
        from cascapy.constants import GeV, TeV
        from cascapy.intrinsic import TemplateSpectralModel

        # Define tabulated model
        def f(e):
            g1, g2, ec, emin, emax = 1.3, 3.1, 100*GeV, 10*GeV, 1*TeV
            return ((e > emin) & (e < emax)) * e ** (-g1) / (ec ** (g2 - g1) + e ** (g2 - g1))
        e = np.logspace(np.log10(1*GeV), np.log10(10*TeV), 20)
        plt.plot(e/GeV, f(e), marker='o', label='Tabulated Data')

        # Define template model
        tmodel = TemplateSpectralModel(e, f(e),fmin=1.e-8)

        # Plot template model
        ee = np.logspace(np.log10(1*GeV), np.log10(10*TeV), 200)
        plt.plot(ee/GeV, tmodel.f(ee), label='Interpolated model')
        plt.xscale('log')
        plt.yscale('log')
        plt.ylim(1.e-10, )
        plt.xlabel('Energy (GeV)')
        plt.ylabel(r'$F_E$ (arbitrary units)')
        plt.legend()
        plt.show()
    """

    def __init__(self, energy, flux, z=0., name='TPL', fmin=1.e-20):
        SpectralModel.__init__(self, z=z)
        self._fmin = fmin
        energy = np.array(energy)
        flux = np.array(flux)
        flux[flux <= 0] = fmin
        self.params.update({'Etype': name, 'energy': energy, 'flux': flux})

        if np.any(energy <= 0):
            raise ValueError("Energies must be positive for log interpolation")
        else:
            energy = np.log(energy)
            self._interp = interp1d(energy, np.log(flux), bounds_error=False, fill_value=np.log(self._fmin))

    def update(self, pars):
        """
        SYNOPSIS:
            Update the model parameters with a dictionary of new parameters values
        ARGUMENTS:
            pars: a dictionary of parameter names+values
        """
        for key, val in pars.items():
            if key in self._params:
                self._params[key] = val
                if key in ['energy', 'flux']:
                    self._interp = interp1d(self.params['energy'], np.log(self.params['flux']), bounds_error=False,
                                            fill_value=np.log(self._fmin))

    def _f_noabs(self, energy):
        energy = np.array(energy, ndmin=1)
        if np.any(energy <= 0):
            raise ValueError("Energies must be positive for log interpolation")
        else:
            return np.exp(self._interp(np.log(energy)))
# =================================================================================================


# =================================================================================================
class AngularModel:
    """
    SYNOPSIS:
        This class is a generic class for angular models of sources.
        Angles are in radians.
        The angular distribution is normalized to 1 in the observer direction

        Any precise angular model requires to define a derived class for that model, in which at
        least one method must be OVERRIDDEN:
        self.dndwe(thetae), solid angle distribution as a function of the poloidal angle (with
        respect to the jet axis).
        
        Also, when an analytical evaluation of the phi-integrated model between 0 and 2*pi is available,
        it is faster to also override self.phi_integration(). Otherwise, a default numerical computation
        of the integral is performed
        
        In derived classes, all parameters must be stored in the _params dictionary attribute.
        In addition, this dictionary must have 'Atype' keyword with a short name for that derived model.
        
        So far, three derived class have been defined:
            - IsotropicAngularModel()
            - DiskAngularModel(th_jet,th_obs)
            
    INSTANTIATION ARGUMENTS:
        None
        
    ATTRIBUTES:

    EXAMPLES:
        see deferred classes
    """

    def __init__(self):
        th_obs = 0.0
        self._params = {'Atype': 'DefaultContainer', 'th_obs': th_obs}
        self._sin_obs = np.sin(self.params['th_obs'])
        self._cos_obs = np.cos(self.params['th_obs'])

    @property
    def params(self):
        return self._params

    def update(self, pars):
        """
        SYNOPSIS:
            Update the model parameters with a dictionary of new parameters values
        ARGUMENTS:
            pars: a dictionary of parameter names+values
        """
        for key, val in pars.items():
            if key in self._params:
                self._params[key] = val

    def dndwe(self, thetae):
        """
        DEFERRED METHOD!!!
        SYNOPSIS:
            axi-symmetric angular distribution as a function of the poloidal angle with respect to
            the jet axis. This method must be OVERRIDDEN by the proper angular distribution.
        ARGUMENTS:
            thetae: poloidal angle with respect to the jet axis
        """
        return 0.0

    def _dndwe(self, thetap, phid, phid0):
        """
        SYNOPSIS:
            axi-symmetric angular distribution as a function of numerical (in the results of Monte Carlo
            simulations) and observable quantities.
        ARGUMENTS:
            thetap: Numerical arrival, poloidal angle of photon with respect to the observer direction
            phid:   Numerical azimuthal, arrival angle of photons
            phid0:  Observable azimuthal, observation angle at which to compute the flux
        """
        return self.dndwe(self.thetae(thetap, phid, phid0))

    def phi_integration(self, thetap, phid):
        """
        SYNOPSIS:
            Computes the weights corresponding to the numerical integration over the
            azimuthal, observation angle. This is a default integrator. When an analytical
            evaluation is available, it is much faster to override this method with the
            proper expression.
        ARGUMENTS:
            thetap: Numerical, arrrival position of photons on the observer sphere
            phid:   Numerical, arrival azimuthal direction of photons
        RETURN:
            w:      weight due to phi-integration
        """
        nphi = 100
        phib = np.linspace(0, 2*np.pi, nphi+1)
        phia = 0.5*(phib[1:]+phib[:-1])
        dphi = (phib[1:]-phib[:-1])
        w = 0.0
        for i in range(len(phia)):
            w += self._dndwe(thetap, phid, phia[i]) * dphi[i]
            
        return w

    def thetae(self, thetap, phid, phid0):
        """
        SYNOPSIS:
            Computes the poloidal emission angle (theta_e) of primaries with respect
            to the jet axis corresponding to photons that are detected by the code with azimuthal
            direction (phi_d) and poloidal position (theta_p) and that are observed in
            the observer sky with azimuthal angle (phid0) in the global frame.
        ARGUMENTS:
            thetap: Numerical, arrival, poloidal angle of photon with respect to the observer direction
            phid:   Numerical, azimuthal, arrival angle of photons
            phid0:  Observable, azimuthal, observation angle at which to compute the flux
        RETURN:
            Poloidal emission angle with respect to the jet axis
        """
        mue = self._cos_obs*np.cos(thetap) + self._sin_obs*np.sin(thetap)*np.cos(phid0-phid)
        return np.arccos(mue)

    def info(self):
        """
        Display some information on the angular model
        """
        print(f"   Angular model:")
        for key, val in self.params.items():
            print(f"   {key:15} = {val:>8}")


# =================================================================================================
class IsotropicAngularModel(AngularModel):
    """
    SYNOPSIS:
        This class defines an isotropic emission profile, normalized to the emission in the observer
        direction

    INSTANTIATION ARGUMENTS:
        None

    EXAMPLE:
        import numpy as np
        import matplotlib.pyplot as plt
        from cascapy.constants import degree
        from cascapy.intrinsic import *

        # Define the angular model plots the emission distribution
        model1 = IsotropicAngularModel()
        theta = np.linspace(-np.pi, np.pi, 200)
        r = model1.dndwe(theta)
        plt.plot(r * np.cos(theta), r * np.sin(theta), label='Iso')
        plt.plot([0, 1], [0, 0], color='k', label='Observer')
        plt.gca().set_aspect('equal')
        plt.legend(loc='upper left')
        plt.xlim(-1, 1)
        plt.ylim(-1, 1)
        plt.show()
    """

    def __init__(self):
        AngularModel.__init__(self)
        self._params.update({'Atype': 'Iso'})

    def dndwe(self, thetae):
        return np.ones_like(thetae)

    def phi_integration(self, thetap, phid):
        return 2 * np.pi * np.ones_like(thetap)
    

# =================================================================================================
class DiskAngularModel(AngularModel):
    """
    SYNOPSIS:
        This class defines an emission profile for a uniform jet (or disk), normalized to the
        emission in the observer direction: dn/dwe = [1./(2pi)/(1-np.cos(th_jet)) in units 1/str],
        as a function of the poloidal angle with respect to the jet axis,

    INSTANTIATION ARGUMENTS:
        th_jet: Jet half-opening angle (default 1°)
        th_obs: Angle between the jet axis and the observation angle (default: 0, i.e. an aligned jet)

    EXAMPLE:
        import numpy as np
        import matplotlib.pyplot as plt
        from cascapy.constants import degree
        from cascapy.intrinsic import *

        # Define the angular model plots the emission distribution
        model = DiskAngularModel(th_jet=20 * degree, th_obs=10 * degree)
        theta = np.linspace(-np.pi, np.pi, 200)
        r = model.dndwe(theta)
        x = r * np.cos(theta + model.params['th_obs'])
        y = r * np.sin(theta + model.params['th_obs'])
        plt.plot(x, y, label=f"Disk, {model.params['th_jet'] / degree} deg")
        plt.plot([0, 1], [0, 0], color='k', label='Observer')
        plt.gca().set_aspect('equal')
        plt.legend(loc='upper left')
        plt.xlim(-1, 1)
        plt.ylim(-1, 1)
        plt.show()
    """

    def __init__(self, th_jet=1*degree, th_obs=0.0):
        AngularModel.__init__(self)
        self._params.update({'Atype': 'Disk', 'th_obs': th_obs, 'th_jet': th_jet})
        self._sin_jet = np.sin(self.params['th_jet'])
        self._cos_jet = np.cos(self.params['th_jet'])
        self._sin_obs = np.sin(self.params['th_obs'])
        self._cos_obs = np.cos(self.params['th_obs'])
        self.update({'th_obs': th_obs, 'th_jet': th_jet})

    def update(self, pars):
        for key, val in pars.items():
            if key in self._params:
                self._params[key] = val
        self._sin_jet = np.sin(self.params['th_jet'])
        self._cos_jet = np.cos(self.params['th_jet'])
        self._sin_obs = np.sin(self.params['th_obs'])
        self._cos_obs = np.cos(self.params['th_obs'])

    def dndwe(self, thetae):
        return thetae**2 < self.params['th_jet']**2

    def phi_integration(self, thetap, phid):

        w = np.zeros_like(thetap)

        cp = np.cos(self.params['th_obs']+thetap)
        cm = np.cos(self.params['th_obs']-thetap)
        
        # Emission always originating out of the jet opening angle (for any phid0 of the average)
        # no instance for isotropic emission (np.cos_jet=-1)
        w[self._cos_jet >= cm] = 0

        # Emission always originating in the jet opening angle (for any phid0 of the average)
        # all instances for isotropic emission (np.cos_jet=-1)
        w[self._cos_jet <= cp] = 2 * np.pi

        # Emission originating in or out of the jet opening angle (depending on the phid0 in the average)
        ind = (self._cos_jet > cp) & (self._cos_jet < cm)
        if (self.params['th_obs'] > 0) and (np.count_nonzero(ind) > 0):
            # np.conp.sine of the azimuthal angle at wich the emission angle equals
            # the jet opening angle
            mud = (self._cos_jet - self._cos_obs * np.cos(thetap)) / (self._sin_obs * np.sin(thetap))

            w[ind] = 2*np.arccos(mud[ind])
        
        return w

# =================================================================================================


# =================================================================================================
class SourceModel:
    """
    SYNOPSIS:
        This class defines global models of intrinsic emission, including time, energy and angle
        dependent distributions.
        The three properties (time, energy, angle) are assumed to be separated (i.e. independent
        to each other). In particular, the fluxes are computed by multiplying the fluxes provided
        by the TimeModel and SpectralModel respectively.
        
    INSTANTIATION ARGUMENTS:
        emodel: spectral model
        tmodel: time model
        amodel: angular model. Optional: by default, an isotropic model is used
        coords: Source celestial coordinates (as an astropy.coordinates.Skycoord() instance). Optional.
        t0:     Source trigger time (as an astropy.time.Time() instance). Optional.

    ATTRIBUTES:
        emodel, tmodel, amodel, coords, t0 (see above)

    EXAMPLES:
        mport numpy as np
        import matplotlib.pyplot as plt
        from cascapy.constants import *
        from cascapy.intrinsic import *
        from cascapy.cosmo import LCDM

        # Define un-normalized model
        z = 0.4252
        emodel = LogParabolaSpectralModel(gamma=2.51, eta=0.21 * 0.4342, epiv=0.4 * TeV,
                                          emin=10 * MeV, emax=10 * TeV,z=z)
        tmodel = PowerLawTimeModel(alpha=1.6, tmin=6)
        model = SourceModel(tmodel, emodel)

        # Normalize model to the isotropic luminosity Eiso in the 300GeV-1TeV band and between 62s ans 2454s
        Eiso = 4e51 # erg
        Dl = LCDM().dl_z(z)
        model.norm = Eiso * (1 + 0.4245) / (4 * np.pi * Dl ** 2) / model.f(62., 2454, 0.3 * TeV, 1 * TeV)
        model.info()

        # Plot flux at t=1000s
        t = 1.e3
        E = np.logspace(np.log10(1 * GeV), np.log10(200 * TeV), 100)
        plt.loglog(E, E * model.fet(E, t, absorbed=True), color='#1f77b4', label='PowerLaw')
        plt.loglog(E, E * model.fet(E, t, absorbed=False), ls='--', color='#1f77b4')
        plt.ylabel(r"$EF_E$ (erg/s/cm2/erg)")
        plt.xlabel("Energy (TeV)")
        plt.xlim(1.e-3, 2.e1)
        plt.ylim(1.e-14, 1.e-8)
        plt.title('GRB190114C emission at 1000s')
        plt.legend()
        plt.show()
    """

    tag = "SeparableSourceModel"

    def __init__(self, tmodel, emodel, amodel=None, coords=None, t0=None):

        self._params = {'norm': 1.0}

        # Temporal model
        self.tmodel = tmodel
        self._params.update(**self.tmodel.params)

        # Spectral model
        self.emodel = emodel
        self._params.update(**self.emodel.params)

        # Angular model
        if amodel is None:
            self.amodel = IsotropicAngularModel()
        else:
            self.amodel = amodel
        self._params.update(**self.amodel.params)

        # Reference time for time model
        if t0 is None:
            self.t0 = Time('2000-01-01T00:00:00.00', format='isot', scale='utc')
        else:
            self.t0 = t0

        # Source coordinates
        if coords is None:
            self.coords = SkyCoord(frame='icrs', ra=0., dec=0., unit='deg', equinox='J2000', obstime=self.t0)
        else:
            self.coords = coords

    @property
    def params(self):
        return self._params

    def update(self, pars):
        self.tmodel.update(pars)
        self.emodel.update(pars)
        self.amodel.update(pars)
        for key, val in pars.items():
            if key in self._params:
                self._params[key] = val

    @property
    def norm(self):
        return self._params['norm']

    @norm.setter
    def norm(self, norm):
        self._params['norm'] = norm

    # Energy- and time-dependent fluxes ---------------------------------------------------
    def net(self, energy, t, absorbed=False):
        """
        Photon flux integrated between two arbitrary energies E1 and E2 as a function of time
        set absorbed=True to apply EBL absorption
        """
        return self.params['norm'] * self.tmodel.f(t) * self.emodel.n(energy, absorbed=absorbed)

    def fet(self, energy, t, absorbed=False):
        """
        Energy flux model F_E(E,t) as a function of energy and time
        set absorbed=True to apply EBL absorption
        """
        return self.params['norm'] * self.tmodel.f(t) * self.emodel.f(energy, absorbed=absorbed)

    def efet(self, energy, t, absorbed=False):
        """
        E x Energy flux integrated between two arbitrary energies E1 and E2 as a function of time
        set absorbed=True to apply EBL absorption
        """
        return self.params['norm'] * self.tmodel.f(t) * self.emodel.ef(energy, absorbed=absorbed)

    # Energy dependent, time integrated fluxes --------------------------------------------
    def ne(self, energy, t1, t2, absorbed=False):
        """
        Photon flux integrated between two arbitrary times t1 and t2 as a function of energy
        set absorbed=True to apply EBL absorption
        """
        return self.params['norm'] * self.tmodel.fint(t1, t2) * self.emodel.n(energy, absorbed=absorbed)

    def ne_ave(self, energy, t1, t2, absorbed=False):
        """
        Photon flux averaged between two arbitrary times t1 and t2 as a function of energy
        set absorbed=True to apply EBL absorption
        """
        return self.params['norm'] * self.tmodel.fave(t1, t2) * self.emodel.n(energy, absorbed=absorbed)

    def fe(self, energy, t1, t2, absorbed=False):
        """
        Energy flux integrated between two arbitrary times t1 and t2 as a function of energy
        set absorbed=True to apply EBL absorption
        """
        return self.params['norm'] * self.tmodel.fint(t1, t2) * self.emodel.f(energy, absorbed=absorbed)

    def fe_ave(self, energy, t1, t2, absorbed=False):
        """
        Energy flux averaged between two arbitrary times t1 and t2 as a function of energy
        set absorbed=True to apply EBL absorption
        """
        return self.params['norm'] * self.tmodel.fave(t1, t2) * self.emodel.f(energy, absorbed=absorbed)

    def efe(self, energy, t1, t2, absorbed=False):
        """
        E x Energy flux integrated between two arbitrary times t1 and t2 as a function of energy
        set absorbed=True to apply EBL absorption
        """
        return self.params['norm'] * self.tmodel.fint(t1, t2) * self.emodel.ef(energy, absorbed=absorbed)

    def efe_ave(self, energy, t1, t2, absorbed=False):
        """
        E x Energy flux averaged between two arbitrary times t1 and t2 as a function of energy
        set absorbed=True to apply EBL absorption
        """
        return self.params['norm'] * self.tmodel.fave(t1, t2) * self.emodel.ef(energy, absorbed=absorbed)

    # Energy integrated, time dependent fluxes --------------------------------------------
    def nt(self, t, e1, e2, absorbed=False):
        """
        Photon flux integrated between two arbitrary energies E1 and E2 as a function of time
        set absorbed=True to apply EBL absorption
        """
        return self.params['norm'] * self.tmodel.f(t) * self.emodel.nint(e1, e2, absorbed=absorbed)

    def ft(self, t, e1, e2, absorbed=False):
        """
        Flux integrated between two arbitrary energies E1 and E2 as a function of time
        set absorbed=True to apply EBL absorption
        """
        return self.params['norm'] * self.tmodel.f(t) * self.emodel.fint(e1, e2, absorbed=absorbed)

    # Energy- and time- integrated fluxes -------------------------------------------------
    def n(self, t1, t2, e1, e2, absorbed=False):
        """
        Photon flux integrated between two arbitrary energies E1 and E2 and times t1 and t2
        set absorbed=True to apply EBL absorption
        """
        return self.params['norm'] * self.tmodel.fint(t1, t2) * self.emodel.nint(e1, e2, absorbed=absorbed)

    def f(self, t1, t2, e1, e2, absorbed=False):
        """
        Flux integrated between two arbitrary energies E1 and E2 and times t1 and t2
        set absorbed=True to apply EBL absorption
        """
        return self.params['norm'] * self.tmodel.fint(t1, t2) * self.emodel.fint(e1, e2, absorbed=absorbed)

    def info(self):
        """
        Display model parameters to screen
        """
        print('ENERGY SPECTRUM:')
        self.emodel.info()
        print('TIME EVOLUTION:')
        self.tmodel.info()
        print('ANGULAR DISTRIBUTION:')
        self.amodel.info()
        print('POINT SOURCE NORMALIZATION:')
        print(f"   norm  = {self.norm:.2e}")

# =================================================================================================


# =================================================================================================
class NonSeparableSourceModel:
    """
    SYNOPSIS:
        This class is similar to the 'SourceModel' class.
        However, here, time and energy are not separated variables, meaning that this new class
        can handle sources with time-dependent spectra.
        In the present version, only tabulated models can be used.

    INSTANTIATION ARGUMENTS:
        energy:     energy array (in ergs)
        time:       time array (in s)
        flux:       F_E energy flux array[energy,time] (in erg/s/erg/cm2 for example)

    ATTRIBUTES:

    EXAMPLES:
       # Model properties
        emin, emax = 0.1 * GeV, 15 * TeV
        tmin, tmax = 13., 10 * day
        z = 0.42

        # Model with separable time and energy
        emodel = LogParabolaSpectralModel(emin=emin, emax=emax, z=z)
        tmodel = LogParabolaTimeModel(tmin=tmin, tmax=tmax)
        model = SourceModel(emodel=emodel, tmodel=tmodel)

        # Setup grids that will define the non-separable model
        egrid = np.logspace(np.log10(0.05 * GeV), np.log10(20 * TeV), 23)
        tgrid = np.logspace(np.log10(10.), np.log10(20 * day), 21)
        flux = np.zeros((len(egrid), len(tgrid)))
        for it, t in enumerate(tgrid):
            flux[:, it] = model.fet(egrid, t)

        # Define model with non-separable time and energy
        model2 = NonSeparableSourceModel(egrid, tgrid, flux, z=z)

        # Plot and compare results
        e = np.logspace(np.log10(emin / 10), np.log10(emax * 10), 100)
        t = 100
        pp = plt.loglog(e / GeV, model.efet(e, t, absorbed=False), ls='--', label='Separable')
        col = pp[0].get_color()
        plt.loglog(e / GeV, model2.efet(e, t, absorbed=False), color=col, label='Non Separable')
        plt.vlines(egrid / GeV, 1.e-2, 1.e1, color='grey', lw=0.5, label='Model grid')

        plt.xlabel(r"Energy (GeV)")
        plt.ylabel(r"$E F_E$")
        plt.ylim(1.e-2, 1.e1)
        plt.xlim(4.e-2, 2.e4)
        plt.title(f"Instantaneous spectra at t={t}s")
        plt.legend(loc='upper left')
        plt.show()
    """

    tag = "NonSeparableSourceModel"

    def __init__(self, energy, time, flux, z=0.0, amodel=None, coords=None, t0=None):

        self._params = {'norm': 1.0, 'z': z}
        self.fabs = EBL(z=z).fabs

        if amodel is None:
            self.amodel = IsotropicAngularModel()
        else:
            self.amodel = amodel
        self._params.update(**self.amodel.params)

        self.x1 = None
        self.x2 = None
        self.log1 = None
        self.log2 = None
        self.y = None
        self._params.update({'time': time, 'energy': energy, 'flux': flux})
        self.update({'time': time, 'energy': energy, 'flux': flux})

        if t0 is None:
            self.t0 = Time('2000-01-01T00:00:00.00', format='isot', scale='utc')
        else:
            self.t0 = t0

        if coords is None:
            self.coords = SkyCoord(frame='icrs', ra=0., dec=0., unit='deg', equinox='J2000', obstime=self.t0)
        else:
            self.coords = coords

    @property
    def params(self):
        return self._params

    def update(self, pars):

        self.amodel.update(pars)

        for key, val in pars.items():
            if key in self._params:
                self._params[key] = val

        # store energies as class attribute
        if 'energy' in pars:
            energy = np.atleast_1d(pars['energy'])
            if np.any(np.abs(1.0 - energy[1:] * energy[0] / energy[:-1] / energy[1]) > 1.e2 * np.finfo(float).eps):
                if np.any(np.abs(1.0 - (energy[1:] - energy[:-1]) / (energy[1] - energy[0])) > 1.e2 * np.finfo(
                        float).eps):
                    raise ValueError("Energy array must be evenly spaced in lin- or log-space")
                else:
                    self.x1 = energy
                    self.log1 = False
            else:
                self.x1 = np.log(energy)
                self.log1 = True

        # store time as class attribute
        if 'time' in pars:
            time = np.atleast_1d(pars['time'])
            if np.any(np.abs(1.0 - time[1:] * time[0] / time[:-1] / time[1]) > 1.e2 * np.finfo(float).eps):
                if np.any(np.abs(1.0 - (time[1:]-time[:-1]) / (time[1]-time[0])) > 1.e2 * np.finfo(float).eps):
                    raise ValueError("Time array must be evenly spaced in lin- or log-space")
                else:
                    self.x2 = time
                    self.log2 = False
            else:
                self.x2 = np.log(time)
                self.log2 = True

        # store flux as class attribute
        if 'flux' in pars:
            self.y = np.atleast_2d(pars['flux'])
            self.y[self.y < 1.e-25] = 1.e-25
            self.y = np.log(self.y)

        if self.y.shape != (len(self.x1), len(self.x2)):
            raise ValueError(
                "Array dimensions do not match for energy ({len(self.x1)}), "
                "time ({len(self.x2)}) and flux ({self.y.shape})")

    @property
    def norm(self):
        return self._params['norm']

    @norm.setter
    def norm(self, norm):
        self._params['norm'] = norm

    # Energy- and time-dependent fluxes ---------------------------------------------------
    def net(self, energy, time, absorbed=False):
        """
        Photon flux integrated between two arbitrary energies E1 and E2 as a function of time
        set absorbed=True to apply EBL absorption
        """
        return self.fet(energy, time, absorbed=absorbed) / energy

    def fet(self, energy, time, absorbed=False):
        """
        Energy flux model F_E(E,t) as a function of energy and time
        set absorbed=True to apply EBL absorption
        """

        # Deal with scalars and arrays
        x1 = np.log(energy) if self.log1 else energy
        x2 = np.log(time) if self.log2 else time
        x1 = np.array(x1, copy=False, dtype=float, ndmin=1)
        x2 = np.array(x2, copy=False, dtype=float, ndmin=1)
        n = np.max([len(x1), len(x2)])
        if len(x1) == 1:
            x1 = np.full(n, x1[0])
        if len(x2) == 1:
            x2 = np.full(n, x2[0])
        if len(x1) != len(x2):
            raise ValueError("Arguments 'energy', 'time' must be scalars or arrays of same dimension")

        fl = np.zeros(n)

        # Interpolate flux only in the tabulated range
        mask = (x1 > self.x1[0]) & (x1 < self.x1[-1]) & (x2 > self.x2[0]) & (x2 < self.x2[-1])

        i1 = np.int_(np.floor((x1[mask]-self.x1[0]) / (self.x1[1]-self.x1[0])))
        i2 = np.int_(np.floor((x2[mask]-self.x2[0]) / (self.x2[1]-self.x2[0])))

        fl1 = (x1[mask]-self.x1[i1]) * self.y[i1+1, i2] + (self.x1[i1+1]-x1[mask]) * self.y[i1, i2]
        fl2 = (x1[mask]-self.x1[i1]) * self.y[i1+1, i2+1] + (self.x1[i1+1]-x1[mask]) * self.y[i1, i2+1]

        fl[mask] = (x2[mask]-self.x2[i2])*fl2 + (self.x2[i2+1]-x2[mask])*fl1
        fl[mask] = np.exp(fl[mask]/(self.x1[i1+1]-self.x1[i1])/(self.x2[i2+1]-self.x2[i2]))

        if absorbed:
            energy = np.array(energy, copy=False, dtype=float, ndmin=1)
            fl[mask] = fl[mask] * self.fabs(energy[mask])

        return self.params['norm'] * np.squeeze(fl)

    def efet(self, energy, time, absorbed=False):
        """
        E x Energy flux integrated between two arbitrary energies E1 and E2 as a function of time
        set absorbed=True to apply EBL absorption
        """
        return self.fet(energy, time, absorbed=absorbed) * energy

    # Energy dependent, time integrated fluxes --------------------------------------------
    def ne(self, energy, t1, t2, absorbed=False):
        """
        Photon flux integrated between two arbitrary times t1 and t2 as a function of energy
        set absorbed=True to apply EBL absorption
        """
        return self.fe(energy, t1, t2, absorbed=absorbed) / energy

    def ne_ave(self, energy, t1, t2, absorbed=False):
        """
        Photon flux averaged between two arbitrary times t1 and t2 as a function of energy
        set absorbed=True to apply EBL absorption
        """
        return self.fe(energy, t1, t2, absorbed=absorbed) / energy / (t2-t1)

    def fe(self, energy, time1, time2, absorbed=False):
        """
        Energy flux integrated between two arbitrary times t1 and t2 as a function of energy
        set absorbed=True to apply EBL absorption
        This method returns the analytical integration of the log-interpolation over energy
        """

        # Deal with scalar and arrays
        e = np.array(energy, copy=False, ndmin=1)
        t1 = np.array(time1, copy=False, ndmin=1)
        t2 = np.array(time2, copy=False, ndmin=1)
        n = np.max([len(e), len(t1), len(t2)])
        if len(e) == 1:
            e = np.full(n, e[0])
        if len(t1) == 1:
            t1 = np.full(n, t1[0])
        if len(t2) == 1:
            t2 = np.full(n, t2[0])
        if len(t2) != len(e) or len(t1) != len(e):
            raise ValueError("Arguments 'energy', 'time1' and 'time2' must be scalars or arrays of same dimension")
        flux = np.zeros(n)

        if self.log1:
            e = np.log(e)

        # check whether energy in the tabulated range
        mask0 = (e < self.x1[-1]) & (e > self.x1[0])

        # reduce computation to energies within the tabulated energy range
        nn = np.count_nonzero(mask0)
        if nn > 0:

            e = e[mask0]
            t1 = t1[mask0]
            t2 = t2[mask0]
            fl = np.zeros(nn)

            if self.log2:
                tt = np.exp(self.x2[0])
                t1[t1 < tt] = tt
                t2[t2 < tt] = tt
                t1 = np.log(t1)
                tt = np.exp(self.x2[-1])
                t2[t2 > tt] = tt
                t2 = np.log(t2)
            else:
                t1[t1 < self.x2[0]] = self.x2[0]
                t2[t2 > self.x2[-1]] = self.x2[-1]

            de = self.x1[1] - self.x1[0]
            dt = self.x2[1] - self.x2[0]

            i1 = np.int_(np.floor((e-self.x1[0]) / de))

            # Compute the contribution of all time bins to the integral
            for i2 in range(len(self.x2)-1):

                # adjust left and right boundary integration of the current time bin
                left = np.where(self.x2[i2] < t1, t1, self.x2[i2])
                right = np.where(self.x2[i2+1] > t2, t2, self.x2[i2+1])
                mask = (right > left)

                # Reduce computation to contributing time bins
                if np.count_nonzero(mask) > 0:

                    ii = i1[mask]

                    de_l = e[mask] - self.x1[ii]
                    de_r = self.x1[ii+1] - e[mask]

                    fl1 = de_l * self.y[ii+1, i2] + de_r * self.y[ii, i2]
                    fl2 = de_l * self.y[ii+1, i2+1] + de_r * self.y[ii, i2+1]

                    bb = (fl2 - fl1) / (de*dt)
                    if self.log1:
                        bb = bb + 1
                    aa = (self.x2[i2+1]*fl1 - self.x2[i2]*fl2) / (de*dt)

                    fl[mask] = fl[mask] + (np.exp(aa+right[mask]*bb) - np.exp(aa+left[mask]*bb)) / bb

            if absorbed:
                if self.log1:
                    fl = fl * self.fabs(np.exp(e))
                else:
                    fl = fl * self.fabs(e)

            flux[mask0] = fl

        return self.params['norm'] * np.squeeze(flux)

    def fe_ave(self, energy, t1, t2, absorbed=False):
        """
        Energy flux averaged between two arbitrary times t1 and t2 as a function of energy
        set absorbed=True to apply EBL absorption
        """
        return self.fe(energy, t1, t2, absorbed=absorbed) / (t2-t1)

    def efe(self, energy, t1, t2, absorbed=False):
        """
        E x Energy flux integrated between two arbitrary times t1 and t2 as a function of energy
        set absorbed=True to apply EBL absorption
        """
        return energy * self.fe(energy, t1, t2, absorbed=absorbed)

    def efe_ave(self, energy, t1, t2, absorbed=False):
        """
        E x Energy flux averaged between two arbitrary times t1 and t2 as a function of energy
        set absorbed=True to apply EBL absorption
        """
        return energy * self.fe(energy, t1, t2, absorbed=absorbed) / (t2-t1)

    # Energy integrated, time dependent fluxes --------------------------------------------
    def nt(self, time, e1, e2, absorbed=False):
        """
        Photon flux integrated between two arbitrary energies E1 and E2 as a function of time
        set absorbed=True to apply EBL absorption
        """
        t = np.array(time, copy=False, dtype=float, ndmin=1)
        fi = np.zeros_like(t)
        if e2 > e1:
            for it, tt in enumerate(t):
                fi[it] = quad(self.net, e1, e2, args=(tt, absorbed), full_output=True)[0]

        return np.squeeze(fi)[()]

    def ft(self, time, e1, e2, absorbed=False):
        """
        Flux integrated between two arbitrary energies E1 and E2 as a function of time
        set absorbed=True to apply EBL absorption
        Here the integration is performed numerically. An faster, analytical computation
        could also be implemented when the non-absorbed flux is wanted. However, this
        method is usually called for small arrays, so that computation speed is not
        a bottleneck.
        """
        t = np.array(time, copy=False, dtype=float, ndmin=1)
        fi = np.zeros_like(t)
        if e2 > e1:
            for it, tt in enumerate(t):
                fi[it] = quad(self.fet, e1, e2, args=(tt, absorbed), full_output=True)[0]

        return np.squeeze(fi)[()]

    # Energy- and time- integrated fluxes -------------------------------------------------
    def n(self, t1, t2, e1, e2, absorbed=False):
        """
        Photon flux integrated between two arbitrary energies E1 and E2 and times t1 and t2
        set absorbed=True to apply EBL absorption
        """
        if e2 > e1:
            return quad(self.ne, e1, e2, args=(t1, t2, absorbed), full_output=True)[0]
        else:
            return 0.0

    def f(self, t1, t2, e1, e2, absorbed=False):
        """
        Flux integrated between two arbitrary energies E1 and E2 and times t1 and t2
        set absorbed=True to apply EBL absorption
        """
        if e2 > e1:
            return quad(self.fe, e1, e2, args=(t1, t2, absorbed), full_output=True)[0]
        else:
            return 0.0

    def info(self):
        """
        Display model parameters to screen
        """
        print('ANGULAR DISTRIBUTION:')
        self.amodel.info()
        print('POINT SOURCE NORMALIZATION:')
        print(f"   norm  = {self.norm:.2e}")


# =================================================================================================

# =================================================================================================
#                             Reference models for a few sources
# =================================================================================================

# Reference Model for GRB130427A
zz = 0.34
tt0 = Time('2013-04-27T07:47:57.511', format='isot', scale='utc')
coo = SkyCoord(frame='icrs', ra=173.13683, dec=27.69894, unit='deg', equinox='J2000', obstime=tt0)
emod = PowerLawSpectralModel(gamma=1.66, emin=1 * keV, emax=10 * TeV, z=zz)
tmod = PowerLawTimeModel(alpha=1.17, tmin=10)
GRB130427A = SourceModel(tmod, emod, coords=coo, t0=tt0)
Eobs = 100*keV
GRB130427A.norm = 4.2e-4/keV * Eobs / GRB130427A.fe_ave(Eobs, 11.5, 33)

# Reference Model for GRB180720B
zz = 0.654
tt0 = Time('2018-07-20T14:21:39.65', format='isot', scale='utc')
coo = SkyCoord(frame='icrs', ra=0.52913, dec=-2.91883, unit='deg', equinox='J2000', obstime=tt0)
emod = PowerLawSpectralModel(gamma=1.6, emin=1 * MeV, emax=30 * TeV, z=zz)
tmod = PowerLawTimeModel(alpha=1.83, tmin=10)
GRB180720B = SourceModel(tmod, emod, coords=coo, t0=tt0)
Eobs = 0.154*TeV
GRB180720B.norm = 7.52e-10/TeV*Eobs / GRB180720B.fe_ave(Eobs, 10.1 * hour, 12.1 * hour)

# Reference Model for GRB190114C
zz = 0.4245
tt0 = Time('2019-01-14T20:47:03.0', format='isot', scale='utc')
coo = SkyCoord(frame='icrs', ra=54.5492, dec=-26.94661, unit='deg', equinox='J2000', obstime=tt0)
emod = PowerLawSpectralModel(gamma=2.22, emin=10 * MeV, emax=10 * TeV, z=zz)
tmod = PowerLawTimeModel(alpha=1.6, tmin=6)
GRB190114C = SourceModel(tmod, emod, coords=coo, t0=tt0)
Eiso = 4e51*(1+zz)  # erg
Dl = LCDM().dc_z(zz) * (1 + zz)
GRB190114C.norm = Eiso/(4*np.pi*Dl**2)/GRB190114C.f(62., 2454, 0.3 * TeV, 1 * TeV)

# Reference Model for GRB190829A
zz = 0.0785
tt0 = Time('2019-08-29T19:55:53.0', format='isot', scale='utc')
coo = SkyCoord(frame='icrs', ra=44.543792, dec=-8.957556, unit='deg', equinox='J2000', obstime=tt0)
emod = PowerLawSpectralModel(gamma=2.06, emin=1 * MeV, emax=30 * TeV, z=zz)
tmod = PowerLawTimeModel(alpha=1.09, tmin=10)
GRB190829A = SourceModel(tmod, emod, coords=coo, t0=tt0)
Eobs = 0.556*TeV
GRB190829A.norm = 22.67e-12/TeV*Eobs / GRB190829A.fe_ave(Eobs, 4.3 * hour, (4.3 + 3.6) * hour)

# reference model for GRB201216C
# zz = 1.1
# t0 = Time('2020-12-16T23:07:31.0', format='isot', scale='utc')

# Reference model for GRB221009A:
zz = 0.151
tt0 = Time('2022-10-09T13:16:59.99', format='isot', scale='utc')
coo = SkyCoord(frame='icrs', ra=288.264, dec=19.768, unit='deg', equinox='J2000', obstime=tt0)
ts = 226.0
emod = PowerLawSpectralModel(emin=1 * GeV, emax=10 * TeV, gamma=2.3, z=zz)
A = 1.5e-5
tis = [1, 4.85, 18, 670, 2000.]
alphas = [-14.9, -1.2, 1.02, 2.21]
# A = 2.22e-5
# ti = [1, 4.85, 15.37, 670, 2000.]
# alphas = [-14.9, -1.82, 1.115, 2.21]
tmod = BrokenPowerLawTimeModel(ti=tis, alpha=alphas)
amod = IsotropicAngularModel()
GRB221009A = SourceModel(tmodel=tmod, emodel=emod, amodel=amod, coords=coo, t0=tt0)
GRB221009A.norm = A / GRB221009A.ft(tis[2], 0.3 * TeV, 5 * TeV)


# ==================================================================================================
if __name__ == '__main__':
    pass
# ==================================================================================================
