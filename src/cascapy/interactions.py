"""
SYNOPSIS:
    This module defines classes holding the properties of the two main interactions governing the
    cascades, namely:
    - photon-photon pair production
    - Compton scattering.
    This module aims at making analytical estimates and checks (not to analyse the MC results)

DEPENDENCIES:
    numpy, scipy

META:
    author: R. Belmont
    date:   23/05/24
"""

# Public packages
import numpy as np
from scipy.special import spence
from scipy.integrate import quad
from scipy.optimize import root_scalar as solve

# Current package
from .constants import c, mc2, sigmaT, eV
from .cosmo import LCDM
from .backgrounds import BKG

__all__ = ["Interaction", "PairProd", "Compton"]


def _li2(x):
    return spence(1-x)


# ==================================================================================================
# Generic interaction class
# ==================================================================================================
class Interaction:
    """
    SYNOPSIS:
        Generic class to describe the property of an interaction. It holds methods to compute the
        interaction rate, optical depth, mean free path and so on. It relies on the interaction
        cross-sections that are deferred to classes specific to a given interaction.

    ARGUMENTS:
        lcdm: an instance of the LCDM class

    ATTRIBUTES;
        lcdm:   Instance of the LCDM class
        target: Instance of the BKG() class
    """

    def __init__(self, lcdm=LCDM()):
    
        self.target = BKG()
        self.lcdm = lcdm

    @staticmethod
    def sigma(x):
        """
        Total Cross-section (in 1/cm2). Deferred method.
        """
        return 0.0 * x

    # local absorption rate
    def dtau_dt(self, ti, energy_0, tinit=None):
        """
        SYNOPSIS:
            Local variation of the interaction optical depth with time as the particle propagates
            (in s^{-1}). This is achieved by integrating the interaction cross-section over the
            spectrum of target photons.
        ARGUMENTS:
            ti: cosmic time at interaction. For any past time, it is negative.
            energy_0: particle energy at 'tinit' if provided, at 'ti' otherwise. In ergs.
            tinit (optional): the time at which the energy is given. If not provided, E0 is assumed
                to be the energy at the interaction. If provided, the given energy is redshifted
                before interaction. The redshift is exact for photon, but only an approximation
                for high energy leptons.
        RETURN:
            res: interaction rate in (1/s)
        """

        emin = 1.e-5*eV
        emax = np.max([emin, 30*eV])
        e = np.logspace(np.log10(emin), np.log10(emax), 200)
        de = e[1:]-e[:-1]
        e = np.sqrt(e[1:]*e[:-1])

        tmax = 1.e19
        zi = self.lcdm.z_t(min([ti, tmax]))
        earr, zarr = np.meshgrid(energy_0, zi)
        
        # if the reference time is not ti, apply photon redshift
        if tinit is not None:
            earr = earr * (1 + zarr) / (1 + self.lcdm.z_t(min([tinit, tmax])))
            
        res = np.zeros_like(earr)
        for i in range(res.shape[0]):
            for j in range(res.shape[1]):
                res[i, j] = c * np.sum(self.sigma(e * earr[i, j] / mc2 ** 2) * self.target.ne(e, zarr[i, j]) * de)
        return np.squeeze(res)

    # absorption between two cosmic times
    def dtau(self, t1, t2, energy_0):
        """
        SYNOPSIS:
            Cumulative optical depth between two cosmic times.
            This computed by integrating dtau_dt over time and consistently redshifting the particle
            energy as time goes on.
        ARGUMENTS:
            t1: starting cosmic time (in s). For any past time, it is negative.
            t2: ending cosmic times (in s). For any past time, it is negative.
            energy_0: particle energy (as measured in the LOCAL frame at initial cosmic time t1). In erg.
        RETURN:
            res: the interaction optical depth
        """
        tarr1 = np.array(t1, ndmin=1, copy=True)
        n1 = len(tarr1)
        t2arr = np.array(t2, ndmin=1, copy=True)
        n2 = len(t2arr)
        earr = np.array(energy_0, ndmin=1, copy=True)
        n3 = len(earr)
        res = np.zeros((n1, n2, n3))
        for i in range(n1):
            for j in range(n2):
                for k in range(n3):
                    res[i, j, k] = quad(self.dtau_dt, tarr1[i], t2arr[j], args=(earr[k], tarr1[i],), epsrel=1.e-4,
                                        full_output=1)[0]
        return np.squeeze(res)

    def _dtau(self, dt, t1, energy_0):
        return self.dtau(t1, t1 + dt, energy_0) - 1.0

    def _dtau_dt(self, dt, t1, energy_0):
        return self.dtau_dt(t1 + dt, energy_0, t1) - 1.0

    def tmfp(self, ti, energy_0):
        """
        SYNOPSIS:
            Mean free time (in s), namely: travel time required to reach unity optical depth tau=1.
        ARGUMENTS;
            ti: Initial cosmic time (in s). For past time, it iis negative.
            E0: Initial particle energy (as measured in the LOCAL frame at initial cosmic time).
                In ergs.
        RETURN:
            Tmfp: Travel time (in s)
        """
        tarr = np.array(ti, ndmin=1, copy=True, dtype=float)
        n1 = len(tarr)
        earr = np.array(energy_0, ndmin=1, copy=True, dtype=float)
        n2 = len(earr)
        ttarr = np.zeros((n1, n2))
        for i in range(n1):
            for j in range(n2):
                ttarr[i, j] = solve(self._dtau, args=(tarr[i], earr[j]), x0=1.0 / self.dtau_dt(tarr[i], earr[j]),
                            fprime=self._dtau_dt, rtol=1.e-3).root
        ttarr[ttarr <= 0] = 1.e50
        return np.squeeze(ttarr)

    def lmfp(self, t1, energy_0):
        """
        SYNOPSIS
            Comoving mean free path, namely: comoving distance required to reach unity optical
            depth tau=1.
        ARGUMENTS;
            ti: Initial cosmic time (in s). For past times, it is negative.
            E0: Initial particle energy (as measured in the LOCAL frame at initial cosmic time).
                In ergs
        RETURN:
            Lmfp: Comoving distance (in cm)
        """
        t1arr = np.array(t1, ndmin=1, copy=True, dtype=float)
        n1 = len(t1arr)
        earr = np.array(energy_0, ndmin=1, copy=True, dtype=float)
        n2 = len(earr)
        larr = np.zeros((n1, n2))
        for i in range(n1):
            for j in range(n2):
                larr[i, j] = self.lcdm.xc_t(t1arr[i], t1arr[i] + self.tmfp(t1arr[i], earr[j]))
        larr[larr <= 0] = 1.e50
        return np.squeeze(larr)

# ==================================================================================================


# ==================================================================================================
# Pair production
# ==================================================================================================
class PairProd(Interaction):
    """
    SYNOPSIS:
        Class for the photon pair production interaction. It inherits from the generic
        interaction class

    ARGUMENTS:
        see the Interaction class

    ATTRIBUTES:
        see the Interaction class

    EXAMPLES:
        import numpy as np
        import matplotlib.pyplot as plt
        from cascapy.constants import TeV, mc2
        from cascapy.interactions import PairProd

        # Spectrum of produced pairs after the annihilation of a 1 TeV primary photon
        pp = PairProd()
        z = 0.
        E0 = 1*TeV
        ge0 = np.logspace(np.log10(1.e-2*TeV/mc2), np.log10(2*TeV/mc2), 200)
        dg = ge0[1:] - ge0[:-1]
        y = pp.d2n_dt_dge0(ge0, E0 * (1 + z), z)
        norm = sum(y[:-1] * dg)
        c1 = plt.loglog(ge0 * mc2 / TeV, ge0 * y / norm, label=f"E0 = {E0 / TeV:.2f} TeV")
        plt.xlabel('Local energy of the produced lepton (TeV)')
        plt.ylabel(r'$E_e n(E_e) / n_{tot}$')
        plt.show()
    """
    # ----------------------------------------------------------------------------------------------
    # Differential cross sections
    # ----------------------------------------------------------------------------------------------
    @staticmethod
    def _dn_dge0(et, e0, g):
        """
        SYNOPSIS:
            Energy spectrum of leptons produced by one single photon-photon interaction (in
            lepton/ge0). See e.g. Zdziarski1988 Eq. B1, or Aharonian, Kirillov&Vardanian 1983.
        ARGUMENTS
            et:  target photon energy at the interaction site
            e0:  gamma-ray energy (as measured at the interaction site)
            ge:  Lorentz factor of the produced lepton
        RESULT:
            res: the lepton spectrum
        """
        etarr, e0arr, garr = np.meshgrid(et/mc2, e0/mc2, g)
        res = np.zeros_like(etarr)
        gparr = e0arr - garr
        ind = (gparr > 0) & (e0arr < etarr*4*garr*gparr)
        yg = e0arr[ind]/(etarr[ind]*4*garr[ind]*gparr[ind])
        r = (garr[ind]/gparr[ind] + gparr[ind]/garr[ind]) / 2
        res[ind] = 3./4./(etarr[ind] * e0arr[ind]**2) * ((r-2*yg) * (1-yg) - 2*yg*np.log(yg))
        return np.squeeze(res)
    
    def _d3n_dt_dge0_det(self, et, ge, e0, zi):
        """
        SYNOPSIS:
            Production rate for a primary photon going through a mono-energetic target
            (in lepton/s/ge)
        ARGUMENTS:
            et: target energy
            ge: lorentz factor of the produced lepton
            e0: energy of the primary photon (as measured at the interaction site at zi)
            zi: interaction redshift
        RESULT:
            res: The lepton spectrum per unit time and target energy
        """
        return 2*c*sigmaT * self._dn_dge0(et, e0, ge) * self.target.ne(et, zi)
    
    def d2n_dt_dge0(self, ge0, e0, zi):
        """
        SYNOPSIS:
            Spectrum of produced leptons integrated over the background photon field.
        ARGUMENTS:
            ge0: Lorentz factor of the produced lepton
            e0:  energy of the primary photon (at the interaction site)
            zi:  interaction redshift
        RESULT:
            res: The lepton spectrum per unit time
        """
        etmax = 30*eV
        garr = np.array(ge0, ndmin=1, copy=True, dtype=float)
        n1 = len(garr)
        earr = np.array(e0, ndmin=1, copy=True, dtype=float)
        n2 = len(earr)
        zarr = np.array(zi, ndmin=1, copy=True, dtype=float)
        n3 = len(zarr)
        res = np.zeros((n1, n2, n3))
        for i in range(res.shape[0]):
            for j in range(res.shape[1]):
                if earr[j]/mc2 > garr[i]:
                    etmin = earr[j] / 4 / garr[i] / (earr[j]/mc2-garr[i])
                    etmin = min([etmax, np.max([1.e-5*eV, etmin])])
                    ee = np.logspace(np.log10(etmin), np.log10(etmax), 50)
                    de = ee[1:]-ee[:-1]
                    ee = np.sqrt(ee[1:]*ee[:-1])
                    res[i, j, :] = np.sum(self._d3n_dt_dge0_det(ee, garr[i], earr[j], zarr[:]) * de)
        return np.squeeze(res)
    # ----------------------------------------------------------------------------------------------

    # ----------------------------------------------------------------------------------------------
    # Total cross section
    # ----------------------------------------------------------------------------------------------
    @staticmethod
    def sigma(gg):
        """
        SYNOPSIS:
            Total pair production cross-section (in cm^2)
            See e.g. Zdziarski1988 Eq. B11,B12.
        ARGUMENTS:
            gg: (Et*E0)/(mc2)^2 where Et and E0 are the energies of the target and primary photons
                respectively (as measured at the interaction site).
        RESULT:
            s:  cross-section
        """
        g2 = np.array(gg, dtype=float, ndmin=1, copy=True)
        s = np.zeros_like(g2)
        
        ind = g2 > 1
        g2 = g2[ind]
        b2 = 1.0 - 1.0/g2
        b = np.sqrt(b2)
        w = (1+b)**2 * g2
        ll = np.log(w)
        lb = _li2(-1 / w) + np.pi ** 2 / 12 + ll ** 2 / 2
        ss = 2*b*(1-2*g2) - 4*lb + ll * ((1+b2)*g2 - b2 - ll + 4*np.log(w+1))
        s[ind] = (3./8.) * sigmaT * ss / g2**2
        
        return np.squeeze(s)

# ==================================================================================================


# ==================================================================================================
class Compton(Interaction):

    """
    SYNOPSIS:
        Class for the Compton interaction. It inherits from the generic interaction class

    ARGUMENTS:
        see the Interaction class

    ATTRIBUTES
        see the Interaction class

    EXAMPLES:
        import numpy as np
        import matplotlib.pyplot as plt
        from cascapy.constants import GeV,TeV,mc2
        from cascapy.interactions import Compton

        # Plot the spectrum of scattered photon resulting from the scattering of leptons with energy 1*TeV
        cc = Compton()
        Ee0 = 1 * TeV
        E =  np.logspace(np.log10(1.e-3*GeV),np.log10(1.e3*GeV),200)
        c1 = plt.loglog(E / GeV, E * cc.d2n_dt_deic(Ee0 / mc2, E, 0.0), label=r"$E_{e,0}=$" + f"{Ee0 / TeV} TeV")
        plt.xlabel('Observed energy of the scattered photons (GeV)')
        plt.ylabel(r'$E_e n(E_e)$ (s$^{-1}$)')
        plt.show()
    """

    # ----------------------------------------------------------------------------------------------
    # Differential cross section
    # ----------------------------------------------------------------------------------------------
    # Jones68 Eq. 9 and 44  or Blumenthal&Gould70 Eq. 2.48
    @staticmethod
    def _dn_deic(et, ge, ee):
        """
        SYNOPSIS:
            Spectrum of scattered photons for one single target photon.
        ARGUMENTS:
            et: energy of the target photon (erg)
            ge: Lorentz factor of the lepton
            ee: energy of the scattered photon as measured in the interaction frame (in erg)
        RESULT:
            res: Photon spectrum
        """
    
        ein, g, eout = np.meshgrid(et / mc2, ge, ee / mc2)
        res = np.zeros_like(ein)
        
        e1 = eout/g
        gge = 4*g*ein
        e1max = gge/(1+gge)
        e1b = e1/e1max

        ind = (e1b < 1) & (g*e1 > ein)

        q = e1b[ind] / (1+gge[ind]*(1-e1b[ind]))
        qm1 = (1+gge[ind]) * (1-e1b[ind]) / (1+gge[ind]*(1-e1b[ind]))  # (= 1-q)

        res[ind] = 2*q*np.log(q) + (1 + 2*q + 0.5*(gge[ind]*q)**2/(1+gge[ind]*q)) * qm1
        res[ind] /= g[ind]**2 * ein[ind] * mc2
        res[ind] *= (3./4.)

        return np.squeeze(res)
    
    def _d3n_dt_deic_det(self, et, ge, eic, zi, at_earth=True):
        """
        SYNOPSIS:
            Spectrum of scattered photons per unit time and per unit target energy. The target
            energy can be the observed one at z=0 (if at_earth=True), or the local one (if
            at_earth=False)
        ARGUMENTS:
            et: energy of the background photon (in erg)
            ge:  Lorentz factor of the lepton
            eic: Energy of the scattered photon (as measured in the OBSERVER frame at z=0). In erg.
            zi:  interaction redshift
            at_earth: if True, transform the result in energies measured at Earth (z=0).
        RESULT:
            res: photon spectrum
        """
        if at_earth:
            return (1+zi) * c * sigmaT * self._dn_deic(et, ge, eic * (1 + zi)) * self.target.ne(et, zi)
        else:
            return c * sigmaT * self._dn_deic(et, ge, eic) * self.target.ne(et, zi)

    def d2n_dt_deic(self, ge, eic, zi, at_earth=True):
        """
        SYNOPSIS:
            Photon spectrum per unit time, once integrated over the target spectrum. The target
            energy can be the observed one at z=0 (if at_earth=True), or the local one (if
            at_earth=False)
        ARGUMENTS:
            ge:  Lorentz factor of the lepton
            eic: Energy of the scattered photon (as measured in the OBSERVER frame at z=0). In erg.
            zi:  interaction redshift
            at_earth: if True, transform the result in energies measured at Earth (z=0).
        """
        garr = np.array(ge, ndmin=1, copy=True, dtype=float)
        n1 = len(garr)
        earr = np.array(eic, ndmin=1, copy=True, dtype=float)
        n2 = len(earr)
        zarr = np.array(zi, ndmin=1, copy=True, dtype=float)
        n3 = len(zarr)
        res = np.zeros((n1, n2, n3))
        
        emin = 1.e-5*eV
        emax = 3.e1*eV
        et = np.logspace(np.log10(emin), np.log10(emax), 100)
        det = et[1:]-et[:-1]
        et = np.sqrt(et[:-1]*et[1:])
        
        for i in range(n1):
            for j in range(n2):
                for k in range(n3):
                    res[i, j, k] = np.sum(self._d3n_dt_deic_det(et, garr[i], earr[j], zarr[k], at_earth=at_earth) * det)
        
        return np.squeeze(res)

    # ----------------------------------------------------------------------------------------------
    # Total  cross section
    # ----------------------------------------------------------------------------------------------
    @staticmethod
    def sigma(s0):
        """
        SYNOPSIS:
            Total Compton cross-section (in cm^2).
            See e.g. Zdziarski 1988, Eq. A22
        ARGUMENTS:
            s0: (Et*E0)/(mc2)^2 where Et and E0 are the energies of the target photon and lepton
                respectively (as measured at the interaction site).
        RESULT:
            cross-section
        """
        s = np.array(4*s0, ndmin=1, copy=True, dtype=float)
        psi = 1.0 - (2./3.)*s + (13./20)*s**2 - (133./200.)*s**3
        ind = s > 1.e-2
        s = s[ind]
        psi[ind] = 1.5/s**2 * ((1+s) * (1+8/s) * np.log(1+s) - 0.5 * s + 4 * _li2(-s) + 0.5 / (s + 1) - 8.5)
        psi[psi < 0] = 0.0
        return sigmaT * np.squeeze(psi)

    def lic(self, z):
        """
        SYNOPSIS:
            Approximate mean free path including only targets from the CMB, assuming Thomson regime
            and neglecting the cosmological expansion during this mean free path.
        ARGUMENTS:
            z: Lepton redshift
        RETURN:
            lic: the approximate mean free path (in proper cm)
        """
        return 1/(sigmaT*self.target.cmb.n(z))

    def tic(self, z):
        """
        SYNOPSIS:
            Approximate interaction distance including only targets from the CMB, assuming Thomson
            regime and neglecting the cosmological expansion during this mean free path.
        ARGUMENTS:
            z: Lepton redshift
        RETURN:
            tic: the approximate interaction time (in proper s)
        """
        return 1/(c*sigmaT*self.target.cmb.n(z))

    def lcoolic(self, energy_e, z):
        """
        SYNOPSIS:
            Approximate cooling distance including only targets from the CMB and neglecting the
            cosmological expansion during this mean free path.
        ARGUMENTS:
            energy_e:
            z: Lepton redshift
        RETURN:
            lic: the approximate cooling distance (in propre cm)
        """
        return 3*mc2 / (4*sigmaT*self.target.cmb.u(z)) * (mc2 / energy_e)

    def tcoolic(self, energy_e, z):
        """
        SYNOPSIS:
            Approximate cooling time including only targets from the CMB and neglecting the
            cosmological expansion during this mean free path.
        ARGUMENTS:
            energy_e:
            z: Lepton redshift
        RETURN:
            lic: the approximate cooling time (in proper s)
        """
#        3/mc2^2/(4*c*sigmaT*self.target.cmb.n(z)*Ecmb)/Ee
        return 3*mc2 / (4*c*sigmaT*self.target.cmb.u(z)) * (mc2 / energy_e)

    def energy_c(self, z):
        return 3.*mc2**2/4./self.target.cmb.e(z)

    def eic_ee(self, energy_e, z):
        return energy_e**2/self.energy_c(z)

    def ee_eic(self, energy_ic, z):
        return np.sqrt(energy_ic * self.energy_c(z))

    def ee_t(self, t, energy0_e, z0):
        """
        SYNOPSIS:
            Time evolution of the lepton.
        ARGUMENTS:
            t: time since t0
            energy0_e: lepton energy at t0
            z0: redshift at t0
        RETURN:
            Ee_t: lepton energy at time t
        """
        return energy0_e/(1 + t / self.tcoolic(energy0_e, z0))

    def ee_x(self, x, energy0_e, z0):
        """
        SYNOPSIS:
            Distance evolution of the lepton.
        ARGUMENTS:
            x: distance since x0
            energy0_e: lepton energy at x0
            z0: redshift at x0
        RETURN:
            Ee_t: lepton energy at x0
        """
        return energy0_e/(1 + x / self.lcoolic(energy0_e, z0))

    def eic_t(self, t, energy0_ic, z0, at_earth=True):
        """
        SYNOPSIS:
            Time evolution of the lepton.
        ARGUMENTS:
            t: time since t0
            energy0_ic: lepton energy at t0
            z0: redshift at t0
            at_earth:
        RETURN:
            Ee_t: lepton energy at time t
        """
        e = energy0_ic / (1 + t / self.tcoolic(self.ee_eic(energy0_ic, z0), z0)) ** 2
        if at_earth:
            e = e/(1+z0)
        return e

    def eic_x(self, x, energy0_ic, z0, at_earth=True):
        """
        SYNOPSIS:
            Distance evolution of the lepton.
        ARGUMENTS:
            x: distance since x0
            energy0_ic: lepton energy at x0
            z0: redshift at x0
            at_earth:
        RETURN:
            Ee_t: lepton energy at x0
        """
        e = energy0_ic / (1 + x / self.lcoolic(self.ee_eic(energy0_ic, z0), z0)) ** 2
        if at_earth:
            e = e/(1+z0)
        return e
# ==================================================================================================


# ==================================================================================================
if __name__ == "__main__":
    pass
