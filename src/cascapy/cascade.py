"""
SYNOPSIS:
    This module defines classes to read event data-files from Monte-Carlo simulations produced by the `cascadel` code
    and to build binned, observable quantities.
    Namely:
    - EventCatalog:  This class reads the MC event data files and applies preliminary event selection.
    - BinnedData:    This class further processes these events and produces binned information.

DEPENDENCIES
    numpy, copy

META:
    author: R. Belmont
    date:   16/10/24
"""


# Generic packages
import numpy as np
from copy import deepcopy

# Current package
from .constants import mc2, MeV, GeV, TeV, degree

# Public module items
__all__ = ["EventCatalog", "BinnedData", "parse_filename"]

_start = ('pad1', 'i4')
_end = ('pad2', 'i4')


# ==================================================================================================
class EventCatalog:
    """
    SYNOPSIS:
        This class reads all events (photons) from the output file of Monte Carlo simulations performed
        by the `cascadel` code. Then, it applies selections and cuts in these events in order to reduce
        the amount of data to analyze and hence the need for time and memory resources.
        The routines defined here assume that the binary file complies to a well-defined structure of
        the binary file.
        By default, only photons with positive weight and time delay are collected.

    INSTANTIATION ARGUMENTS:
        filename:        file with the simulation results (default:'./data/particles.csc')
        info_only:       if True: only read the file headers (default: False)
        fixt:            if True (default): discard all secondaries with negative time delay, and set zero
                         time delay to all primaries.
        gen:             list of generations to keep in selection (i.e. gen=[0] only keeps primary photons);
                         default: first 10 generations
        emin, emax:      min and max energy of selection (in ergs); default: 1MeV-100TeV
        tmin, tmax:      min and max time delays for selection (in s); default: -1e-10,1.e20
        thpmin, thpmax:  min and max arrival angles for selection (in rad): default: 0-pi
        thdmin, thdmax:  min and max observation angle for selection (in rad); default: 0-pi
        exclude_field:   list if field names to discard in order to reduce memory (and because unnecessary);
                         Default: ('phip')
        chatter:         integer to display various levels of information (0: no display, default: 1)

    ATTRIBUTES:
        filename, chatter, gen, emin, emax, tmin, tmax, thpmin, thpmax, thdmin, thdmax (see above)
        params:     dictionary of simulation parameters
        preproc:    list of preprocessing options
        data:       dictionary of particles properties:
            energy0: primary particle energy (erg)
            primary: index of the parent primary particle (from 1 to the total number of primaries)
            gen:     photon generation number (0 for primaries, >0 for secondaries)
            weight:  photon weight (normalized to 1 single primary photon)
            energy:  photon energy (in units erg)
            time:    photon time delay (in s)
            thetad:  Polar     angle of photon arrival direction (rad)
            phid:    Azimuthal angle of photon arrival direction (rad)
            thetap:  Polar    angle of photon arrival position (rad)
            phip:    Azimuthal angle of photon arrival position (rad)

    EXAMPLES:
        import numpy as np
        from cascapy.constants import GeV, TeV
        from cascapy.cascade import EventCatalog

        file = './data/particles.csc'

        # Check the simulation information:
        EventCatalog(filename=file, info_only=True).info()

        # Read events from file, apply some selection, and display some info:
        cat = EventCatalog(filename=file, emin=10 * GeV, emax=10 * TeV, chatter=False)
        print(f"Max energy (TeV): {np.max(cat.data['energy']) / TeV:.2e}")
        print(f"Number of numerical particles: {cat.params['npart']:d}")
        print(f"Number of physical particles:  {np.sum(cat.data['weight']):.2e}")
    """

    # ----------------------------------------------------------------------------------------------
    def __init__(self, filename, chatter=1, info_only=False, fixt=True,
                 gen=range(10),
                 emin=1*MeV,
                 emax=100*TeV,
                 tmin=-1.e10,
                 tmax=1.e20,
                 thpmin=0,
                 thpmax=np.pi,
                 thdmin=0,
                 thdmax=np.pi,
                 exclude_field=['phip','primary'],
                 lock=None
                 ):

        self.chatter = chatter
        self.filename = filename
        self.info_only = info_only
        self.exclude_field = exclude_field
        self.fmt = 'S10'

        # Selection attributes
        self.gen = gen
        self.emin, self.emax = emin, emax
        self.tmin, self.tmax = tmin, tmax
        self.thpmin, self.thpmax = thpmin, thpmax
        self.thdmin, self.thdmax = thdmin, thdmax

        # Empty containers
        self.data = {}
        self.params = {}
        self.preproc = []

        if self.chatter > 0:
            print(f"\nReading: {self.filename}...")

        try:
            self.fmt = 'S10'
            self._readfile(lock=lock)
        except:
            self.fmt = 'S11'
            self._readfile(lock=lock)

        if not self.info_only:
            if fixt:
                self._correct_time()
            self._apply_selection()
            self._apply_weights()

        if self.chatter > 0:
            print("Done")
    # ----------------------------------------------------------------------------------------------

    # ----------------------------------------------------------------------------------------------
    def _apply_selection(self):
        """
        Apply all selections (based on energy, time, angles etc.) by hard-removing discarded
        events from the current catalog.
        """
        
        # Discard leptons (non-null mass) and unphysical results
        mask = (self.data['mass'] == 0) & (self.data['weight'] > 0) & \
               ((self.data['gen'] == 0) | (self.data['time'] >= -1000000))

        # Selection on photon arrival energy
        # ----------------------------------------------------------------------
        mask &= self.data['energy'] >= self.emin
        mask &= self.data['energy'] <= self.emax

        # Selection on photon arrival time
        # ----------------------------------------------------------------------
        if self.tmin > -1.e-10:
            mask &= self.data['time'] >= self.tmin
        if self.tmax < 1.e20:
            mask &= self.data['time'] <= self.tmax

        # Election on photon generation
        # ----------------------------------------------------------------------
        mask &= np.any([self.data['gen'] == g for g in self.gen], axis=0)
                        
        # Selection on photon arrival position
        # ----------------------------------------------------------------------
        if self.thpmin > 0:
            mask &= self.data['thetap'] >= self.thpmin
        if self.thpmax < np.pi:
            mask &= self.data['thetap'] <= self.thpmax

        # Selection on photon arrival direction
        # ----------------------------------------------------------------------
        if self.thdmin > 0:
            mask &= self.data['thetad'] >= self.thdmin
        if self.thdmax < np.pi:
            mask &= self.data['thetad'] <= self.thdmax

        # Hard remove all non-selected photons to lighten the data structure
        # ----------------------------------------------------------------------
        self.select(mask)
    # ----------------------------------------------------------------------------------------------

    # ----------------------------------------------------------------------------------------------
    def _apply_weights(self):
        """
        Re-weight the events in order to deconvolve the Monte Carlo setup and to recover
        weights corresponding to an intrinsic flat spectrum (i.e. Gamma=0) and detections
        per unit azimuthal observation angle.
        """
    
        # Re-weight to flat spectrum
        # -----------------------------------------------------------------------
        if self.params['E00min'] >= self.params['E00max']:
            norm00 = 1./self.params['E00max']
        else:
            # The intrinsic code spectrum is dN/dE = E^(-Gamma00)/norm00. It is
            # normalised to the particle number
            c00 = 1-self.params['Gamma00']
            if c00 == 0:
                norm00 = np.log(self.params['E00max']/self.params['E00min'])
            else:
                norm00 = (self.params['E00max']**c00-self.params['E00min']**c00)/c00
            norm00 *= self.data["energy0"]**(1.*self.params['Gamma00'])
        self.data["weight"] *= norm00

        # Re-weight to get results per unit azimuthal observation angle (phid)
        # -----------------------------------------------------------------------
        self.data["weight"] /= (2*np.pi)

    # ----------------------------------------------------------------------------------------------
    def _correct_time(self):
        """
        Set time delay of primary photons to 0.0s
        Discard secondaries with negative time delay
        """
    
        self.data['time'][self.data['gen'] == 0] = 0.0
        self.data['time'][self.data['time'] < 0] = 0.0

    # ----------------------------------------------------------------------------------------------
    def _readfile(self, lock=None):
        """
        Main method to read the Fortran binary file produced by the `cascadel`code.
        this method call so independent other method to read the header and
        data section of the file, respectively.
        """

        # Open file
        if lock is not None:
            lock.acquire()

        file = open(self.filename, 'rb')

        # Get parameter values and preprocessing options of the numerical setup
        self._get_param(file)
        dt = np.dtype([_start, ('name', self.fmt), _end])
        hdu = np.fromfile(file, dt, count=1)['name'][0]
        hdu = str(hdu, 'ascii').strip()
        if hdu != 'PARTICLES':
            print("ERROR: section 'PARTICLES' not found in file")
            raise

        if not self.info_only:

            # Get particle information
            self._get_particles(file)

            # Display summary information on this record
            if self.chatter > 1:
                self.info()

        # close file
        file.close()

        if lock is not None:
            lock.release()

    # ----------------------------------------------------------------------------------------------

    # ----------------------------------------------------------------------------------------------
    def _get_param(self, file):
        """
        Read the header section containing all setup parameters
        """

        # get HDU name
        dt = np.dtype([_start, ('name', self.fmt), _end])
        hdu = np.fromfile(file, dt, count=1)['name'][0]
        hdu = str(hdu, 'ascii').strip()
        if hdu != 'PARAMETERS':
            print("WARNING: PARAMETERS HDU not found")

        # get number of parameters
        dt = np.dtype([_start, ('nb', 'i4'), _end])
        nparam = np.fromfile(file, dt, count=1)['nb'][0]

        # read parameters
        self.params = {}
        for i in range(nparam):
            name, value = self._read_val(file)
            if name.strip() in ['nature', 'Neth', 'genmax', 'OMP_num_th', 'Bvectsize', 'number', 'nBseed']:
                value = int(value)
            elif name.strip() in ['E00min', 'E00max', 'Ewth', 'Eeth']:
                value = mc2 * value
            self.params[name] = value
        if 'nBseed' not in self.params:
            self.params['nBseed'] = 1

        # get HDU name
        try:
            dt = np.dtype([_start, ('name', self.fmt), _end])
            hdu = np.fromfile(file, dt, count=1)['name'][0]
            hdu = str(hdu, 'ascii').strip()
        except:
            dt = np.dtype([_start, ('name', 'S11'), _end])
            hdu = np.fromfile(file, dt, count=1)['name'][0]
            hdu = str(hdu, 'ascii').strip()
        if hdu != 'PREPROCESS':
            print("WARNING: PREPROCESS HDU not found")

        # get number of preprocessing options
        dt = np.dtype([_start, ('nb', 'i4'), _end])
        npp = np.fromfile(file, dt, count=1)['nb'][0]

        # read parameters
        self.preproc = []
        dt = np.dtype([_start, ('name', 'S20'), _end])
        for i in range(npp):
            name = np.fromfile(file, dt, count=1)['name'][0]
            self.preproc.append(str(name, 'ascii').strip())
    # ----------------------------------------------------------------------------------------------

    # ----------------------------------------------------------------------------------------------
    def _read_val(self, file):
        """
        Read a single couple: (parameter name, parameter string value)
        RETURNS:
            (name, value)
        """
        dt = np.dtype([_start, ('name', self.fmt), ('value', self.fmt), _end])
        line = np.fromfile(file, dt, count=1)
        name = str(line['name'][0], 'ascii').strip()
        value = float(line['value'][0])
        return name, value
    # ----------------------------------------------------------------------------------------------

    # ----------------------------------------------------------------------------------------------
    def _get_structure(self, file):
        """
        SYNOPSIS:
            Read the file header and defines the binary file structure. The knowledge
            of this structure is required in order to read the rest of the record.
            Each file is composed by a given number of records
            Each record is composed by a given number of blocks (buffersize)
            Each block has information about one particle
        RETURNS:
            nblocks (int):    number of particles in each record
            dt (dtype):       type (i.e. format) of information for one single particle
        """
        
        # Get the number of blocks in each record (i.e. buffer size)
        dt = np.dtype([_start, ('nb', 'i4'), _end])
        nblocks = np.fromfile(file, dt, count=1)['nb'][0]

        # Get the number of fields stored in each block (i.e. for each particle)
        dt = np.dtype([_start, ('nvar', 'i4'), _end])
        nvar = np.fromfile(file, dt, count=1)['nvar'][0]
        
        # Get the info on these variables
        dt = np.dtype([_start, ('name', self.fmt), ('fmt', self.fmt), _end])
        block = []
        for i in range(nvar):
            line = np.fromfile(file, dt, count=1)
            name = str(line['name'][0], 'ascii').strip()
            fmt = bytes(line['fmt'][0]).strip()
            # apply patch for backwards compatibility:
            if fmt == b'f32':
                fmt = b'float32'
            block.append((name, fmt))

        # Build the structure of one full record
        record = [_start, ('line', block, nblocks), _end]
        dt = np.dtype(record)

        return nblocks, dt
    # ----------------------------------------------------------------------------------------------

    # ----------------------------------------------------------------------------------------------
    def _get_particles(self, file):
        """
        Read the file section containing data on all stored particles
        """
        
        # Define file structure and read header file
        nblocks, linetype = self._get_structure(file)
        
        # Read full record as string
        rec = np.fromfile(file, dtype=linetype)

        # Read data
        # ----------------------------------------------------------------------
        properties = ["mass", "energy", "energy0", "time", "thetad", "phid", "thetap", "phip",
                      "weight", "gen", "primary", "bseed"]
        types = ['int', 'float64', 'float64', 'float64', 'float64', 'float64', 'float64',
                 'float64', 'float64', 'int', 'int', 'int']
        self.data = {}
        for key, ttype in zip(properties, types):
                try:
                    self.data[key] = np.array((rec["line"][:][key]).flatten(), dtype=ttype)
                except:
                    if self.chatter > 0:
                        print(f"Warning: key {key} not found")

        # Convert energy from mc2 tp erg
        # ----------------------------------------------------------------------
        self.data['energy'] *= mc2
        self.data['energy0'] *= mc2
        
        # rescale generation number
        # ----------------------------------------------------------------------
        self.data['gen'] = self.data['gen']//2

        # Get redshift of the simulation
        # ----------------------------------------------------------------------
        if 'redshift' in self.params:
            self.params['z'] = self.params['redshift']
        else:
            mask = (self.data['gen'] == 0) & (self.data['weight'] != 0)
            self.params['z'] = np.round(np.mean(self.data['energy0'][mask]/self.data['energy'][mask] - 1), decimals=6)

        # build-in spectrum used in the Monte-Carlo code
        # ----------------------------------------------------------------------
        if 'E00min' not in self.params:
            self.params['E00min'] = np.min(self.data['energy0'][self.data['weight'] != 0])
        if 'E00max' not in self.params:
            self.params['E00max'] = np.max(self.data['energy0'])
        if 'Gamma00' not in self.params:
            self.params['Gamma00'] = 0.75

        # identity of the magnetic structure
        # ----------------------------------------------------------------------
        if 'bseed' not in self.data:
            self.data['bseed'] = self.data['primary'] % self.params['nBseed']

        # count number of numerical particles
        # ----------------------------------------------------------------------
        self.params['npart'] = len(self.data['weight'])

        for key in self.exclude_field:
            if key in self.data:
                del self.data[key]


    # ----------------------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------------------
    def select(self, mask):
        """
        Apply selection corresponding to the mask. Update the number of particles accordingly.
        """
        for key in self.data:
            self.data[key] = self.data[key][mask]
        self.params["npart"] = np.count_nonzero(mask)
# ----------------------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------------------
    def info(self):
        """
        Display the numerical parameters used in the Monte Carlo simulation
        """

        print("SIMULATION SETUP:")
        if len(self.params) > 0:
            for key in self.params:
                if key == 'z':
                    print(f"   {key:14}= {self.params[key]:.4f}")
                elif key == 'E00min':
                    print(f"   {key+' (MeV)':14}= {self.params[key]/MeV:.2e}")
                elif key == 'E00max':
                    print(f"   {key+' (TeV)':14}= {self.params[key]/TeV:.2e}")
                elif key == 'Ewth':
                    print(f"   {key+' (MeV)':14}= {self.params[key]/MeV:.2e}")
                elif key == 'Eeth':
                    print(f"   {key+' (MeV)':14}= {self.params[key]/MeV:.2e}")
                elif key == 'Gamma00':
                    print(f"   {key:14}= {self.params[key]:.2f}")
                elif key == 'npart':
                    print(f"   {key:14}= {self.params[key]:.4e}")
                elif key == 'tmax':
                    print(f"   {key:14}= {self.params[key]:.4e}")
                else:
                    print(f"   {key:14}= {self.params[key]}")
        else:
            print("   no information found")
            
        print("PREPROCESSING OPTIONS:")
        if len(self.preproc) > 0:
            for opt in self.preproc:
                print(f"   {opt:20}")
        else:
            print("   no information found")

        print('===================================================')
        if not self.info_only:
            print("EVENT SELECTION:")
            print(f"   GENERATION: {self.gen}")
            print(f"   TIME:       {max([self.tmin,0]):.2e} <    DT   (s)  < {self.tmax:.2e}")
            print(f"   ENERGY:     {self.emin/GeV:.2e} <    E   (GeV) < {self.emax/GeV:.2e}")
            print(f"   DIRECTION:  {self.thdmin/degree:.2e} < thetad (deg) < {self.thdmax/degree:.2e}")
            print(f"   POSITION:   {self.thpmin/degree:.2e} < thetap (deg) < {self.thpmax/degree:.2e}")
            print("RESULTS")
            print(f"   Number of numerical particles:  {self.params['npart']:.2e}")
            if self.params['npart'] > 0:
                for igen in [0, 1, 2, 3]:
                    if igen in self.gen:
                        mask = self.data['gen'] == igen
                        nn = np.count_nonzero(mask)
                        if nn > 0:
                            print(f"   gen={igen:d}: Fraction={nn/self.params['npart']:.4f},   \
                            {np.min( self.data['time'][mask] ):9.2e} < DT < {np.max( self.data['time'][mask] ):9.2e}")
            print('===================================================')


# ==================================================================================================


# ==================================================================================================
class BinnedData:
    """
    SYNOPSIS:
        This class applies a source intrinsic model (time, energy and angle) and bins all events
        from an event catalog into a multi-d histogram.
        The binned data corresponds to a number of photons per unit detector surface, per unit
        time, and  per unit energy (photon/cm2/s/erg).
        - It is AVERAGED in each ENERGY bin. The total number of photons is retrieved by
        further multiplying by the energy bin width. Alternatively, it must be multiplied by E^2 to
        get an SED (erg/cm2). When no energy bin is provided, or if an energy bin has zero width,
        then the total number of photons is returned (hence in photon/cm2/s)
        - It is AVERAGED in each TIME bin. The total number of photons is retrieved by further
        multiplying by the time bin width. When no time bin is provided or if a time bin has
        zero width, then the total number of photons is returned (photons/cm2/erg)
        - It is INTEGRATED over each observation ANGLES (phid, thetad) bins. It must be further
        divided by the bins width to get the flux density per unit observation solid angle.
        
        WARNING ABOUT BINS FOR THE POLAR OBSERVATION ANGLE (PHID):
        -----------------------------------------------------------
        When no phid-bin is passed, the histogram provides a correct photon flux integrated over
        2*pi. However, when phid-bins are passed, the flux integrated over each bin is actually
        computed by multiplying the differential flux per unit phid at the bin center by the
        bin width.
        This which might be a poor approximation for large bins. For instance if the bin [0,2*pi]
        is passed, tbe result is likely to be different to the integration flux obtained by not
        passing any bin.
        The reason is the procedure uses geometry symmetries in order to increase the
        computational efficiency so that the most fundamental quantities that is computed first
        is a flux per unit phid at a given phid. This makes the polar observation angle a
        specific quantity quite different from all others.


    INSTANTIATION ARGUMENTS:
        ev_cat:     Catalog to bin (as an instance of the EventCatalog() class)
        smodel:     SourceModel for the intrinsic emission
        gbins:      list of generation in the histogram.
        tbins:      list of time contiguous bins edges (s) in increasing order, or (ntbins x 2)
                    array of left and right edges doublets, in increasing order.
        ebins:      list of energy bins edges (erg) in increasing order, or (nebins x 2)
                    array of left and right edges doublets, in increasing order.
        thbins:     list of edges for bins if the poloidal observation angle (rad) in increasing order
        phbins:     list of edges for bins if the azimuthal observations angles (rad) in increasing order
        bmode:      'median' or 'mean' provide the corresponding operation over all realizations of the
                    magnetic structure. 'all' provides an array of results, one for each magnetic structure.
        mask:       mask for additional selection (must be af same size as the event catalog)
        flx_type:   'NE','FE' or 'EFE' for photon flux, energy flux or SED respectively
        copy:       If Trye: make a deep copy of the passed event catalog before playing with it. Default: True
        chatter:    integer to display various levels of information (default: 1)
        stat_level: level used to compute the number of numerical photons contribution to the flux of each energy bin

    ATTRIBUTES:
        smodel,gbins,tbins,embinis,thbins,phbins,chatter (see above)
        params  list of parameters copied fom the EventCatalog instance.
        z:      redshift (from simulation)
        data:   array with the multi-d histogram (in order: gen,phid,thetad,time,energy). When one
                dimension has length 1, it is squeezed to that 'data' is at most of dimension 5.
                As an example, if only one intervalle is given to all five quantities, 'data' is a
                scalar.
        stat:   number of numerical photons contributing significantly to the flux in each multi-d bins
                of the data attribute.
                
    EXAMPLES:
        import numpy as np
        import matplotlib.pyplot as plt
        from cascapy.constants import GeV, TeV, grid, day, degree
        from cascapy.cascade import *
        from cascapy.intrinsic import *

        file = './data/particles.csc'

        # Define energy bins
        emin, emax, nbin = 0.1 * GeV, 10 * TeV, 40
        e, eb, de = grid(emin, emax, nbin, log=True)

        # Define time range
        tmin, tmax = 62., 62 + 30*day

        # Read raw event catalog and apply some selections
        evc = EventCatalog(filename=file, gen=[0, 1, 2, 3], emin=emin, emax=emax,
                           tmax=tmax, thdmax=0.1 * degree, chatter=0)

        # Apply model and bin data
        model = GRB190114C
        bd1 = BinnedData(evc, ebins=eb, tbins=[tmin, tmax], gbins=[0, 1, 2, 3], smodel=model)

        # Plot primary spectrum
        y = bd1.data[0, :]
        plt.plot(e / GeV, e ** 2 * y, ls='--', label='Intrinsic for slow decay (simu)')

        # Overplot the  model in the same time window
        plt.plot(e / GeV, e * model.fe_ave(e, tmin, tmax, absorbed=False),
                 label='intrinsic for pulse (unabsorbed model)', ls=':')

        # Plot the spectrum of secondary particles (with gen>0), as build from the simulation results
        y = np.sum(bd1.data[1:, :], axis=0)
        plt.plot(e / GeV, e ** 2 * y, label='Cascade for slow decay (simu)')

        plt.xscale("log")
        plt.yscale("log")
        plt.xlim(1.e-1, 1.e4)
        plt.ylim(1.e-14, 1.e-10)
        plt.xlabel(r'$E$ (GeV)')
        plt.ylabel(r"$EF_E$ (erg/s/cm2)")
        plt.legend()
        plt.show()
    """

    def __init__(self,
                 ev_cat,
                 smodel,
                 gbins=None,
                 tbins=None,
                 ebins=None,
                 thbins=None,
                 phbins=None,
                 bmode='mean',
                 mask=None,
                 flx_type='NE',
                 chatter=1,
                 copy=True,
                 stat_level=0.9
                 ):
        
        self.chatter = chatter
        if self.chatter > 0:
            print("\nBinning Data")
            print("This might take a minute or so...")

        self.flx_type = flx_type
        self.params = ev_cat.params.copy()
        self.z = ev_cat.params['z']
        self.nbseed = ev_cat.params['nBseed'] if 'nbBeed' in ev_cat.params else 1
        if bmode in ['all', 'mean', 'median']:
            self.bmode = bmode
        else:
            raise ValueError("ERROR: bmode must be 'all', 'mean' or 'median'...")

        self.smodel = smodel

        # selection of generations
        self.gbins = gbins
        if gbins is None:
            self.ngen = 1
        else:
            self.ngen = len(gbins)
        
        # theta_d bins
        self.thbins = thbins
        self.nth = 1 if thbins is None else len(thbins)-1
        
        # phi_d bins
        self.phbins = phbins
        self.nph = 1 if phbins is None else len(phbins)-1

        # energy bins must be contiguous in order to use the numpy histogram
        if ebins is None:
            self.ne = 1
            self.ebins = None
        else:
            self.ebins = np.atleast_1d(ebins)
            if len(self.ebins.shape) == 1:
                self.ebins = np.array([self.ebins[:-1], self.ebins[1:]]).T
            pb = np.any(self.ebins[:, :-1] >= self.ebins[:, 1:])
            if self.ebins.shape[0] > 1:
                pb = pb or np.any(self.ebins[1, :-1] < self.ebins[0, 1:])
            if pb:
                raise ValueError("ERROR: Energy bins must be sorted and not overlapping")
            else:
                ebins = np.unique(self.ebins.flatten())
            self.ne = self.ebins.shape[0]
        eind = [np.where(ebins == ee)[0][0] for ee in self.ebins[:, 0]]

        # Times bins can be contiguous or not.
        if tbins is None:
            self.nt = 1
        else:
            self.tbins = np.atleast_1d(tbins)
            if len(self.tbins.shape) == 1:
                self.tbins = np.array([self.tbins[:-1], self.tbins[1:]]).T
            pb = np.any(self.tbins[:, :-1] >= self.tbins[:, 1:])
            if self.tbins.shape[0] > 1:
                pb = pb or np.any(self.tbins[1, :-1] < self.tbins[0, 1:])
            if pb:
                raise ValueError("ERROR: Time bins must be sorted and not overlapping")
            self.nt = self.tbins.shape[0]

        if copy:
            # Make a deepcopy
            data0 = deepcopy(ev_cat.data)
        else:
            # Modify original data
            data0 = ev_cat.data

        # Apply mask if provided
        if mask is not None:
            for key in data0:
                data0[key] = data0[key][mask]

        # Adapt weight to the chosen flux type
        if self.flx_type == 'FE':
            data0['weight'] *= data0['energy']
        elif self.flx_type == 'EFE':
            data0['weight'] *= data0['energy']**2

        # Apply Spectral Model
        data0['weight'] *= self.nbseed / (1 + self.z)

        # data holder
        self.data = np.zeros((self.nbseed, self.ngen, self.nph, self.nth, self.nt, self.ne))
        if self.bmode in ['median', 'mean']:
            self.stat = np.zeros((self.ngen, self.nph, self.nth, self.nt, self.ne))
        else:
            self.stat = np.zeros((self.nbseed, self.ngen, self.nph, self.nth, self.nt, self.ne))

        data0 = data0

        # Azimuthal, observation angle -----------------------
        for iph in range(self.nph):

            # Apply Angular Model
            if self.phbins is None:
                data0['weight'] *= self.smodel.amodel.phi_integration(data0['thetap'], data0['phid'])
                data1 = data0
            else:
                data1 = data0.copy()
                data1['weight'] = data1['weight'].copy()
                phid0 = 0.5*(self.phbins[iph]+self.phbins[iph+1])
                data1['weight'] *= self.smodel.amodel.dNdWe(data0['thetap'], data0['phid'], phid0) \
                    * (self.phbins[iph+1]-self.phbins[iph])

            del data1['thetap']
            del data1['phid']

            # Generation --------------------------------------
            for igen in range(self.ngen):

                if self.gbins is None:
                    data2 = data1
                else:
                    data2 = {}
                    mask = data1['gen'] == igen
                    for key in data1:
                        data2[key] = data1[key][mask]
                        data1[key] = data1[key][~mask]
                del data2['gen']

                # Poloidal, observation angle -----------------
                for ith in range(self.nth):

                    if self.thbins is None:
                        data3 = data2
                    else:
                        data3 = {}
                        mask = (data2['thetad'] >= self.thbins[ith]) & (data2['thetad'] < self.thbins[ith+1])
                        for key in data2:
                            data3[key] = data2[key][mask]
                            data2[key] = data2[key][~mask]
                    del data3['thetad']

                    # Time bins -------------------------------
                    for it in range(self.nt-1, -1, -1):

                        # Apply Time model
                        if self.tbins is None:
                            t1 = 0.0 - data3['time']
                            t2 = 1.e20 - data3['time']
                            w = data3['weight'] * self.smodel.ne(data3['energy0'] / (1 + self.z), t1, t2)
                        else:
                            mask = (data3['time'] <= self.tbins[it, 1])
                            for key in data3:
                                data3[key] = data3[key][mask]
                            t1 = self.tbins[it, 0]-data3['time']
                            t2 = self.tbins[it, 1]-data3['time']
                            w = data3['weight'] * self.smodel.ne(data3['energy0']/(1 + self.z), t1, t2)

                        for ibseed in range(self.nbseed):

                            if self.nbseed <= 1:
                                ee = data3['energy']
                                ww = w
                            else:
                                mask = data3['bseed'] == ibseed
                                ee = data3['energy'][mask]
                                ww = w[mask]

                            # Energy bins -------------------------
                            self.data[ibseed, igen, iph, ith, it, :], _ = \
                                np.histogram(ee, weights=ww, bins=ebins, density=False)

                            # Get differential flux for finite size energy bins
                            if ebins is not None:
                                de = (self.ebins[:, 1]-self.ebins[:, 0])
                                self.data[ibseed, igen, iph, ith, it, de > 0] /= de

                            # Get differential flux for finite size time bins
                            if self.tbins is not None:
                                dt = (self.tbins[it, 1]-self.tbins[it, 0])
                                if dt > 0:
                                    self.data[ibseed, igen, :, ith, it, :] /= dt

                            # statistical error for each magnetic seed
                            if self.bmode not in ['median', 'mean']:
                                self.stat[ibseed, igen, iph, ith, it, :] = self.compute_stat(ee, ww, level=stat_level)

                        # statistical error for average magnetic structures
                        if self.bmode in ['median', 'mean']:
                            self.stat[igen, iph, ith, it, :] = self.compute_stat(data3['energy'], w, level=stat_level)

        # Average/median over realisation of the magnetic structure
        if self.bmode == 'median':
            self.data = np.median(self.data, axis=0)
        elif self.bmode == 'mean':
            self.data = np.mean(self.data, axis=0)

        # remove dimensions of length 1.
        self.data = np.squeeze(self.data[..., eind])[()]
        self.stat = np.squeeze(self.stat[..., eind])[()]

        if self.chatter > 0:
            print("Done")

    def compute_stat(self, energy, weight, level=0.9):
        """
        Compute the number of numerical particles contributing at a certain level to the total flux in
        each energy bin. This aims at checking the statistical uncertainty in the model, due to the lack
        of statistics in the Monte Carlo simulation.
        """

        stat = np.zeros(self.ne)
        for ie in range(self.ne):
            ind = (weight > 0) & (energy >= self.ebins[ie, 0]) & (energy < self.ebins[ie, 1])
            if np.count_nonzero(ind) > 0:
                sw = np.cumsum(np.sort(weight[ind])[::-1])
                stat[ie] = np.count_nonzero(sw <= level * sw[-1])
        return stat


# ==================================================================================================
def parse_filename(params, mode=0):
    """
    SYNOPSIS:
        This function computes the file name to be read for given magnetic intensity, magnetic
        coherence length and redshift.
    ARGUMENTS:
        params: dictionary of parameters {name:value} required in the filename parsing. Namely:
                'logB','lambdaB','z'
        mode:   parsing mode (default: 0) used for backward compatibility.
                0 -> /zxxx/particles-zxxxx_Bxxxx_Lxxx.csc
    RETURN:
        filename
    """

    logb = params['logB']
    lambdab = params['lambdaB']
    z = params['z']

    if mode == 0:
        sfile = f"z{int(np.floor(100*z+0.5)):03d}/particles_z{int(np.floor(1000*z+0.5)):04d}"
        sfile += f"_B{int(-logb*100):04d}"
        sfile += f"_L{int(lambdab*100):03d}.csc"
    else:
        sfile = None

    return sfile
# ==================================================================================================


# ==================================================================================================
if __name__ == "__main__":
    pass
# ==================================================================================================
