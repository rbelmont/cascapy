"""
SYNOPSIS:
    This module holds methods to describe the triangle geometry for production of secondary photons.
    Namely, a source is assumed at a given distance D (in comoving frame). A primary photon is
    emitted from this source at an angle alpha (a.k.a. emission angle) with respect to the line of
    sight. It annihilates at a distance x*D from the source (x is here called the annihilation
    distance). Secondary photons are produced in the direction of the observer. This corresponds to
    a defflexion angle delta with respect to the direction of the primary photon. On Earth, this
    secondary photon is observed with an obseervation angle theta, and with a time delay t*D/c.
    
                                  **
                             **
                          X     ) delta
             x       **          ***
                 **                       ***
              **                                  ***
           **   ) alpha                         theta  (   ***
         S * * * * * * * * * * * * * * * * * * * * * * * * * * * * O
                                   D
    
    This 5 quantities (x, t, theta, alpha, delta) are related through 3 trigonometric equations:
        delta = alpha + theta
        sin(theta) = x * sin(delta)
        t = x*(1-cos(delta)) - (1-cos(theta))
    So that most of time, only two of them are required to define a unique geometrie. However, in
    some cases, setting two parameters only defines two distinct geometries.
    
    The methods of this module allows to compute any of the 5 parameters when two of them are set
    (with a additional choice : s=+/-1 when two geometries are possible)


DEPENDENCIES:
    numpy

META:
    author: R. Belmont
    date:   15/12/23

"""


from numpy import pi,sqrt,sin,cos,tan,arcsin,arccos,arctan,arctan2,count_nonzero


# ==================================================================================================
def timedelay(x=None,theta=None,delta=None,alpha=None,s=1):
# ==================================================================================================
    """
    SYNOPSIS:
        This function computes the time delay in a triangle geometry.
        This geometry is defined by primary photons emitted by a source at a comoving distance Dc
        reaching directly the Earth, and secondary photons produced at a given comoving distance x
        and from the source and at a given angle with respect to the line of sight.
        The entire geometry is determined by two parameters that must be provided as arguments (see
        list below).
    ARGUMENTS:
        x:      Comoving distance from the source where secondary photons are produced (in units
                the comoving distance between the Earth and the source.
        theta:  Observation angle between the seconday production site and the line of sight
        delta:  Deflexion angle between the directions of the primary and secondary photons
        alpha:  Emission angle between the secondary production site and the line of sight
        s:      +/- 1: In some situations, there are two possible secondary emission sites
                corresponding to the given couple of parameters. This flag allows to select on of
                the two.
    RETURNS:
        cDt/Dc: Time delay in units Dc/c where Dc us the source comoving distance.
    """

    npar = count_nonzero([ key is None for key in [x,theta,delta,alpha]])
    if npar != 2:
        
        print("ERROR: please provide two and only two parameters to set a unique geometry")
        raise
    
    else:
    
        # if x is provided
        if x is not None:
        
            #if x and theta are provided, two annihilation site exist.
            # Default is the shortest time delay
            if theta is not None:
                sth = sin(theta)
#                cm1 = 1.0-cos(theta)
                cm1 = 2.0*sin(theta/2.0)**2
                C = (theta<=pi) & (theta>0) & (x>=sth)
                test = (s<0) or (x>1)
                res = x + sqrt(x**2-sth**2) - cm1
                return C * ( test*res + (1-test)* 2.0 * cm1 * (1.0-x) / res )

            # if x and delta are provided, two annihilation sites exist if x>1
            # Default is the shortest time delay
            elif delta is not None:
                C = (delta<=pi) & (delta>0) & (x*sin(delta)<=1)
                cm1 = 2.0*sin(delta/2.0)**2
                D = 1 + sqrt(1.0-x**2*cm1*(2.0-cm1))
                test = (s<0) or (x<1)
                return C * ( test*x*cm1*( 1.0 - x*(2.0-cm1)/D ) + (1-test)*( x*cm1 - D ) )

            #if x and alpha are provided, only one annihilation site exists
            else:
                C = (alpha>0) & (alpha<=pi)
                D = 1.0 + sqrt(1.0-2.0*x*cos(alpha)+x**2)
#                return C * (sqrt(1.0-2.0*x*cos(alpha)+x**2) + x - 1.0)
                return C * x * (1.0 +  (x-2.0*cos(alpha))/D )

    
        #if theta is provided (but not x)
        elif theta is not None:
                
            # if theta and delta are provided, only one annihilation site exists
            if delta is not None:
                C = (theta>0) & (theta<=pi) & (delta>0) & (delta<=pi) & (delta>=theta)
                return C * 2.0 * sin((delta-theta)/2.0) * sin(theta/2.0) / cos(delta/2.0)

            #if theta and alpha are provided, only one annihilation site exists
            else:
                C = (theta>0) & (theta<=pi) & (alpha>0) & (alpha<=pi) & (alpha+theta<=pi)
                return C * 2.0 * sin(alpha/2.0) * sin(theta/2.0) / cos((alpha+theta)/2.0)
#                return C * 2.0 * tan(alpha/2.0) * tan(theta/2.0) / (1.0- tan(alpha/2)*tan(theta/2.0))

        # if delta and alpha are provided, only one annihilation site exists
        else:
            C = (alpha>0) & (alpha<=pi) & (delta>0) & (delta<=pi) & (delta>=alpha)
            return C * 2.0 * sin(alpha/2.0) * sin((delta-alpha)/2.0) / cos(delta/2.0)
        
# ==================================================================================================



# ==================================================================================================
def annidistance(t=None,theta=None,delta=None,alpha=None,s=1):
# ==================================================================================================
    """
    SYNOPSIS:
        This function computes the comoving annnihilation distance in a triangle geometry.
        This geometry is defined by primary photons emitted by a source at a comoving distance Dc
        reaching directly the Earth, and secondary photons produced at a given comoving distance x
        and from the source and at a given angle with respect to the line of sight.
        The entire geometry is determined by two parameters that must be provided as arguments (see
        list below).
    ARGUMENTS:
        t:      Time delay in units Dc/c where Dc us the source comoving distance.
        theta:  Observation angle between the seconday production site and the line of sight
        delta:  Deflexion angle between the directions of the primary and secondary photons
        alpha:  Emission angle between the secondary production site and the line of sight
        s:      +/- 1: In some situations, there are two possible secondary emission sites
                corresponding to the given couple of parameters. This flag allows to select on of
                the two.
    RETURNS:
        x:      Comoving distance from the source where secondary photons are produced (in units
                the comoving distance between the Earth and the source).
    """

    npar = count_nonzero([ key is None for key in [t,theta,delta,alpha]] )
    if npar != 2:

        print("ERROR: please provide two and only two parameters to set a unique geometry")
        raise
    
    else:
    
        # if t is provided
        if t is not None:
        
            C = (t>0)
        
            #if t and theta are provided, only one annihilation site exists
            if theta is not None:
                C = C & (theta>0) & (theta<=pi)
                cm1 = 2.0*sin(theta/2.0)**2
                return C * ( t + (2.0+t)*cm1/(t+cm1) ) / 2.0

            # if t and delta are provided, two annihilation sites exist
            # Default is the one closest to the source
            elif delta is not None:
                sd2 = sin(delta/2)
                cm1 = 2*sd2**2
                sdc = sqrt(t*(t+2.0))/(t+1.0)
                D = 1.0 + (1.0+t)*sqrt(1.0-(sdc/sd2)**2)
                C = C & (delta>0) & (delta<=pi) & (sd2>sdc)
                if (s>0):
                    return C * ( t + t*(t+2.0)*(2.0-cm1)/cm1/D  ) / 2.0
                else:
                    return C * ( t + D ) / 2.0

            #if t and alpha are provided, only one annihilation site exists
            else:
                C = C & (alpha>0) & (alpha<=pi)
                cm1 = 2.0 * sin(alpha/2.0)**2
                return C * t * (t+2.0) / 2.0 / (t+cm1)

        #if theta is provided (but not t)
        elif theta is not None:
        
            C = (theta>0) & (theta<=pi)
                
            # if theta and delta are provided, only one annihilation site exists
            if delta is not None:
                C = C & (delta>0) & (delta<=pi)
                return C * sin(theta)/sin(delta)

            #if theta and alpha are provided, only one annihilation site exists
            else:
                C = C & (alpha>0) & (alpha<=pi) & (alpha+theta<pi)
                return C * sin(theta)/sin(alpha+theta)

        # if delta and alpha are provided, only one annihilation site exists
        else:
            C = (alpha>0) & (alpha<=pi) & (delta>0) & (delta<=pi) & (delta>=alpha)
            return C * sin(delta-alpha)/sin(delta)

# ==================================================================================================



# ==================================================================================================
def observationangle(t=None,x=None,delta=None,alpha=None,s=1):
# ==================================================================================================
    """
    SYNOPSIS:
        This function computes the observation angle in a triangle geometry.
        This geometry is defined by primary photons emitted by a source at a comoving distance Dc
        reaching directly the Earth, and secondary photons produced at a given comoving distance x
        and from the source and at a given angle with respect to the line of sight.
        The entire geometry is determined by two parameters that must be provided as arguments (see
        list below).
    ARGUMENTS:
        t:      Time delay in units Dc/c where Dc us the source comoving distance.
        x:      Comoving distance from the source where secondary photons are produced (in units
                the comoving distance between the Earth and the source).
        delta:  Deflexion angle between the directions of the primary and secondary photons
        alpha:  Emission angle between the secondary production site and the line of sight
        s:      +/- 1: In some situations, there are two possible secondary emission sites
                corresponding to the given couple of parameters. This flag allows to select on of
                the two.
    RETURNS:
        theta:  Observation angle between the seconday production site and the line of sight
    """
    
    
    npar = count_nonzero([ key is None for key in [t,x,delta,alpha] ] )
    if npar != 2:

        print("ERROR: please provide two and only two parameters to set a unique geometry")
        raise
    
    else:
    
        # if t is provided
        if t is not None:
        
            C = (t>0)
        
            #if t and x are provided, only one annihilation site exists
            if x is not None:
                C = C & (t>2.0*(x-1.0)) & (t<2.0*x)
                costheta = 1.0 - (t/2.0) * (2.0*x-t)/(t+1.0-x)
                sintheta = sqrt( t * (2.0+t) * (2.0*x-t) * (2.0-2.0*x+t) ) / 2.0 / (t+1.0-x)
                return C * arctan2(sintheta,costheta)

            # if t and delta are provided, two annihilation sites exist
            # Default is the one with smallest observation angle (closest to the source)
            elif delta is not None:
                C = C & (delta>=0) & (delta<pi) & ((t+1.0)*cos(delta/2.0)<1.0 )
                return C * ( delta/2.0 - s * arctan( sqrt(tan(delta/2)**2-t*(t+2.0)) / (t+1.0) ) )

            #if t and alpha are provided, only one annihilation site exists
            else:
                C = C & (alpha>=0) & (alpha<=pi)
                return C * 2 * arctan2( t/(t+2.0) , tan(alpha/2.0) )


        #if x is provided (but not t)
        elif x is not None:
            
            C = (x>0)
            
            # if x and delta are provided, two annihilation sites exist if x>1
            # Default is the one with smallest observation angle (closest to the source)
            if delta is not None:
                sd = sin(delta)
                C = C & (delta>=0) & (delta<=pi) & (x*sd<=1)
                test = (s>0) or (x<1)
                return C * ( test*arcsin(x*sd) + (1-test)*(pi-arcsin(x*sd)) )

            # if x and alpha are provided, only one annihilation site exists
            else:
                C = C & (alpha>0) & (alpha<=pi)
                return C * arctan2( x * sin(alpha) , 1.0-x*cos(alpha) )

        # if delta and alpha are provided, only one annihilation site exists
        else:
            C = (alpha>0) & (alpha<=pi) & (delta>0) & (delta<=pi) & (delta>=alpha)
            return C * (delta - alpha)

# ==================================================================================================




# ==================================================================================================
def deflexionangle(t=None,x=None,theta=None,alpha=None,s=1):
# ==================================================================================================
    """
    SYNOPSIS:
        This function computes the deflexion angle in a triangle geometry.
        This geometry is defined by primary photons emitted by a source at a comoving distance Dc
        reaching directly the Earth, and secondary photons produced at a given comoving distance x
        and from the source and at a given angle with respect to the line of sight.
        The entire geometry is determined by two parameters that must be provided as arguments (see
        list below).
    ARGUMENTS:
        t:      Time delay in units Dc/c where Dc us the source comoving distance.
        x:      Comoving distance from the source where secondary photons are produced (in units
                the comoving distance between the Earth and the source).
        theta:  Observation angle between the seconday production site and the line of sight
        alpha:  Emission angle between the secondary production site and the line of sight
        s:      +/- 1: In some situations, there are two possible secondary emission sites
                corresponding to the given couple of parameters. This flag allows to select on of
                the two.
    RETURNS:
        delta:  Deflexion angle between the directions of the primary and secondary photons
    """

    npar = count_nonzero([ key is None for key in [t,x,theta,alpha] ] )
    if npar != 2:

        print("ERROR: please provide two and only two parameters to set a unique geometry")
        raise

    else:
    
        # if t is provided
        if t is not None:
        
            C = (t>0)
        
            #if t and x are provided, only one annihilation site exists
            if x is not None:
                C = C & (t>2.0*(x-1)) & (t<2.0*x)
                cosdelta = 1.0 - t/2.0/x * (2.0+t) / (t+1.0-x)
                sindelta= sqrt( t * (2.0+t) * (2.0*x-t) * (2.0-2.0*x+t) ) / (t+1.0-x) / 2.0 / x
                return C * arctan2(sindelta,cosdelta)

            # if t and theta are provided, only one annihilation site exists
            elif theta is not None:
                C = C & (theta>0) & (theta<=pi)
                return C * 2.0 * arctan( (1.0+t-cos(theta))/sin(theta) )

            #if t and alpha are provided, only one annihilation site exists
            else:
                C = C & (alpha>0) & (alpha<=pi)
                return C * 2.0 * arctan2( 1.0+t-cos(alpha),sin(alpha) )

        #if x is provided (but not t)
        elif x is not None:
        
            C = (x>0)
                
            # if x and theta are provided, two annihilation sites exist
            # Default is the most forward one (in the observer direction)
            if theta is not None:
                sth = sin(theta)
                C = C & (theta>0) & (theta<=pi) & (sth<=x)
                test = (s<0) or (x>1)
                return C * ( test*(pi-arcsin(sth/x)) + (1-test)*arcsin(sth/x) )

            # if x and alpha are provided, only one annihilation site exists
            else:
                C = C & (alpha>0) & (alpha<=pi)
                return C * arctan2( sin(alpha) , (cos(alpha)-x) )

        # if theta and alpha are provided, only one annihilation site exists
        else:
        
            C = (theta>0) & (theta<=pi) & (alpha>0) & (alpha<=pi) & (theta+alpha<=pi)
            return theta + alpha

# ==================================================================================================




# ==================================================================================================
def emissionangle(t=None,x=None,theta=None,delta=None,s=1):
# ==================================================================================================
    """
    SYNOPSIS:
        This function computes the emission angle in a triangle geometry.
        This geometry is defined by primary photons emitted by a source at a comoving distance Dc
        reaching directly the Earth, and secondary photons produced at a given comoving distance x
        and from the source and at a given angle with respect to the line of sight.
        The entire geometry is determined by two parameters that must be provided as arguments (see
        list below).
    ARGUMENTS:
        t:      Time delay in units Dc/c where Dc us the source comoving distance.
        x:      Comoving distance from the source where secondary photons are produced (in units
                the comoving distance between the Earth and the source).
        theta:  Observation angle between the seconday production site and the line of sight
        delta:  Deflexion angle between the directions of the primary and secondary photons
        s:      +/- 1: In some situations, there are two possible secondary emission sites
                corresponding to the given couple of parameters. This flag allows to select on of
                the two.
    RETURNS:
        alpha:  Emission angle between the secondary production site and the line of sight
    """

    npar = count_nonzero([ key is None for key in  [t,x,theta,delta] ] )
    if npar != 2:

        print("ERROR: please provide two and only two parameters to set a unique geometry")
        raise

    else:
    
        # if t is provided
        if t is not None:
        
            C = (t>0)
        
            #if t and x are provided, only one annihilation site exists
            if x is not None:
                C = C & (t>2.0*(x-1)) & (t<2.0*x)
                cosalpha = 1.0 - t*(t+2.0-2.0*x)/(2.0*x)
                sinalpha = sqrt( t * (2.0+t) * (2.0*x-t) * (2.0-2.0*x+t) ) / 2.0 / x
                return C * arctan2(sinalpha,cosalpha)
    
            # if t and theta are provided, only one annihilation site exists
            elif theta is not None:
                C = C & (theta>0) & (theta<=pi)
                return C * 2.0 * arctan( t / (t+2.0) / tan(theta/2.0) )

            # if t and delta are provided, two annihilation sites exist
            # Default is the one with smallest emission angle (closest to the source)
            else:
                cd = (t+1)*cos(delta/2)
                C = C & (delta>0) & (delta<=pi) & (cd<1)
#                return C * ( delta/2 - s*arccos(cd) )
                return C * ( delta/2.0 + s * arctan( sqrt(tan(delta/2)**2-t*(t+2.0)) / (t+1.0) ) )

        #if x is provided (but not t)
        elif x is not None:
                
            C = (x>0)
            
            #if x and theta are provided, two annihilation site exist.
            # Default is the shortest time delay
            if theta is not None:
                sth = sin(theta)
                C = C & (theta>0) & (theta<=pi) & (sth<=x)
                test = (s<0) or (x>1)
                return C * ( test*(pi-arcsin(sth/x)) + (1-test)*arcsin(sth/x) - theta )


            # if x and delta are provided, two annihilation site exist if x>1
            # Default is the one with smallest emission angle (closest to the source)
            else:
                sd = sin(delta)
                C = C & (delta>0) & (delta<=pi) & (x*sd<=1)
                test = (s>0) or (x<1)
                return C * ( delta  - (test*arcsin(x*sd) + (1-test)*(pi-arcsin(x*sd)) ) )

        # if delta and theta are provided
        else:
            C = (theta>0) & (theta<=pi) & (delta>0) & (delta<=pi) & (delta>=theta)
            return  C * (delta-theta)

# ==================================================================================================

# ==================================================================================================


def test():

#     from t, x -----------------------------------------------------

#    for x in [0.1,1.1]:
#        t = np.linspace(np.max([0.00001,2*(x-1)]),2*x,300)
#
#        theta = observationangle(x=x,t=t)
#        plt.plot(t,timedelay(theta=theta,x=x)/t)
#        plt.plot(t,timedelay(theta=theta,x=x,s=-1)/t,ls='--')
#        plt.ylim(0,2)
#        plt.xlabel('t')
#        plt.ylabel('t/t')
#        plt.title(f't,x : theta, x={x:.1f}')
#        plt.show()
#
#        alpha = emissionangle(x=x,t=t)
#        plt.plot(t,timedelay(alpha=alpha,x=x)/t)
#        plt.ylim(0,2)
#        plt.xlabel('t')
#        plt.ylabel('t/t')
#        plt.title(f't,x : alpha, x={x:.1f}')
#        plt.show()
#
#        delta = deflexionangle(x=x,t=t)
#        plt.plot(t,timedelay(delta=delta,x=x)/t)
#        plt.plot(t,timedelay(delta=delta,x=x,s=-1)/t,ls='--')
#        plt.ylim(0,2)
#        plt.xlabel('t')
#        plt.ylabel('t/t')
#        plt.title(f't,x : delta, x={x:.1f}')
#        plt.show()


    # from t, theta -------------------------------------------------
    
#    for t in [0.1,1.5,2.5]:
#        theta = np.linspace(0.0001,np.pi-0.0001,300)
#
#        x = annidistance(t=t,theta=theta)
#        plt.plot(theta,observationangle(t=t,x=x)/theta)
#        plt.ylim(0,2)
#        plt.xlabel('theta')
#        plt.ylabel('theta/theta')
#        plt.title(f't,theta : x, t={t:.1f}')
#        plt.show()
#
#        delta = deflexionangle(t=t,theta=theta)
#        plt.plot(theta,observationangle(t=t,delta=delta)/theta)
#        plt.plot(theta,observationangle(t=t,delta=delta,s=-1)/theta,ls='--')
#        plt.ylim(0,2)
#        plt.xlabel('theta')
#        plt.ylabel('theta/theta')
#        plt.title(f't,theta : delta, t={t:.1f}')
#        plt.show()
#
#        alpha = emissionangle(t=t,theta=theta)
#        plt.plot(theta,observationangle(t=t,alpha=alpha)/theta)
#        plt.ylim(0,2)
#        plt.xlabel('theta')
#        plt.ylabel('theta/theta')
#        plt.title(f't,theta : alpha, t={t:.1f}')
#        plt.show()


    # from t, alpha -------------------------------------------------

#    for t in [0.1,1.5,2.5]:
#        alpha = np.linspace(0.0001,np.pi-0.0001,300)
#
#        x = annidistance(t=t,alpha=alpha)
#        plt.plot(alpha,emissionangle(t=t,x=x)/alpha)
#        plt.ylim(0,2)
#        plt.xlabel('alpha')
#        plt.ylabel('alpha/alpha')
#        plt.title(f't,alpha : x, t={t:.1f}')
#        plt.show()
#
#
#        delta = deflexionangle(t=t,alpha=alpha)
#        plt.plot(alpha,emissionangle(t=t,delta=delta)/alpha)
#        plt.plot(alpha,emissionangle(t=t,delta=delta,s=-1)/alpha,ls='--')
#        plt.ylim(0,2)
#        plt.xlabel('alpha')
#        plt.ylabel('alpha/alpha')
#        plt.title(f't,alpha : delta, t={t:.1f}')
#        plt.show()
#
#        theta = observationangle(t=t,alpha=alpha)
#        plt.plot(alpha,emissionangle(t=t,theta=theta)/alpha)
#        plt.ylim(0,2)
#        plt.xlabel('alpha')
#        plt.ylabel('alpha/alpha')
#        plt.title(f't,alpha : theta, t={t:.1f}')
#        plt.show()


    # from t, delta -------------------------------------------------

#    for t in [0.1,1.5,2.5]:
#        delta = np.linspace(2*arcsin(sqrt(t*(t+2.0))/(t+1.0))+0.0001,np.pi-0.00001,300)
#
#        x = annidistance(t=t,delta=delta,s=1)
#        plt.plot(delta,deflexionangle(t=t,x=x)/delta)
#        plt.ylim(0,2)
#        plt.xlabel('delta')
#        plt.ylabel('delta/delta')
#        plt.title(f't,delta : x, t={t:.1f}')
#        plt.show()
#
#        theta = observationangle(t=t,delta=delta,s=1)
#        plt.plot(delta,deflexionangle(t=t,theta=theta)/delta)
#        plt.ylim(0,2)
#        plt.xlabel('delta')
#        plt.ylabel('delta/delta')
#        plt.title(f't,delta : theta, t={t:.1f}')
#        plt.show()
#
#        alpha = emissionangle(t=t,delta=delta,s=1)
#        plt.plot(delta,deflexionangle(t=t,alpha=alpha)/delta)
#        plt.ylim(0,2)
#        plt.xlabel('delta')
#        plt.ylabel('delta/delta')
#        plt.title(f't,delta : alpha, t={t:.1f}')
#        plt.show()


    # from x, theta -------------------------------------------------
#    for x in [0.1,1.1]:
#        if (x<1):
#            theta = np.linspace(0.00001,np.arcsin(x)-0.00001,150)
#        else:
#            theta = np.linspace(0.00001,np.pi,300)
#            
#        t = timedelay(x=x,theta=theta)
#        plt.plot(theta,observationangle(t=t,x=x)/theta)
#        t = timedelay(x=x,theta=theta,s=-1)
#        plt.plot(theta,observationangle(t=t,x=x)/theta,ls='--')
#        plt.ylim(0,2)
#        plt.xlabel('theta')
#        plt.ylabel('theta/theta')
#        plt.title(f'theta,x : t, x={x:.1f}')
#        plt.show()
#
#        alpha = emissionangle(x=x,theta=theta)
#        plt.plot(theta,observationangle(alpha=alpha,x=x)/theta)
#        alpha = emissionangle(x=x,theta=theta,s=-1)
#        plt.plot(theta,observationangle(alpha=alpha,x=x)/theta,ls='--')
#        plt.ylim(0,2)
#        plt.xlabel('theta')
#        plt.ylabel('theta/theta')
#        plt.title(f'theta,x : alpha, x={x:.1f}')
#        plt.show()
#
#        delta = deflexionangle(x=x,theta=theta)
#        plt.plot(theta,observationangle(delta=delta,x=x)/theta)
#        delta = deflexionangle(x=x,theta=theta,s=-1)
#        plt.plot(theta,observationangle(delta=delta,x=x,s=-1)/theta,ls='--')
#        plt.ylim(0,2)
#        plt.xlabel('theta')
#        plt.ylabel('theta/theta')
#        plt.title(f'theta,x : delta, x={x:.1f}')
#        plt.show()

    
    # from x, alpha -------------------------------------------------
#    for x in [0.1,1.1]:
#        alpha = np.linspace(0.00001,np.pi,300)
#            
#        t = timedelay(x=x,alpha=alpha)
#        plt.plot(alpha,emissionangle(t=t,x=x)/alpha)
#        plt.ylim(0,2)
#        plt.xlabel('alpha')
#        plt.ylabel('alpha/alpha')
#        plt.title(f'alpha,x : t, x={x:.1f}')
#        plt.show()
#
#        theta = observationangle(x=x,alpha=alpha)
#        plt.plot(alpha,emissionangle(theta=theta,x=x)/alpha)
#        plt.plot(alpha,emissionangle(theta=theta,x=x,s=-1)/alpha,ls='--')
#        plt.ylim(0,2)
#        plt.xlabel('alpha')
#        plt.ylabel('alpha/alpha')
#        plt.title(f'alpha,x : theta, x={x:.1f}')
#        plt.show()
#
#        delta = deflexionangle(x=x,alpha=alpha)
#        plt.plot(alpha,emissionangle(delta=delta,x=x)/alpha)
#        plt.plot(alpha,emissionangle(delta=delta,x=x,s=-1)/alpha,ls='--')
#        plt.ylim(0,2)
#        plt.xlabel('alpha')
#        plt.ylabel('alpha/alpha')
#        plt.title(f'alpha,x : delta, x={x:.1f}')
#        plt.show()

    # from x, delta -------------------------------------------------

#    for x in [0.1,1.1]:
#        if x<1:
#            delta = np.linspace(0.00001,np.pi-0.0001,300)
#        else:
#            delta = np.linspace(pi-arcsin(1.0/x),pi-0.00001,300)
#        
#        t = timedelay(x=x,delta=delta)
#        plt.plot(delta,deflexionangle(t=t,x=x)/delta)
#        plt.ylim(0,2)
#        plt.xlabel('delta')
#        plt.ylabel('delta/delta')
#        plt.title(f'delta,x : t, x={x:.1f}')
#        plt.show()
#
#        theta = observationangle(x=x,delta=delta)
#        plt.plot(delta,deflexionangle(theta=theta,x=x)/delta)
#        plt.plot(delta,deflexionangle(theta=theta,x=x,s=-1)/delta,ls='--')
#        plt.ylim(0,2)
#        plt.xlabel('delta')
#        plt.ylabel('delta/delta')
#        plt.title(f'delta,x : theta, x={x:.1f}')
#        plt.show()
#
#        alpha = emissionangle(x=x,delta=delta)
#        plt.plot(delta,deflexionangle(alpha=alpha,x=x)/delta)
#        plt.ylim(0,2)
#        plt.xlabel('delta')
#        plt.ylabel('delta/delta')
#        plt.title(f'delta,x : alpha, x={x:.1f}')
#        plt.show()


    # from theta, alpha ---------------------------------------------
    
#    for alpha in [np.pi/10,np.pi/3,np.pi/2,np.pi*2/3.]:
#        theta =  np.linspace(0.0001,np.pi-alpha-0.0001,300)
#        
#        t = timedelay(theta=theta,alpha=alpha)
#        plt.plot(theta,observationangle(t=t,alpha=alpha)/theta)
#        plt.ylim(0,2)
#        plt.xlabel('theta')
#        plt.ylabel('theta/theta')
#        plt.title(f'alpha,theta : t, alpha={alpha:.1f}')
#        plt.show()
#
#        x = annidistance(theta=theta,alpha=alpha)
#        plt.plot(theta,observationangle(x=x,alpha=alpha)/theta)
#        plt.ylim(0,2)
#        plt.xlabel('theta')
#        plt.ylabel('theta/theta')
#        plt.title(f'alpha,theta : x, alpha={alpha:.1f}')
#        plt.show()
#
#        delta = deflexionangle(theta=theta,alpha=alpha)
#        plt.plot(theta,observationangle(delta=delta,alpha=alpha)/theta)
#        plt.ylim(0,2)
#        plt.xlabel('theta')
#        plt.ylabel('theta/theta')
#        plt.title(f'alpha,theta : delta, alpha={alpha:.1f}')
#        plt.show()

    # from theta, delta ---------------------------------------------

#    for delta in [np.pi/10,np.pi/3,np.pi/2,np.pi*2/3.]:
#        theta =  np.linspace(0.0001,delta-0.0001,300)
#        
#        t = timedelay(theta=theta,delta=delta)
#        plt.plot(theta,observationangle(t=t,delta=delta)/theta)
#        plt.plot(theta,observationangle(t=t,delta=delta,s=-1)/theta,ls='--')
#        plt.ylim(0,2)
#        plt.xlabel('theta')
#        plt.ylabel('theta/theta')
#        plt.title(f'delta,theta : t, delta={delta:.1f}')
#        plt.show()
#
#        x = annidistance(theta=theta,delta=delta)
#        plt.plot(theta,observationangle(x=x,delta=delta)/theta)
#        plt.plot(theta,observationangle(x=x,delta=delta,s=-1)/theta,ls='--')
#        plt.ylim(0,2)
#        plt.xlabel('theta')
#        plt.ylabel('theta/theta')
#        plt.title(f'delta,theta : x, delta={delta:.1f}')
#        plt.show()
#
#        alpha = emissionangle(theta=theta,delta=delta)
#        plt.plot(theta,observationangle(alpha=alpha,delta=delta)/theta)
#        plt.ylim(0,2)
#        plt.xlabel('theta')
#        plt.ylabel('theta/theta')
#        plt.title(f'delta,theta : alpha, delta={delta:.1f}')
#        plt.show()


    # from alpha, delta ---------------------------------------------


#    for delta in [np.pi/10,np.pi/3,np.pi/2,np.pi*2/3.]:
#        alpha =  np.linspace(0.0001,delta-0.0001,300)
#        
#        t = timedelay(alpha=alpha,delta=delta)
#        plt.plot(alpha,emissionangle(t=t,delta=delta)/alpha)
#        plt.plot(alpha,emissionangle(t=t,delta=delta,s=-1)/alpha,ls='--')
#        plt.ylim(0,2)
#        plt.xlabel('alpha')
#        plt.ylabel('alpha/alpha')
#        plt.title(f'delta,alpha : t, delta={delta:.1f}')
#        plt.show()
#
#        x = annidistance(alpha=alpha,delta=delta)
#        plt.plot(alpha,emissionangle(x=x,delta=delta)/alpha)
#        plt.plot(alpha,emissionangle(x=x,delta=delta,s=-1)/alpha,ls='--')
#        plt.ylim(0,2)
#        plt.xlabel('alpha')
#        plt.ylabel('alpha/alpha')
#        plt.title(f'delta,alpha : x, delta={delta:.1f}')
#        plt.show()
#
#        theta = observationangle(alpha=alpha,delta=delta)
#        plt.plot(alpha,emissionangle(theta=theta,delta=delta)/alpha)
#        plt.ylim(0,2)
#        plt.xlabel('alpha')
#        plt.ylabel('alpha/alpha')
#        plt.title(f'delta,alpha : theta, delta={delta:.1f}')
#        plt.show()

    pass


if __name__ == "__main__":
    import numpy as np
    import matplotlib.pyplot as plt
    test()
    pass
