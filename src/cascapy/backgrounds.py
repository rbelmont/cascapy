"""
SYNOPSIS:
    This Module defines models for cosmological backgrounds, namely CMB, and EBL.
    In the later case, it also provides a method to compute the EBL absorption.

DEPENDENCIES:
    environ, numpy, scipy

META:
    author: R. Belmont
    date:   23/05/24
"""

# Intrinsic packages
from os import environ

# Public packages
import numpy as np
from scipy.interpolate import RegularGridInterpolator as Interpnd

# Current package
from .constants import TeV, hp, c, kb, lC, mc2

# Public module items
__all__ = ["EBL", "CMB", "BKG"]


# ==================================================================================================
class EBL:
    """
    SYNOPSIS:
        This class defines a model for the Extragalactic Background Light (EBL) and a source at
        a given redshift.
        WARNING: It relies on tabulated EBL data files. Make sure the EBL_DATA environment variable is
        set and points to the correct location for these files.
    
    ARGUMENTS:
        model:  EBL model. So far, only 'Dominguez' is implemented.
        z:      Source redshift (used to compute absorption)

    ATTRIBUTES:
        model, z


    EXAMPLES:
        # Plot the extinction coefficient for a source at z=0.6
        import numpy as np
        import matplotlib.pyplot as plt
        from cascapy.backgrounds import EBL
        from cascapy.constants import GeV,TeV
        ebl = EBL(z=0.6)
        E = np.logspace(np.log10(10 * GeV), np.log10(10 * TeV), 100)
        plt.loglog(E / GeV, ebl.fabs(E))
        plt.ylim(1.e-6, 2)
        plt.xlabel('Observed Energy (GeV)')
        plt.ylabel('Absorption coefficient')
        plt.show()
    """

    def __init__(self, model='Dominguez', z=0):

        self.model = model
        self.z = z
        _ebl_path = environ["EBL_DATA"]

        if model == 'Dominguez':
        
            # EBL Absorption -----------------------------------------------------------------------
            filename = _ebl_path+"Dominguez/tau_dominguez_cta/tau_dominguez11_cta.txt"

            f = open(filename, "r")
            for i in range(3):
                f.readline()
            zarr = np.array([float(val) for val in f.readline().replace(',', '  ').split()[2:]])
            zarr = np.insert(zarr, 0, 0.)
            f.close()

            data = np.loadtxt(filename, skiprows=5, dtype=np.float64)
            earr = np.array(data[:, 0])*TeV  # from TeV to erg
            tau = 1.*data[:, 1:].T
            tau = np.insert(tau, 0, np.zeros_like(earr), axis=0)
            self._tau0 = Interpnd((zarr, np.log(earr)), tau, bounds_error=False, fill_value=0.0)

            # EBL density --------------------------------------------------------------------------
            filename = _ebl_path+"Dominguez/ebl_dominguez11_fmt.txt"

            f = open(filename, "r")
            zcarr = np.array([np.float64(val) for val in f.readline().replace(',', '  ').split()[1:]], dtype=np.float64)
            f.close()

            data = np.loadtxt(filename, skiprows=1, dtype=np.float64)
            ecarr = hp*c / (data[::-1, 0]*1.e-4)
            eenez = 4.*np.pi * 1.e-6 * data[::-1, 1:] / c
            for [i, zz] in enumerate(zcarr):
                eenez[:, i] = eenez[:, i] * (1.0 + zz)**3

            self._eue = Interpnd((np.log(ecarr), zcarr), np.log(eenez), bounds_error=False, fill_value=-1.e100)

        else:
        
            raise ValueError(f"Error in initializing EBL model: unknown model {model}")

    def tau_abs(self, energy):
        """
        Function that returns the absorption optical depth as a function of energy
        (energy as observed at z=0)
        """
        return self._tau0((self.z, np.log(energy)))

    def fabs(self, energy):
        """
        Function that returns the absorption (aka extinction) coefficient as a function of energy
        (energy as observed at z=0)
        """
        return (energy < 100*TeV) * np.exp(-self.tau_abs(energy))

    def ne(self, energy, z):
        """
        Function that returns the comoving, differential ebl photon density in 1/ccm3/erg as
        a functon of photon energy and redshift (energy as measured in the local frame at redshift z)
        """
        return self.e2ne(energy, z)/energy**2

    def ene(self, energy, z):
        """
        Function that returns the comoving, differential ebl photon density (times E) in 1/ccm3 as
        a functon of photon energy and redshift (energy as measured in the local frame at redshift z)
        """
        return self.e2ne(energy, z)/energy

    def e2ne(self, energy, z):
        """
        Function that returns the comoving, differential ebl photon density (times E^2) in erg/ccm3 as
        a functon of photon energy and redshift (energy as measured in the local frame at redshift z)
        """
        if z >= 0:
            return np.exp(self._eue((np.log(energy), z)))
        else:
            return np.exp(self._eue((np.log(energy), 0.0))) * (1 + z)**3


# ==================================================================================================
class CMB:
    """
    SYNOPSIS:
        This class defines a model for the Cosmological Microwave Background (CMB).

    ARGUMENTS:

    ATTRIBUTES:
        kT0: CMB temperature at z=0 (erg)
        E0:  CMB average energy at z=0 (erg)
        n0:  CMB photon number density (in photons per comoving cm3)
        u0:  CMB average energy density (in ergs per comoving cm3)

    EXAMPLES:
        import numpy as np
        import matplotlib.pyplot as plt
        from cascapy.backgrounds import CMB
        from cascapy.constants import eV
        cmb = CMB()
        E = np.logspace(np.log10(1.e-5*eV), np.log10(1.e-1*eV), 100)
        for z in [0,0.5,1,2]:
            plt.loglog(E / eV, cmb.e2ne(E,z=z),label=f'z={z}')
        plt.ylim(1.e-18,1.e-10)
        plt.xlabel('Local Energy (eV)')
        plt.ylabel(r'$EF_E$ (erg/ccm3)')
        plt.legend()
        plt.show()
    """

    def __init__(self):
        self.kT0 = 2.725*kb
        aa = 2.701178034
        self.E0 = aa*self.kT0
        self.n0 = 410.5089783
        self.u0 = 4.171848126e-13
        self.nn = 8*np.pi/(lC*mc2)**3

    # temperature
    def kt(self, z):
        """
        CMB temperature as a functon of redshjft (in erg)
        """
        return self.kT0 * (1+z)
    
    # average energy
    def e(self, z):
        """
        Average energy of CMB photons as a function of redshift (in erg)
        """
        return self.E0 * (1+z)
    
    # total number density (photons/cm3)
    def n(self, z):
        """
        Mean number density of CMB photons as a function of redshift (in ph/ccm3)
        """
        return self.n0 * (1+z)**3
        
    # total energy density (erg/cm3)
    def u(self, z):
        """
        Mean energy density of the CMB as a function of redshift (in erg/ccm3)
        """
        return self.u0 * (1+z)**4
        
    # Differential density (phot/cm3/erg)
    def ne(self, et, z):
        """
        Differential photon density of CMB photons as a function of photon energy and
        redshift (in ph/ccm3/erg)
        """
        x = np.exp(-et / self.kt(z))
        return self.nn * et**2 * x / (1.0-x)

    def ene(self, et, z):
        """
        Differential energy density of CMB photons as a function of photon energy and
        redshift (in erg/ccm3/erg)
        """
        return et * self.ne(et, z)

    def e2ne(self, et, z):
        """
        Differential photon SED of CMB photons as a function of photon energy and
        redshift (in erg^2/ccm3/erg)
        """
        return et**2 * self.ne(et, z)


# ==================================================================================================
class BKG:
    """
    SYNOPSIS:
        This class defines a model for the total background (EBL+CMB) density.

    ARGUMENTS:

    ATTRIBUTES:
        cmb: instance of the CMB class
        ebl: instance of the EBL class

    EXAMPLES:
        import numpy as np
        import matplotlib.pyplot as plt
        from cascapy.backgrounds import BKG
        from cascapy.constants import eV
        cmb = BKG()
        E = np.logspace(np.log10(1.e-5*eV), np.log10(1.e1*eV), 100)
        for z in [0,0.5,1,2]:
            plt.loglog(E / eV, cmb.e2ne(E,z=z),label=f'z={z}')
        plt.ylim(1.e-18,1.e-10)
        plt.xlabel('Local Energy (eV)')
        plt.ylabel(r'$EF_E$ (erg/ccm3)')
        plt.legend()
        plt.show()
    """

    def __init__(self):
        self.cmb = CMB()
        self.ebl = EBL()
           
    def ne(self, et, z):
        """
        Differential photon density of background (CMB+EBL)  photons as a function of photon energy and
        redshift (in ph/ccm3/erg)
        """
        return self.cmb.ne(et, z) + self.ebl.ne(et, z)

    def ene(self, et, z):
        """
        Differential energy density of background (CMB+EBL)  photons as a function of photon energy and
        redshift (in erg/ccm3/erg)
        """
        return self.cmb.ene(et, z) + self.ebl.ene(et, z)

    def e2ne(self, et, z):
        """
        Differential SED of background (CMB+EBL)  photons as a function of photon energy and
        redshift (in erg^2/ccm3/erg)
        """
        return self.cmb.e2ne(et, z) + self.ebl.e2ne(et, z)
# ==================================================================================================


# ==================================================================================================
if __name__ == "__main__":
    pass
# ==================================================================================================
