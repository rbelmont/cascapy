"""
SYNOPSIS:
    This module defines a class describing a Lambda-CDM cosmological model.

DEPENDENCIES:
    numpy, scipy

META:
    author: R. Belmont
    date:   23/05/24
"""

# Public packages
import numpy as np
from scipy.integrate import quad
from scipy.optimize import newton

# Current package
from .constants import Mpc, c

# Public module items
__all__ = ["LCDM"]


# ==================================================================================================
class LCDM:
    """
    SYNOPSIS:
        This class describes a flat (Omegak=0) LCDM model without radiation.
        Some properties ar conversions (e.g. time to comoving distance) imply numerical integration
        and/or solving implicit equations. This is done using the `quad` and `newton` methods of
        the scipy package.
        
    ARGUMENTS:
        h0:         Hubble constant at present time (in 1/s). Default: H0=68.7e5/Mpc
        wm:         Matter constant at present time. Default: OmegaM=0.3
        rtol:       relative precision passed to quad and newton (default 1.e-15)
        maxiter:    maximal number of iterations passed to quad and newton (default 50)
        
    ATTRIBUTES:
        H0, Wm (see below)
        Wl: Dark energy constant (at t=0)
        t0: Age of the universe (in s)
        
    EXAMPLES:
        # Plot the evolution of the scale factor
        import numpy as np
        import matplotlib.pyplot as plt
        from cascapy.cosmo import LCDM
        c = LCDM()
        t = np.linspace(-c.t0,c.t0/10,100)
        plt.plot(t,c.a_t(t))
        plt.xlabel("Cosmic Time (s)")
        plt.ylabel("Scale factor a(t)")
        plt.ylim(0, )
        plt.show()

    """

    def __init__(self, h0=68.7e5 / Mpc, wm=0.3, rtol=1.e-15, maxiter=50):
        self.H0 = h0
        self.Wm = wm
        self.Wl = 1.0 - self.Wm
        self._alpha = np.sqrt(self.Wl / self.Wm)
        self._tau = 1.0 / (1.5 * np.sqrt(self.Wl) * self.H0)
        self.t0 = self._tau * np.arcsinh(self._alpha)
        self._tc = 1.5 * self._tau * self._alpha / np.sqrt(1 + self._alpha ** 2)
        self.rtol = rtol
        self.maxiter = maxiter

    # Scale factor and redshift --------------------------------------------------------------------
    @staticmethod
    def a_z(z):
        """
        Scale factor a(z) as a function of source redshift
        """
        return 1.0 / (1.0 + z)

    @staticmethod
    def z_a(a):
        """
        Source redshift z(a) as a function of scale factor
        """
        return 1.0 / a - 1.0

    # Comic time and scale factor or redshift ------------------------------------------------------
    def t_a(self, a):
        """
        Cosmic time t(a) as a function of scale factor
        """
        return self._tau * np.arcsinh(self._alpha * a ** 1.5) - self.t0

    def a_t(self, t):
        """
        Scale factor a(t) as a function of cosmic time
        """
        return (np.sinh((t+self.t0) / self._tau) / self._alpha)**(2. / 3.)

    def t_z(self, z):
        """
        Cosmic time t(z) as a function of source redshift
        """
        return self.t_a(self.a_z(z))

    def z_t(self, t):
        """
        Source redshift z(t) at a function of cosmic time
        """
        return self.z_a(self.a_t(t))

    # Hubble constant ------------------------------------------------------------------------------
    def h_a(self, a):
        """
        Hubble factor H(a) as a function of scale factor
        """
        return self.H0 * np.sqrt(self.Wm/a**3 + self.Wl)

    def h_z(self, z):
        """
        Hubble factor H(z) as a function of redshift
        """
        return self.h_a(self.a_z(z))

    def h_t(self, t):
        """
        Hubble factor H(t) as a function of cosmic time
        """
        return self.h_a(self.a_t(t))

    # Comoving velocity ----------------------------------------------------------------------------
    @staticmethod
    def vc_a(a):
        """
        Comoving velocity vc(a) as a function of scale factor
        """
        return c/a

    def vc_t(self, t):
        """
        Comoving velocity vc(t) as a function of cosmic time
        """
        return c/self.a_t(t)

    @staticmethod
    def vc_z(z):
        """
        Comoving velocity vc(z) as a function of redshift
        """
        return c * (1+z)

    # Comoving distance and time -------------------------------------------------------------------
    def xc_t(self, t1, t2):
        """
        Comoving distance travelled between arbitrary cosmic times t1 and t2
        """
        tg1, tg2 = np.meshgrid(t1, t2)
        xc = np.zeros_like(tg1, dtype=float)
        for i in range(xc.shape[0]):
            for j in range(xc.shape[1]):
                xc[i, j] = quad(self.vc_t, tg1[i, j], tg2[i, j], epsrel=self.rtol, limit=self.maxiter, full_output=1)[0]
        return np.squeeze(xc)
    
    # Starting time as a function of arrival time and comoving distance
    def _yt1(self, t1, t2, xc):
        return self.xc_t(t1, t2) - xc

    def _dyt1(self, t1, t2, xc):
        return -self.vc_t(t1)

    def t1_xc(self, t2, xc):
        """
        Starting cosmic time of photons arriving at cosmic time t2 and having travelled a comoving
        distance Xc
        """
        tg2, xcg = np.meshgrid(t2, xc)
        tg1 = tg2 - xcg/self.vc_t(tg2)
        for i in range(tg1.shape[0]):
            for j in range(tg1.shape[1]):
                tt = np.max([tg1[i, j], -self.t0*.9999999])
                tg1[i, j] = newton(self._yt1, tt, self._dyt1, args=(tg2[i, j], xcg[i, j]), rtol=self.rtol,
                                   maxiter=self.maxiter)
        return np.squeeze(tg1)
    
    # Arrival time as a function of start time and comoving distance
    def _yt2(self, t2, t1, xc):
        return self.xc_t(t1, t2) - xc

    def _dyt2(self, t2):
        return self.vc_t(t2)

    def t2_xc(self, t1, xc):
        """
        Arrival cosmic time of photons starting at cosmic time t1 and travelling a comoving
        distance Xc
        """
        tg1, xcg = np.meshgrid(t1, xc)
        tg2 = tg1 + xcg/self.vc_t(tg1)
        for i in range(tg2.shape[0]):
            for j in range(tg2.shape[1]):
                tg2[i, j] = newton(self._yt2, tg2[i, j], self._dyt2, args=(tg1[i, j], xcg[i, j]),
                                   rtol=self.rtol, maxiter=self.maxiter)
        return np.squeeze(tg2)
    
    # Time and comoving distance for photons arriving at z=0.
    def dc_t(self, t):
        """
        Comoving distance to the Earth of a source having emitted photons at cosmic time t
        """
        return self.xc_t(-t, 0.0)

    def t_dc(self, dc):
        """
        Emission cosmic time of a source at comoving distance Dc to the Earth
        """
        return -self.t1_xc(0.0, dc)

    # Comoving distance and redshift ---------------------------------------------------------------
    def _dxc_dz(self, z):
        return -c / self.h_z(z)

    def xc_z(self, z1, z2):
        """
        Comoving distance travelled between arbitrary cosmic redshifts z1 and z2
        """
        zg1, zg2 = np.meshgrid(z1, z2)
        xcg = np.zeros_like(zg1, dtype=float)
        for i in range(xcg.shape[0]):
            for j in range(xcg.shape[1]):
                xcg[i, j] = quad(self._dxc_dz, zg1[i, j], zg2[i, j], epsrel=self.rtol, limit=self.maxiter,
                                 full_output=1)[0]
        return np.squeeze(xcg)
        
    def _yz1(self, z1, z2, xc):
        return self.xc_z(z1, z2) - xc

    def _dyz1(self, z1, z2, xc):
        return -self._dxc_dz(z1)

    def z1_xc(self, z2, xc):
        """
        Starting redshift of photons arriving at redshift z2 and having travelled a
        comoving distance Xc
        """
        zg2, xcg = np.meshgrid(z2, xc)
        zg1 = zg2 - xcg / self._dxc_dz(zg2)
        for i in range(zg1.shape[0]):
            for j in range(zg1.shape[1]):
                zg1[i, j] = newton(self._yz1, zg1[i, j], self._dyz1, args=(zg2[i, j], xcg[i, j]), rtol=self.rtol,
                                   maxiter=self.maxiter)
        return np.squeeze(zg1)

    def _yz2(self, z2, z1, xc):
        return self.xc_z(z1, z2) - xc

    def _dyz2(self, z2, z1, xc):
        return self._dxc_dz(z2)

    def z2_xc(self, z1, xc):
        """
        Arrival redshift of photons starting at redshift z1 and travelling a comoving
        distance Xc
        """
        zg1, xcg = np.meshgrid(z1, xc)
        zg2 = zg1 + xcg/self._dxc_dz(zg1)
        for i in range(zg2.shape[0]):
            for j in range(zg2.shape[1]):
                zg2[i, j] = newton(self._yz2, zg2[i, j], self._dyz2, args=(zg1[i, j], xcg[i, j]), rtol=self.rtol,
                                   maxiter=self.maxiter)
        return np.squeeze(zg2)

    def dc_z(self, z):
        """
        Comoving distance to the Earth of a source having emitted photons at redshift z
        """
        return self.xc_z(z, 0.0)

    def z_dc(self, dc):
        """
        Emission redshift of a source at comoving distance Dc to the Earth
        """
        return self.z1_xc(0.0, dc)

    # Comoving distance and scale factor ---------------------------------------------------------------
    def _dxc_da(self, a):
        return c / self.h_a(a) / a**2

    def xc_a(self, a1, a2):
        """
        Comoving distance travelled between arbitrary scale factors a1 and a2
        """
        a1g, a2g = np.meshgrid(a1, a2)
        xcg = np.zeros_like(a1g, dtype=float)
        for i in range(xcg.shape[0]):
            for j in range(xcg.shape[1]):
                xcg[i, j] = quad(self._dxc_da, a1g[i, j], a2g[i, j], epsrel=self.rtol, limit=self.maxiter,
                                 full_output=1)[0]
        return np.squeeze(xcg)

    def _ya1(self, a1, a2, xc):
        return self.xc_a(a1, a2) - xc

    def _dya1(self, a1, a2, xc):
        return -self._dxc_da(a1)

    def a1_xc(self, a2, xc):
        """
        Starting scale factor of photons arriving at scale factor a2 and having travelled a
        comoving distance Xc
        """
        a2g, xcg = np.meshgrid(a2, xc)
        a1g = a2g - xcg / self._dxc_da(a2g)
        for i in range(a1g.shape[0]):
            for j in range(a1g.shape[1]):
                aa = max([a1g[i, j], 1.e-10])
                a1g[i, j] = newton(self._ya1, aa, self._dya1, args=(a2g[i, j], xcg[i, j]), rtol=self.rtol,
                                   maxiter=self.maxiter)
        return np.squeeze(a1g)

    def _ya2(self, a2, a1, xc):
        return self.xc_a(a1, a2) - xc

    def _dya2(self, a2, a1, xc):
        return self._dxc_da(a2)

    def a2_xc(self, a1, xc):
        """
        Arrival scale factor of photons starting at scale factor a1 and travelling a
        comoving distance Xc
        """
        a1g, xcg = np.meshgrid(a1, xc)
        a2g = a1g + xcg / self._dxc_da(a1g)
        for i in range(a2g.shape[0]):
            for j in range(a2g.shape[1]):
                a2g[i, j] = newton(self._ya2, a2g[i, j], self._dya2, args=(a1g[i, j], xcg[i, j]), rtol=self.rtol,
                                   maxiter=self.maxiter)
        return np.squeeze(a2g)

    def dc_a(self, a):
        """
        Comoving distance to the Earth of a source having emitted photons at scale factor a
        """
        return self.xc_a(a, 1.0)

    def a_dc(self, dc):
        """
        Emission scale factor of a source at comoving distance Dc to the Earth
        """
        return self.a1_xc(1.0, dc)

    # Luminosity distance --------------------------------------------------------------------------
    def dl_t(self, t):
        """
        Luminosity distance of a source at cosmic time t
        """
        return self.dc_t(t) / self.a_t(t)

    def dl_z(self, z):
        """
        Luminosity distance of a source at redshift z
        """
        return (1+z) * self.dc_z(z)

    def dl_a(self, a):
        """
        Luminosity distance of a source at scale factor a
        """
        return self.dc_a(a) / a
# ==================================================================================================


# ==================================================================================================
if __name__ == "__main__":
    pass
# ==================================================================================================
