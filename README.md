# Cascapy.py

## Synopsis:
This package contains modules and methods to compute the total emission of TeV induced, electromagnetic cascades, as observed on Earth.  
Included modules are:
- `constants`: for universal constants 
- `cosmo`: for the cosmological properties of a LCDM model
- `backgrounds`: for the absorption by the Extragalactic Background Light, EBL and CMB properties
- `intrinsic`: for time-, spectral- and angular properties of primary source
- `cascade`: for the realistic, binned emission
- `tablemodel`: for building tables of models  
- `GammapyTableModel`: for Gammapy compatible table models  

## Author:
R. Belmont  

## History:
12/01/2025: v1.2.0
17/06/2024: v1.1.0  
15/02/2024: v1.0.0  
21/11/2023: v0.2.2  
20/11/2023: v0.2.1  
27/10/2023: v0.2.0  
25/10/2023: v0.1.0  
24/10/2023: v0.0.1  

## Dependencies:
Mandatory packages: `numpy`, `scipy`, `h5py`  
Optional packages: `gammapy`

## Installation

#### Install last version
To install the very last committed version of `cascapy`, run:  
`pip install cascapy@git+https://gitlab.com/rbelmont/cascapy`  
If you want to force re-installation, add the following options: `--force-reinstall --no-deps`  

#### Install a specific distribution
A specific version can be installed from the URL of their tar file. For instance, to install version 0.0.1 from the master branch, run:  
`pip install https://gitlab.com/rbelmont/cascapy/-/blob/master/dist/cascapy-0.1.0.tar.gz`  

Alternatively it is also possible to:
- Download a distribution (.whl or .tar.gz) of the seek version from the 'dist/' directory
- Go to the local directory where the distribution was downloaded
- run `pip install distribution.tar.gz` or `pip install distribution.whl`

#### Install the EBL data
The `cascapy.cosmo` module includes data files used to compute the absorption by the Extragalactic Background Light (EBL). These files are not part of the `cascapy` distribution (large files). They must be installed separately.  
Then, an environment variable `EBL_DATA` must be set so that the package always finds it. The simplest way is to set this variable in the bashrc file. E.g. for tcsh: `setenv EBL_DATA $HOME/cascadel/EBL/`


## Examples:
See the `examples/` directory for examples of python scripts and notebooks.
