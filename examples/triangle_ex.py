"""
Some simple test examples...
"""
import numpy as np
import matplotlib.pyplot as plt
from cascapy.triangle import *
from cascapy.constants import *

# iso-time delay
def test1():
    """
    This function plots the annihilation sites corresponding to given time delays.
    """
    for i,t in enumerate([1.e-3,1.e-2,1.e-1,0.2]):
        a = np.linspace(0.0001,np.pi-0.0001,200)
        x = annidistance(t=t,alpha=a)
        if i==0:
            label = r'$c\Delta t / D_c = $ '+f"{t}"
        else:
            label = f"{t}"
        cc = plt.plot(x*np.cos(a),x*np.sin(a),label=label)
    plt.gca().set_aspect('equal')
    plt.plot([0,1],[0,0],marker='o',color='k',ls='')
    plt.ylim(0,0.5)
    plt.legend(loc='upper center',ncols=4)
    plt.title("Lines of equi time delay")
    plt.show()


# iso-deflexion
def test2():
    """
    This function plots the annihilation sites corresponding to given time deflexion angles.
    """
    for d in np.array([1,10,20,60,90,120,140,170])*degree:
        a = np.linspace(0,d,300)
        x = annidistance(delta=d,alpha=a)
        cc = plt.plot(x*np.cos(a),x*np.sin(a),label=r'$\delta = $ '+f"{d/degree:.0f}°")
    plt.gca().set_aspect('equal')
    plt.plot([0,1],[0,0],marker='o',color='k',ls='')
    plt.ylim(0,1.5)
    plt.xlim(-0.5,1.5)
    plt.legend(loc='upper right')
    plt.title("Lines of equi-deflexion angle")
    plt.show()
    
    
def test3():

    """
    This function plots the two annihilation sites defined by a given time delay and
    a given deflexion angle.
    """

    # iso deflexion angle
    d = 80*degree
    a = np.linspace(0,d,300)
    x = annidistance(delta=d,alpha=a)
    cc = plt.plot(x*np.cos(a),x*np.sin(a),label=r"$\delta=$"+f"{d/degree:.1f}°")

    # iso time delay
    t = 0.2
    a = np.linspace(0,np.pi,500)
    x = annidistance(t=t,alpha=a)
    cc = plt.plot(x*np.cos(a),x*np.sin(a),label=f"ct/D = {t:.1f}")

    # First solution
    s = +1
    a = emissionangle(t=0.2,delta=d,s=s)
    xmax = annidistance(t=0.2,delta=d,s=s)
    plt.plot([xmax*np.cos(a)],[xmax*np.sin(a)],color='k',marker='o')
    x = np.linspace(0,xmax,300)
    cc = plt.plot(x*np.cos(a),x*np.sin(a),color='k')
    lmax = np.sqrt((1-xmax*np.cos(a))**2+(xmax*np.sin(a))**2)
    th = observationangle(t=0.2,delta=d,s=s)
    plt.plot([1,1-lmax*np.cos(th)],[0,lmax*np.sin(th)],color='k')

    # second solution
    s = -1
    a = emissionangle(t=0.2,delta=d,s=s)
    xmax = annidistance(t=0.2,delta=d,s=s)
    plt.plot([xmax*np.cos(a)],[xmax*np.sin(a)],color='k',marker='o')
    x = np.linspace(0,xmax,300)
    cc = plt.plot(x*np.cos(a),x*np.sin(a),color='k')
    lmax = np.sqrt((1-xmax*np.cos(a))**2+(xmax*np.sin(a))**2)
    th = observationangle(t=0.2,delta=d,s=s)
    plt.plot([1,1-lmax*np.cos(th)],[0,lmax*np.sin(th)],color='k')

    plt.gca().set_aspect('equal')
    plt.plot([0,1],[0,0],marker='o',color='k',ls='')
    plt.ylim(0,)
    plt.legend()
    plt.show()

if __name__ == "__main__":
    test1()
    test2()
    test3()
    pass
