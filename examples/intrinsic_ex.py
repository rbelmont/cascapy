"""
Some simple test examples...
"""

import numpy as np
import matplotlib.pyplot as plt
from cascapy.constants import *
from cascapy.intrinsic import *


def test1():
    """
    TIMEMODEL
    Plot the time model integrated in an observation window of 10s, as a function of the observation start time
    """
    Tmodel = PowerLawTimeModel(alpha=1.2, tmin=15, tmax=1.e4)
    t = np.logspace(0,3,100)
    plt.loglog(t,Tmodel.fint(t,t+10))
    plt.xlabel("Window start time (s)")
    plt.ylabel("Flux integrated in a moving window of 10s")
    plt.title(f"Powerlaw model with index {Tmodel.params['alpha']:.1f} starting at t={Tmodel.params['tmin']:.1f}s")
    plt.show()


def test1b():
    """
    Plot a template (i.e. tabulated) time model
    """
    # Define tabulated model
    def f(t):
        g1, g2, tc, tmin, tmax = 1.3, 3.1, 1.e3, 20., 2.e4
        return ((t > tmin) & (t < tmax)) * t ** (-g1) / (tc ** (g2 - g1) + t ** (g2 - g1))

    t = np.logspace(np.log10(10), np.log10(1.e5), 20)
    plt.plot(t, f(t), marker='o', label='Tabulated Data')

    # Define and plot template model
    tmodel = TemplateTimeModel(t, f(t))
    tt = np.logspace(np.log10(10.), np.log10(1.e5), 200)
    plt.plot(tt, tmodel.f(tt), label='Interpolated model')
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim(1.e-21, )
    plt.xlabel('Time (s)')
    plt.ylabel('Flux (arbitrary units)')
    plt.legend()
    plt.show()

def test2():
    # Print flux integrated in the 100GeV,3*TeV band
    Emodel = PowerLawSpectralModel(z=0.42, gamma=1.5, emin=10 * GeV, emax=10 * TeV)
    print("Unabsorbed flux:",Emodel.fint(100*GeV,3*TeV,absorbed=False))
    print("Absorbed flux:  ",Emodel.fint(100*GeV,3*TeV,absorbed=True))

    # Plot absorbed and unabsorbed spectra
    E = np.logspace(np.log10(1*GeV),np.log10(100*TeV),200)
    Emodel = PowerLawSpectralModel(z=0.42, gamma=1.5, emin=10 * GeV, emax=10 * TeV)
    l, = plt.loglog(E/GeV,E*Emodel.f(E),label='model 1')
    plt.loglog(E/GeV,E*Emodel.f(E,absorbed=True),'--',color=l.get_color())
    Emodel.update({'Gamma':2.2,'Emin':5*GeV,'Emax':5*TeV})
    l,= plt.loglog(E/GeV,E*Emodel.f(E),label='model 2')
    plt.loglog(E/GeV,E*Emodel.f(E,absorbed=True),'--',color=l.get_color())
    plt.xlabel('Energy (GeV)')
    plt.ylabel('SED (arbitrary units)')
    plt.ylim(1.e-4,)
    plt.legend()
    plt.title(f"Absorbed and unabsorbed flux for z={Emodel.params['z']}")
    plt.show()

def test2b():
    """
    Define a template spectral model
    """
    # Define tabulated model
    def f(e):
        g1, g2, ec, emin, emax = 1.3, 3.1, 100 * GeV, 10 * GeV, 1 * TeV
        return ((e > emin) & (e < emax)) * e ** (-g1) / (ec ** (g2 - g1) + e ** (g2 - g1))

    e = np.logspace(np.log10(1 * GeV), np.log10(10 * TeV), 20)
    plt.plot(e / GeV, f(e), marker='o', label='Tabulated Data')

    # Define template model
    tmodel = TemplateSpectralModel(e, f(e), fmin=1.e-8)

    # Plot template model
    ee = np.logspace(np.log10(1 * GeV), np.log10(10 * TeV), 200)
    plt.plot(ee / GeV, tmodel.f(ee), label='Interpolated model')
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim(1.e-10, )
    plt.xlabel('Energy (GeV)')
    plt.ylabel(r'$F_E$ (arbitrary units)')
    plt.legend()
    plt.show()


def test3():
    """
    SPECTRALMODEL
    """
    # Plot the spectral model flux (absorbed and unabsorbed) for different rollover indices
    model = CutoffPowerLawSpectralModel(gamma=1.9, emax=15 * TeV, z=0.2)
    model.info()
    E = np.logspace(np.log10(1*GeV),np.log10(1000*TeV),100)
    l,=plt.loglog(E/TeV,E*model.f(E),label='q=1')
    l,=plt.loglog(E/TeV,E*model.f(E,absorbed=True),'--',color=l.get_color())
    model.update({'q':2})
    l,=plt.loglog(E/TeV,E*model.f(E),label='q=2')
    l,=plt.loglog(E/TeV,E*model.f(E,absorbed=True),'--',color=l.get_color())
    model.update({'q':0.5})
    l,=plt.loglog(E/TeV,E*model.f(E),label='q=1/2')
    l,=plt.loglog(E/TeV,E*model.f(E,absorbed=True),'--',color=l.get_color())
    plt.legend()
    plt.xlim(1.e-3,1.e3)
    plt.ylim(1.e-3,1.e1)
    plt.xlabel('Energy E (in TeV)')
    plt.ylabel(r'$E F_E$ (arbitrary units)')
    plt.title(f"CPL model with index {model.params['gamma']:.1f} and cutoff {model.params['emax']/TeV:.1f} TeV at z={model.params['z']:.1f}")
    plt.show()

def test4():
    """
    Angular models
    """
    # Define model and get information about it
    model1 = IsotropicAngularModel()
    print('Model1:')
    model1.info()

    model2 = DiskAngularModel(th_jet=20*degree,th_obs=10*degree)
    print('Model2:')
    model2.info()

    #Plot the angular distribution of the two models
    theta = np.linspace(-np.pi,np.pi,200)

    r = model1.dndwe(theta)
    plt.plot(r*np.cos(theta),r*np.sin(theta),label='Iso')

    r = model2.dndwe(theta)
    plt.plot(r*np.cos(model2.params['th_obs']+theta),r*np.sin(model2.params['th_obs']+theta),
             label=f"Disk, {model2.params['th_jet']/degree} deg")

    plt.plot([0,1],[0,0],color='k',label='Observer')
    plt.gca().set_aspect('equal')
    plt.legend(loc='upper left')
    plt.xlim(-1,1)
    plt.ylim(-1,1)
    plt.show()


def test5():
    """
    SOURCEMODEL
    Compare the reference PL model of GRB190114C with a Log-parabola model
    """
    # Define a model corresponding to reference source GRB190114C.
    model1 = GRB190114C
    model1.info()

    # Build an unnomalized alternative model with a LogParabola spectrum
    Emodel = LogParabolaSpectralModel(gamma=2.51, eta=0.21 * 0.4342, epiv=0.4 * TeV, emin=10 * MeV, emax=10 * TeV, z=0.4245)
    Tmodel = PowerLawTimeModel(alpha=1.6,tmin=6)
    model2 = SourceModel(emodel=Emodel, tmodel=Tmodel)

    # Normalized this second model to the fluence measured in the 62-2454s observation window and in the 0.3-1 TeV energy range. Disply model information.

    from cascapy.cosmo import LCDM
    z = Emodel.params['z']
    Dl = LCDM().dc_z(z) * (1 + z)
    model2.norm = 4e51*(1+0.4245)/(4*np.pi*Dl**2)/model2.f(62., 2454, 0.3 * TeV, 1 * TeV)
    model2.info()

    #Plot the two model spectra at t=1000s
    E = np.logspace(np.log10(1*GeV),np.log10(200*TeV),100)
    t = 1000.

    plt.loglog(E, E * model1.fet(E, t, absorbed=True), color='#1f77b4', label='PowerLaw')
    plt.loglog(E, E * model1.fet(E, t, absorbed=False), ls='--', color='#1f77b4')

    plt.loglog(E, E * model2.fet(E, t, absorbed=True), color='#ff7f0e', label='Log-Parabola')
    plt.loglog(E, E * model2.fet(E, t, absorbed=False), ls='--', color='#ff7f0e')

    plt.ylabel(r"$EF_E$ (erg/s/cm2/erg)")
    plt.xlabel("Energy (TeV)")
    plt.xlim(1.e-3,2.e1)
    plt.ylim(1.e-14,1.e-8)
    plt.title('GRB190114C emission at 1000s')
    plt.legend()
    plt.grid()
    plt.show()


def test6():
    """
    Build a Non-separable Source model from energy, time, and flux grids and plot the
    predicted instantaneous spectrum.
    Here, the array data used to build this model is actually separable in time and
    energy for check purposes. And both models yield similar results.
    However, any flux(energy,time) can be provided.
    The observed shift in maximal and minimal energies is due to the sparce binning used
    to define the model. This can be removed by a denser binning or more appropriate to
    the model boundaries.
    """

    # Model properties
    emin, emax = 0.1 * GeV, 15 * TeV
    tmin, tmax = 13., 10 * day
    z = 0.42

    # Model with separable time and energy
    emodel = LogParabolaSpectralModel(emin=emin, emax=emax, z=z)
    tmodel = LogParabolaTimeModel(tmin=tmin, tmax=tmax)
    model = SourceModel(emodel=emodel, tmodel=tmodel)

    # Setup grids that will define the non-separable model
    egrid = np.logspace(np.log10(0.05 * GeV), np.log10(20 * TeV), 23)
    tgrid = np.logspace(np.log10(10.), np.log10(20 * day), 21)
    flux = np.zeros((len(egrid), len(tgrid)))
    for it, t in enumerate(tgrid):
        flux[:, it] = model.fet(egrid, t)

    # Define model with non-separable time and energy
    model2 = NonSeparableSourceModel(egrid, tgrid, flux, z=z)

    # Plot and compare results
    e = np.logspace(np.log10(emin / 10), np.log10(emax * 10), 100)
    t = 100
    pp = plt.loglog(e / GeV, model.efet(e, t, absorbed=False), ls='--', label='Separable')
    col = pp[0].get_color()
    plt.loglog(e / GeV, model2.efet(e, t, absorbed=False), color=col, label='Non Separable')
    plt.vlines(egrid / GeV, 1.e-2, 1.e1, color='grey', lw=0.5, label='Model grid')

    plt.xlabel(r"Energy (GeV)")
    plt.ylabel(r"$E\ F_E$")
    plt.ylim(1.e-2, 1.e1)
    plt.xlim(4.e-2, 2.e4)
    plt.title(f"Instantaneous spectra at t={t}s")
    plt.legend(loc='upper left')
    plt.show()

if __name__ == '__main__':
    test1()
    test1b()
    test2()
    test2b()
    test3()
    test4()
    test5()
    test6()
    pass
