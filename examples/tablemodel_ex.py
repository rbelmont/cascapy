"""
Some simple test examples...
"""
from cascapy.constants import *
from cascapy.tablemodel import *
from cascapy.intrinsic import GRB190114C
from h5py import File as h5file


# --------------------------------------------------------------------------------------------------
def test_tbins():
    """
    This function builds a set of time bins
    """
    tb = time_bins(pointing_time=60, night_start=1*hour, n_nights=4, night_duration=5*hour, nbins=7)
    print(tb)
    
    
# --------------------------------------------------------------------------------------------------
def test_vbins():
    """
    This function builds a set pf time bins and make a plot of them.
    """
    view_tbins(pointing_time=60, night_start=1*hour, n_nights=4, night_duration=5*hour, nbins=7)


# --------------------------------------------------------------------------------------------------
def test_table():
    """
    This routine illustrates a way to compute and store a table model.
    """
    

    com = ['This table is a test table','It is just for fun']

    tbins = np.logspace(np.log10(60.),np.log10(5*day),16)
    ebins = np.logspace(np.log10(0.1*GeV),np.log10(10*TeV),31)

    # Instantiate the tablemodel class by define some preliminary properties of the table
    TB = TableModel(file='data/newtable.h5', mode='w', simupath= './data/', chatter=1, comments=com)
    
    # Add some more properties (note that these could have been setup directly at the
    # instantiation step
    TB.tbins = tbins
    TB.ebins = ebins
    TB.smodel  = GRB190114C
    TB.igmf    = {'logB':-17, 'lambdaB':6}
    

    # Define the varying parameters of the table:
    gamma = np.linspace(2,3,10)
    alpha = np.linspace(1,2,10)
    TB.vparams = {'gamma':gamma,'alpha':alpha}

    # setup the table structure
    TB.init_table()
    
    # Compute table
    TB.fill_table(chunkfactor=4,nproc=6)
    TB.info()


# --------------------------------------------------------------------------------------------------
def test_vtable():
    """
    This routine shows how to open an existing table, display some info on it, and visualize its
    data
    """

    # Open file
    f = h5file('./data/table.h5', 'r')

    # read data
    TB = TableModel(file=f, mode='r')

    # Display info on table
    TB.info()

    # Visualize data
    TB.spectra()
    TB.lightcurves()

    # close file
    f.close()


# --------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    test_tbins()
    test_vbins()
    test_table()
    test_vtable()
    pass
