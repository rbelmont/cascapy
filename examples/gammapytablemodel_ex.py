"""
Some simple test examples...
"""
from typing_extensions import Unpack

import matplotlib.pyplot as plt
from astropy import units as u
from cascapy.cascade import *
from cascapy.constants import *
from cascapy.tablemodel import *
from cascapy.gammapy import *


# ==================================================================================================
def test_view(file='./data/table.h5'):
    """
    This function illustrates how to build a simple gammapy table model from an hdf5 table
    """
    
    # Build model with a slight smoothing and a frozen parameter
    models = CascadeTemporalSpectralModel(file, smooth_dex=0.1, frozen={'alpha': 1.17})

    # Visualize table
    models.spectra()
    models.lightcurves()

# ==================================================================================================


# ==================================================================================================
def test_simpleplot(file='./data/table.h5'):
    """
    This function illustrates how to plot two spectra with different magnetic field using the
    default plot method of the gammapy class.
    """

    # Build model with a slight smoothing
    models = CascadeTemporalSpectralModel(file, smooth_dex=0.2)
    
    # Display model information
    print(models)
    
    # Sets some parameters
    models[0].parameters["gamma"].value = 1.3
    models[0].parameters["alpha"].value = 1.1

    # Plot the updated parameter values
    for name,value in zip(models[0].parameters.names,models[0].parameters.value):
        print(f"{name:8}: {value}")

    # Select a time bin
    it = 1
    model = models[it]
    
    # Plot the spectra for different IGMF values
    for gamma in [2.1,2.5,2.9]:
        models[0].parameters["gamma"].value = gamma
        model.spectral_model.plot((2.e0*u.GeV,10*u.TeV),sed_type='e2dnde',label=f"gamma = {gamma}")

    # setup plot options
    plt.ylim(1.e-12,)
    plt.legend()
    plt.show()
# ==================================================================================================


# ==================================================================================================
def test_plotMC(file='./data/table.h5'):
    """
    This function illustrates how to plot two spectra with different magnetic field using the
    default plot method of the gammapy class.
    """
    
    pars = {'gamma':1.88,'scale':1.0}
    frozen = {'alpha':1.34}
    allpars = frozen|pars

    # Build model with a slight smoothing
    models = CascadeTemporalSpectralModel(file, smooth_dex=0.1, frozen=frozen)
    for key,val in pars.items():
        models[0].parameters[key].value = val

    # Plot the updated parameter values
    for name,value in zip(models[0].parameters.names,models[0].parameters.value):
        print(f"{name:8}: {value}")

    # Select a time bin
    it = 4
    model = models[it]

    # Plot interpolated gammapyspectral model
    model.spectral_model.plot((2.e0*u.GeV,10*u.TeV),sed_type='e2dnde',label=f"GammapyTableModel")

    # plot closest spectrum in the table
    tbl = TableModel(file=file, mode='r')
    ind = []
    for name,par in tbl.vparams.items() :
        ind += [np.argmin(np.abs(par.values-allpars[name]))]
        print(name,par.values[ind[-1]])
    ea = (tbl.ebins[:,0]*tbl.ebins[:,1])**0.5
    efe = ea**2 * (tbl.data[(0,*ind, it,...)] + tbl.data[(1,*ind, it,...)])
    plt.plot(ea/TeV,efe,ls='--',label='Closest table spectrum')


    # Plot results MC simulation with the closest logB
    Smodel = tbl.smodel
    Smodel.update(allpars)
    Smodel.info()
    file =  './data/' + parse_filename({'logB':tbl.fparams['logB'],'lambdaB':6,'z':Smodel.params['z']})
    emin, emax = tbl.ebins[0,0], tbl.ebins[-1,-1]
    tmin, tmax = tbl.tbins[it,0], tbl.tbins[it,1]
    rawdata = EventCatalog(filename=file,gen=[0,1,2,3],emin=emin,emax=emax,
                           tmax=tmax,thdmax=0.1*degree,chatter=0)
    nbin = 60
    eb = np.logspace(np.log10(emin),np.log10(emax),nbin+1)
    e = (eb[1:]*eb[:-1])**.5
    tbins = np.array([tmin,tmax])
    bd = BinnedData(rawdata, ebins=eb, tbins=tbins, gbins=[0,1,2,3], smodel=Smodel)
    y = np.sum(bd.data[:,:],axis=0)
    plt.plot(e/TeV,e**2*y,ls=':',label=f"MC")

    # setup plot options
    plt.ylim(1.e-12,)
    plt.legend()
    plt.show()
# ==================================================================================================



if __name__ == "__main__":
    test_view()
    test_simpleplot()
    test_plotMC()
    pass
