"""
Some simple test examples...
"""
from cascapy.constants import Mpc, Gpc, kpc
from cascapy.cosmo import *


def test0(z=1.):
    """
    Display the comoving distance off a source at redshift z, in a default LCDM model
    """
    c = LCDM()
    print(f"At z={z}, Dc={c.dc_z(z) / Mpc:.2e} Mpc")


def test1():
    """
    Plot the comoving distance as a function of redshift
    """
    c = LCDM()
    z = np.linspace(0, 10, 100)
    plt.plot(z, c.dc_z(z) / Mpc)
    plt.xlabel('Redshift')
    plt.ylabel('Comoving Distance (Mpc)')
    plt.show()

def test2():
    """
    Plot the evolution of the scale factor as a function of cosmic time
    """
    c = LCDM()
    t = np.linspace(-c.t0, c.t0/10, 100)
    plt.plot(t,c.a_t(t))
    plt.xlabel("Cosmic Time (s)")
    plt.ylabel("Scale factor a(t)")
    plt.ylim(0,)
    plt.show()

def test3():
    """
    Test the precision of the t -> a -> t conversion
    """
    cc = LCDM()
    t = -np.logspace(np.log10(cc.t0*.99999),1.e-2,1000)
    tt = cc.t_a((cc.a_t(t)))
    plt.plot(abs(t),abs((tt-t)/t))
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel('Cosmic time (s)')
    plt.ylabel('Relative Error when computing t(z(a(t)))')
    plt.show()


def test4():
    """
    Plot Dc(t).
    Test the precision of the t -> Dc -> t conversion
    Test the precision if the Dc -> t -> Dc conversion
    """
    cc = LCDM()
    t = np.logspace(0.,np.log10(cc.t0*.99999),1000)
    dd = cc.dc_t(t)

    plt.plot(t,dd/Gpc)
    plt.xlabel('Cosmic time (s)')
    plt.ylabel('Source Comoving Distance (Gpc)')
    plt.show()

    tt = cc.t_dc(dd)
    plt.plot(t,abs(tt-t)/t)
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel('Cosmic time (s)')
    plt.ylabel('Relative Error when computing t(Dc(t))')
    plt.title('Computing comoving distance from cosmic time and back')
    plt.show()

    Dc = np.logspace(np.log10(1.*kpc),np.log10(14.*Gpc),100)
    dd = cc.dc_t(cc.t_dc(Dc))
    plt.plot(Dc/Gpc,abs(Dc-dd)/Dc)
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel('Comoving distance (Gpc)')
    plt.ylabel('Relative Error when computing Dc(t(Dc))')
    plt.title('Computing cosmic time from comoving distance and back')
    plt.show()

def test5():
    """
    Plot Dc(z).
    Test the precision of the z -> Dc -> z conversion
    Test the precision if the Dc -> z -> Dc conversion
    """
    cc = LCDM()
    z = np.linspace(0.0001,100.,100)

    dd = cc.dc_z(z)

    plt.plot(z,dd/Gpc)
    plt.xlabel('Redshift')
    plt.ylabel('Source Comoving Distance (Gpc)')
    plt.show()

    zz = cc.z_dc(dd)
    plt.plot(z,abs(zz-z)/z)
    plt.yscale('log')
    plt.xlabel('Redshift')
    plt.ylabel('Relative Error when computing z(Dc(z))')
    plt.title('Computing comoving distance from redshift and back')
    plt.show()

    Dc = np.logspace(np.log10(1.*kpc),np.log10(14.*Gpc),100)
    dd = cc.dc_z(cc.z_dc(Dc))
    plt.plot(Dc/Gpc,abs(Dc-dd)/Dc)
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel('Comoving distance (Gpc)')
    plt.ylabel('Relative Error when computing Dc(z(Dc))')
    plt.title('Computing redshift from comoving distance and back')
    plt.show()


def test6():
    """
    Plot Dc(a).
    Test the precision of the a -> Dc -> a conversion
    Test the precision if the Dc -> a -> Dc conversion
    """
    cc = LCDM()
    a = np.logspace(-4,0.,100)

    dd = cc.dc_a(a)
    plt.plot(a,dd/Gpc)
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel('Scale factor')
    plt.ylabel('Source Comoving Distance (Gpc)')
    plt.show()

    aa = cc.a_dc(dd)
    plt.plot(a,abs(aa-a)/a)
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel('Redshift')
    plt.ylabel('Relative Error when computing a(Dc(a))')
    plt.title('Computing comoving distance from scale factor and back')
    plt.show()

    Dc = np.logspace(np.log10(1.*kpc),np.log10(14.*Gpc),100)
    dd = cc.dc_a(cc.a_dc(Dc))
    plt.plot(Dc/Gpc,abs(Dc-dd)/Dc)
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel('Comoving distance (Gpc)')
    plt.ylabel('Relative Error when computing Dc(a(Dc))')
    plt.title('Computing redshift from scale factor and back')
    plt.show()
    
if __name__ == "__main__":
    import numpy as np
    import matplotlib.pyplot as plt
    test0()
    test1()
    test2()
    test3()
    test4()
    test5()
    test6()
