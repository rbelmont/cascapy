"""
Some simple test examples...
"""
import numpy as np
import matplotlib.pyplot as plt
from cascapy.backgrounds import *
from cascapy.constants import eV,GeV,TeV

def test0():
    """
    Plot the background photon density (per unit comoving volume) as a function of redshift
    """
    bb = BKG()
    ee = EBL()
    cc = CMB()

    emin = 1.e-6 *eV
    emax = 30*eV
    e = np.logspace(np.log10(emin),np.log10(emax),500)
    de = e[1:]-e[:-1]
    e = np.sqrt(e[1:]*e[:-1])
    
    z = np.linspace(0,4,50)
    n = np.zeros_like(z)
    
    for i,zz in enumerate(z):
        n[i] = sum(bb.ne(e,zz) * de)
    plt.plot(z,n,label='total')
    
    for i,zz in enumerate(z):
        n[i] = sum(ee.ne(e,zz) * de)
    plt.plot(z,n,'--',label='ebl')
    
    for i,zz in enumerate(z):
        n[i] = sum(cc.ne(e,zz) * de)
    plt.plot(z,n,'--',label='cmb')
    
    plt.yscale('log')
    plt.xlabel(r'Redshift $z$')
    plt.ylabel(r'Comoving photon density (cm$^{-3}$)')
    plt.legend()
    plt.show()

def test1():
    """
    Plot the CMB+EBL background spectrum
    """

    bb = BKG()
    
    E = np.logspace(-5,2,100)*eV
    for z in [0,0.5,1,2,3]:
        plt.loglog(E/eV,bb.ene(E,z),label=f"z = {z}")
    plt.ylim(1.e-5,1.e5)
    plt.xlim(1.e-5,20.)
    plt.xlabel('Energy (eV)')
    plt.ylabel(r"$E\ n_E(E)$ (cm$^{-3})$")
    plt.show()


def test2():
    """
    Plot the absorption coefficient as a function of observed energy, for different source redshifts.
    """
    
    E = np.logspace(np.log10(10*GeV),np.log10(30*TeV),100)
    
    for z in [0.1,0.5,1.]:
        eabs = EBL(z=z).fabs
        plt.loglog(E/GeV,eabs(E),label=f'z = {z}')
    
    plt.ylim(1.e-6,2)
    plt.xlabel('Observed Energy (GeV)')
    plt.ylabel('EBL Absorption coefficient')
    plt.legend()
    plt.show()



if __name__ == "__main__":
    test0()
    test1()
    test2()
