"""
Some simple test examples...
"""
import numpy as np
from cascapy.constants import *
from cascapy.tablemodel import *
from cascapy.intrinsic import GRB190114C
from os import getenv



path = '/feynman/home/dap/lepche/rbelmont/work/cascadel_results/'
com = ['This table is a test table','It is just for fun']

tbins = np.logspace(np.log10(60),np.log10(5*day),11)
ebins = np.logspace(np.log10(1*GeV),np.log10(10*TeV),21)

# Instantiate the tablemodel class by defining some preliminary properties of the table
TB = TableModel(file='test_table.h5', mode='w', simupath=path, chatter=1, comments=com)

# Add some more properties (note that these could have been setup directly directly at the
# instantiation step
TB.tbins = tbins
TB.ebins = ebins
TB.smodel  = GRB190114C
TB.igmf    = {'logB':-16, 'lambdaB':6}

# Define the varying parameters of the table:
logB = [-17,-16]
gamma = np.linspace(2,3,5)
alpha = np.linspace(1,2,5)
TB.vparams = {'logB':logB,'gamma':gamma,'alpha':alpha}

# setup the table structure
TB.init_table()

# Compute table
nproc = int(getenv("SLURM_CPUS_PER_TASK",1))
if nproc>=1:
    TB.fill_table(nproc=nproc,chunkfactor=4)
