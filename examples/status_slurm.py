"""
Analyse a log file produced when a tablemodel is computed
Arguments:
	chatter (int): chatter lever
	logfile (str): name of the logfile 
"""

import argparse
from cascapy.tablemodel import Logf


if __name__ == '__main__':
	parser = argparse.ArgumentParser(prog="statusreport",description="Display a status report for the current job")
	parser.add_argument('-chatter', dest='chatter', type=int, help='Chatter level (integer)',required=False,action='store',default=0)
	parser.add_argument('-logfile', dest='file', type=str, help='Name of the log file',required=False,action='store',default='table.log')
	args = parser.parse_args()
	lf = Logf(logfile=args.file)
	lf.report(chatter=args.chatter)


