#!/bin/bash

#SBATCH --mail-user=renaud.belmont@cea.fr
#SBATCH --mail-type=end,fail
#SBATCH --job-name=test
#SBATCH --output=table.log
#SBATCH --error=trable.err
#SBATCH --cpus-per-task=20
#SBATCH --ntasks=1

srun -N 1 -n 1 -c ${SLURM_CPUS_PER_TASK} python3 table_slurm.py
