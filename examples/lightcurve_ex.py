"""
Some simple test examples for the Cascade module in the cascapy package
"""

import numpy as np
import matplotlib.pyplot as plt
from cascapy.constants import *
from cascapy.cascade import *
from cascapy.intrinsic import *

def test1():

    # Set the MC simulation file to read
    z = 0.4245
    B = 1.e-17
    file = './data/z042/particles_z0425_B1700_L600.csc'


    # read the MC simulation file and apply first selection
    emin,emax = 10*MeV,10*TeV
    tmin,tmax = 1.,1.e9
    ec = EventCatalog(filename=file,emin=emin,emax=emax,tmax=tmax,chatter=2)

    # Define source model
    Emodel = PowerLawSpectralModel(gamma=2., emin=1 * MeV, emax=10 * TeV)
    Tmodel = PulseTimeModel()
    Amodel = IsotropicAngularModel()
    model = SourceModel(emodel=Emodel, amodel=Amodel, tmodel=Tmodel)

    # define time bins from edges list
    ta,tbins,dt = grid(tmin,tmax,50,log=True)

    # define 2 contiguous energy bins from edges list
    ebins = [10*MeV,30*GeV,10*TeV]
    # define 2 seperated energy bins as a list of (left,right) doublets
    #ebins = ([10*MeV,10*GeV],[100*GeV,10*TeV])

    # apply model and bin data
    # As the two energy bands have a significant width, it is important to use the
    # flx_type='FE' mode when binning data. This computes the energy flux in each
    # band instead of the photon flux.
    bd = BinnedData(ec, ebins=ebins, tbins=tbins, smodel=model, flx_type='FE')

    # Plot results
    for ie in range(bd.ne):
        
        # multiply by the energy bin width to get the integrated energy
        y = bd.data[:,ie]*(bd.ebins[ie,1]-bd.ebins[ie,0])
        
        # plot
        plt.plot(ta,ta*y,label=f"E (GeV) = {bd.ebins[ie,0]/GeV:.2e}-{bd.ebins[ie,1]/GeV:.2e}")

    plt.xscale('log')
    plt.yscale('log')
    plt.ylim(1e-5,1e0)
    plt.xlabel('Time (s)')
    plt.ylabel('t*F(t) (erg/cm2)')
    plt.legend()
    plt.show()


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    test1()
    pass
