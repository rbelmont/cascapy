"""
These functions illustrate how to generate synthetic Spectro-Temporal Cascade Datasets and how to analyse them.
"""

import numpy as np
import matplotlib.pyplot as plt
from cascapy.gammapy import *
from cascapy.constants import TeV
from itertools import repeat
from multiprocessing import Pool, set_start_method
from scipy.interpolate import interp1d
from gammapy.modeling.models import UniformPrior


# Global table definitions for all tests
table = '/Users/rbelmont/Desktop/ultimatefit/GRB190114C_full.h5'

# ======================================================================================
# 1- Play with one single dataset
# ======================================================================================


# --------------------------------------------------------------------------------------
def test1():

    """
    Create one CascadeDatasets instance
    """

    # Set a dataset ID
    dsname = 'DS00000'

    # Define model
    model = CascadeTemporalSpectralModel(filename=table, frozen={'lambdaB': 6}, mname=dsname)
    model.params["gamma"].value = 2.22
    model.params["emax"].value = 30*TeV
    model.params["alpha"].value = 1.6
    model.params["logB"].value = -17.5

    # Set observation properties
    ne, emin, emax = 23, 0.105*TeV, 30*TeV
    eb = np.logspace(np.log10(emin), np.log10(emax), ne)
    zenith = 60
    loc = 'north'

    # Instantiate an empty dataset
    DS = CascadeDatasets()
    DS.create(model, eb=eb, loc=loc, zenith=zenith, name=dsname)

    # fake one real observation with given random seed (random_seed=None would use a runtime dependent seed)
    nobs = 1
    DSf = DS.fake(nobs, random_state=42)[0]

    # Save the dataset using the write() method of the gammapy, parent class
    DSf.write(dsname+'/'+dsname, overwrite=True)

    # Read one dataset
    # Note that no model is associated to the dataset at this stage, so that there is no predicted excess count
    dsname = 'DS00000'
    DS = CascadeDatasets().read(f'{dsname}/{dsname}', checksum=False)
# --------------------------------------------------------------------------------------


# ======================================================================================
# 2- Scan a dataset across grids of parameters
# ======================================================================================
# Scanning means freezing some parameters to fixed values on pre-defined grids, while
# fitting for other parameters. This result in arrays of fit statistics for all grids
# points. It can be used to investigate degeneracy between parameters, and also to help
# the convergence of more global fits where the scanned parameters are let free.

# --------------------------------------------------------------------------------------
def test2():
    """
    Scan the dataset
    """

    # Define grids of parameters to scan. Here two parameters are scanned: `logB` and
    # `emax`. Note that the order in which the grids are defined is important:
    #   - The parameter space is investigated by varying first the last parameters.
    #     When fitting the data at a new grid point, the best-fit values found at the
    #     previous grid point are used as initial values for the free parameters. This
    #     might affect the convergence of sparse grids.
    #   - Once the entire scanned parameter space has been scanned, this first scan is
    #     *reduced* iteratively, each time letting free the last parameter and refitting
    #     on grids points for the first parameters.

    # Read a stored dataset
    dsname = 'DS00000'
    name = dsname+'/'+dsname
    DS = CascadeDatasets().read(name, checksum=False)

    # Setup TableFit instance and freeze lambdaB for the entire analysis
    fit = TableFit(table, DS, frozen={'lambdaB': 6})

    # optionally limit the extent of a parameter range
#    fit.parameters["emax"].min=30*TeV

    # Scan for Emax and logB
    egrid = np.linspace(1.1*TeV, 70*TeV, 3)
    bgrid = np.linspace(-20, -13, 3)
    grids = {'logB': bgrid, 'emax': egrid}
    scan_2d, scan_1d, scan_0d = fit.run_scan(grids=grids, chatter=1, filename=name, reduce=True)

    # Analyse scan results
    # Scan results (e.g. here `scan_0d`, `scan_1d`and `scan_2d`) can be used directly,
    # or can be read from files as:
    scan_0d = Scan().from_file(name+'_0d.npy')
    scan_1d = Scan().from_file(name+'_1d.npy')
    scan_2d = Scan().from_file(name+'_2d.npy')

    # Print best fit results
    print(scan_0d)
    # Plot scan profile:
    print(scan_1d)
    ax = scan_1d.plot()
    # Plot scan map:
    print(scan_2d)
    ax = scan_2d.plot()
# --------------------------------------------------------------------------------------


# --------------------------------------------------------------------------------------
def test3():
    """
    Profile a 1-d scan

    Once a basic 1-d scan is obtained, it can be used to further refine the regions
    around minima, and to get the parameter uncertainty at some levels. The following
    cells allow to

    Analyse the 1-d scan and get all regions of the initial parameter grids where TS-TSmin < level
    In each region, perform a global fit by letting free the scanned parameter, and get the TSmin
    of the entire parameter space
    In each region, get the parameter values where sqrt(TS-TSmin) < sigma (with sigma=1,2,3,4,5 here)
    Add all found values (global fits results and uncertainty values) to the 1-d scan
    """

    dsname = 'DS00000'
    name = dsname+'/'+dsname

    # load dataset to re-analyse
    ds = CascadeDatasets().read(name, checksum=False)

    # load existing 1-d scan
    scan_1d = Scan().from_file(name+'_1d.npy')

    # retrieve the table that was used to make the 1d-scan (could also be set manually)
    table = scan_1d.filename_table

    # Setup TAble fit instance and freeze lambdaB for the following analysis
    fit = TableFit(table, ds, frozen={'lambdaB': 6})

    # Profile and store results to file
    fine_scan_1d = fit.profile_from_scan_1d(scan_1d, level=10, nsigma=[1, 2, 3], filename=dsname)

    # Plot fine profile
    ax = fine_scan_1d.plot()
# --------------------------------------------------------------------------------------


# ======================================================================================
# 3- Systematic analysis for many values of the magnetic field
# ======================================================================================

# --------------------------------------------------------------------------------------
def test4():
    """
    Create a series of datasets for many different values of parameter `logB`
    The datasets are labeled from the B-values that were used to generate them
    """

    # logB grid
    gridB = np.linspace(-20.5, -12.5, 8)

    for logB in gridB:

        print(f"logB = {logB}")
        dsname = f"DS{-int(1000*logB)}"

        # Load table model and build dataset
        model = CascadeTemporalSpectralModel(filename=table, frozen={'lambdaB': 6}, mname=dsname)
        model.params["gamma"].value = 2.22
        model.params["emax"].value = 30*TeV
        model.params["alpha"].value = 1.6
        model.params["logB"].value = logB

        # Set observation properties
        ne, emin, emax = 23, 0.105*TeV, 30*TeV
        eb = np.logspace(np.log10(emin), np.log10(emax), ne)
        zenith = 60
        loc = 'north'

        # Instantiate an empty dataset
        DS = CascadeDatasets()
        DS.create(model, eb=eb, loc=loc, zenith=zenith, name=dsname)
        
        # fake one real observation per logB value
        nobs = 1
        DSf = DS.fake(nobs, random_state=42)[0]

        # Save dataset to file
        DSf.write(dsname+'/'+dsname, overwrite=True)
# --------------------------------------------------------------------------------------


# --------------------------------------------------------------------------------------
def scan_one(iid, grids, table):
    """
    Function to scan and profile one single dataset
    """

    print(f"Scanning dataset with logB = {-float(iid)/1000}")
    dsname = 'DS'+iid
    name = dsname+'/'+dsname

    # read dataset
    ds = CascadeDatasets().read(name, checksum=False)

    # Instantiate the TableFit Class and hard-freeze lambdaB
    fit = TableFit(table, ds, frozen={'lambdaB': 6})

    # perform scan
    scan_2d, scan_1d, scan_0d = fit.run_scan(grids=grids, chatter=0, filename=name, reduce=True)

    # perform profile
    scan_1d = fit.profile_from_scan_1d(scan_1d, level=10, nsigma=[1, 2, 3, 4, 5], filename=name, chatter=0)

    return scan_1d
# --------------------------------------------------------------------------------------


# --------------------------------------------------------------------------------------
def test5(nproc=8):
    """
    Run parallel analysis
    """

    # Get all datasets stored in the current directory
    # but dataset 00000 (only used for previous tests)
    ids = get_dir_datasets(path='./')
    if '00000' in ids:
        ids.remove('00000')

    # Define scanning grids
    # Warning: logB must be the first parameter
    gridB = np.linspace(-20.5, -12.5, 10)
    gridE = np.array([1.5, 10, 30, 50])*TeV
    grids = {'logB': gridB, 'emax': gridE}

    # Run parallel analysis
    set_start_method('fork', force=True)
    pool = Pool(processes=nproc)
    pool.starmap(func=scan_one, iterable=zip(ids, repeat(grids), repeat(table)))
    pool.close()
    pool.join()
# --------------------------------------------------------------------------------------


# --------------------------------------------------------------------------------------
def test6():
    """
    Plot a logB-logB map
    Warning: After fine profiling, the profiles of the different have different grids. 
             It is therefore not possible to directly use the usual `colormesh`, `surface` 
             or other standard methods. An interpolation step must be added.
    """
    
    vmax = 10

    # get all datasets in current directory
    # but dataset 00000 (only used for previous tests)
    ids = get_dir_datasets(path='./')
    if '00000' in ids:
        ids.remove('00000')
    nx0 = len(ids)

    # build a fine grids from the grids points of all datasets
    X = []
    X0 = []
    for id in ids:
        prof = np.load(f'DS{id}/DS{id}_prof.npy', allow_pickle=True)[()]
        name = list(prof.grids.keys())[0]
        nval = len(prof.grids[name])
        X0 = np.append(X0, -float(id)/1000.)
        for x in prof.grids[name]:
            if x not in X:
                X = np.append(X, x)
    X.sort()
    nx = len(X)

    # for each dataset, interpolate over this fine grid
    Z = np.zeros((nx0,nx))
    for i0,id in enumerate(ids):
        prof = np.load(f'DS{id}/DS{id}_prof.npy', allow_pickle=True)[()]
        name = list(prof.grids.keys())[0]
        xx = prof.grids[name]
        yy = np.sqrt( prof.total_stat - prof.total_stat.min() )
        f = interp1d(xx, yy, bounds_error=None, fill_value="extrapolate")
        Z[i0,:] = f(X)

    # Plot a map made of vertical bands
    cf = plt.pcolormesh(X0, X, Z.T, vmax=vmax, cmap='viridis_r')
    cb = plt.colorbar(cf)

    # Plot contours
    levels = range(1,6)
    cs = plt.contour(X0, X, Z.T, levels=levels,colors='w',linewidths=0.5)
    plt.clabel(cs, cs.levels)

    plt.plot((X0[-1],X0[0]),(X0[-1],X0[0]),color='r')
    cb.ax.set_ylabel(r'$\sigma_i = \sqrt{TS_i}$')
    plt.gca().set_aspect('equal')
    plt.xlim(X0[-1],X0[0])
    plt.ylim(X0[-1],X0[0])
    plt.xlabel('Dataset '+name)
    plt.ylabel('Fitted '+name)
    plt.grid()
    plt.show()
# --------------------------------------------------------------------------------------

    
    
# ======================================================================================
if __name__ == "__main__":
# ======================================================================================
    test1()
    test2()
    test3()
    test4()
    test5()
    test6()
    pass

